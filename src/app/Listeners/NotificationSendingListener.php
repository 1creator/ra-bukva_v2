<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.10.2019
 * Time: 14:07
 */

namespace App\Listeners;


use App\Utils\AccumulateNotification;
use Illuminate\Notifications\Events\NotificationSending;

class NotificationSendingListener
{
    public function handle(NotificationSending $event)
    {
        if ($event->notification instanceof AccumulateNotification) {
            return $event->notification->check();
        } else {
            return true;
        }
    }
}