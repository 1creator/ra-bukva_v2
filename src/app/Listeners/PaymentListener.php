<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.10.2019
 * Time: 14:07
 */

namespace App\Listeners;


use App\Models\Order;
use App\Notifications\Admin\OrderPayed;
use App\Services\Payment\Events\PaymentRefund;
use App\Services\Payment\Events\PaymentSucceeded;
use Illuminate\Support\Facades\Notification;

class PaymentListener
{
    public function handle($event)
    {
        if ($event instanceof PaymentSucceeded) {
            $paymentId = $event->payment->id;
            /** @var Order $order */
            $order = Order::with('orderProducts')->where('payment_id', $paymentId)->first();
            if ($order) {
                foreach ($order->orderProducts as $orderProduct) {
                    $orderProduct->status = $orderProduct->need_layout ? 'layout' : 'work';
                    $orderProduct->save();
                }
                $order->is_payed_online = true;
                $order->save();

                $notification = new OrderPayed($order);
                Notification::route('viber', env('VIBER_ADMIN_ID'))
                    ->notify($notification);
            }
        } elseif ($event instanceof PaymentRefund) {
            $paymentId = $event->payment->id;
            /** @var Order $order */
            $order = Order::with('orderProducts')->where('payment_id', $paymentId)->first();
            if ($order) {
                foreach ($order->orderProducts as $orderProduct) {
                    $orderProduct->status = 'refunded';
                    $orderProduct->save();
                }
                $order->save();
            }
        }

        return true;
    }
}
