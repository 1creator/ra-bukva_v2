<?php

namespace App\Listeners;

use App\Events\OrderProductStatusChanged;
use App\Notifications\OrderChanged;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class OrderChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        if ($event instanceof OrderProductStatusChanged) {
            $order = $event->orderProduct->order;
            if ($order->email) {
                Notification::route('mail', $order->email)
                    ->notify(new OrderChanged($order));
            }
        }
    }
}
