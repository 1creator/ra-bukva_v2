<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;

class OrderProduct extends RequestedScope
{
    protected $fillable = ['product_id', 'count', 'amount_product', 'amount_layout', 'total',
        'fixed_amount_product', 'fixed_amount_layout',
        'cost', 'need_layout', 'status', 'comment', 'sale_percentage'];
    protected $availableWith = ['order', 'product', 'layouts', 'parameterValues'];

    protected $appends = ['total'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function layoutMediaFiles()
    {
        return $this->belongsToMany(MediaFile::class,
            'order_product_layouts', 'order_product_id', 'media_file_id')
            ->withPivot(['id', 'status']);
    }

    public function layouts()
    {
        return $this->hasMany(OrderProductLayout::class)->with('mediaFile');
    }

    public function parameterValues()
    {
        return $this->belongsToMany(ProductParameterValue::class, 'order_product_parameters')
            ->withPivot(['data']);
    }

    public function parameters()
    {
        $parameters = [];

        $this->parameterValues()->with('parameter')->get()->each(function ($item, $key) use (&$parameters) {
            if (!array_key_exists($item->parameter->id, $parameters)) {
                $parameters[$item->parameter->id] = $item->parameter;
                $parameters[$item->parameter->id]->user_values = collect();
            }
            $item['data'] = json_decode($item['pivot']['data'], true);
            $parameters[$item->parameter->id]->user_values->push($item);
        });

        return array_values($parameters);
    }

    public function getTotalAttribute()
    {
        return $this->amount_layout + $this->amount_product;
    }

    public function getTotalWithSaleAttribute()
    {
        $total = $this->amount_layout + $this->amount_product;
        return intval($total - $total * $this->order->partner_sale);
    }
}
