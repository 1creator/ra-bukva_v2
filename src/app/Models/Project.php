<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;

class Project extends RequestedScope
{
    protected $fillable = ['description', 'media_file_id'];
    protected $availableWith = ['mediaFile', 'products'];


    public function mediaFile()
    {
        return $this->belongsTo(MediaFile::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_project');
    }
}