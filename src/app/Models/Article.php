<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;

class Article extends RequestedScope
{
    protected $fillable = ['name', 'text', 'seo_name', 'image_id'];
    protected $availableWith = ['mediaFiles', 'products', 'image'];

    public function getRouteKeyName()
    {
        return 'seo_name';
    }

    public function mediaFiles()
    {
        return $this->belongsToMany(MediaFile::class, 'article_media_files');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'article_products');
    }

    public function image()
    {
        return $this->belongsTo(MediaFile::class, 'image_id');
    }
}