<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * @property ?Collection sizes
 * @property mixed type
 * @property mixed disk
 * @property mixed path
 * @method static create(array $array)
 * @method static where(string $string, string $string1, string $string2)
 * @method static findOrFail(mixed $attachment_id)
 */
class MediaFile extends Model
{
    protected $fillable = ['path', 'type', 'info', 'name', 'sizes', 'disk'];

    protected $casts = [
        'info' => 'collection',
        'sizes' => 'collection'
    ];

    protected $hidden = ['path'];

    protected $appends = ['url', 'thumbnail'];

//    protected static function booted()
//    {
//        static::deleted(function ($model) {
//            if ($model['path'])
//                File::delete(public_path($model['path']));
//            if ($model['thumbnail'])
//                File::delete(public_path($model['thumbnail']));
//        });
//    }

    public function getThumbnailAttribute(): ?string
    {
//        $this->getRawOriginal()
        if ($this->sizes && $this->sizes->isNotEmpty()) {
            return $this->sizes->sortBy('width')->first()['path'];
        } else {
            return null;
        }
    }

    public function getSizesAttribute($val): Collection
    {
        $val = json_decode($val);
        if (!$val) {
            return collect();
        } else {
            return collect($val)->map(function ($size) {
                return [
                    'width' => $size->width,
                    'height' => $size->height,
                    'path' => Storage::disk($this->disk)->url($size->path),
                ];
            });
        }
    }

    public function getUrlAttribute(): ?string
    {
        if ($this->type === 'embed') {
            return $this->path;
        } elseif (AttachmentService::isWebImage($this->type) && $this->sizes && $this->sizes->isNotEmpty()) {
            return $this->sizes->sortByDesc('width')->first()['path'];
        } else {
            return Storage::disk($this->disk)->url($this->path);
        }
    }
}
