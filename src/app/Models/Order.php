<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/**
 * @method static whereHas(string $string, \Closure $param)
 * @method static where(string $string, string $string1, $from)
 * @property mixed rating
 * @property mixed review
 * @property mixed client_upd
 * @property mixed created_at
 */
class Order extends RequestedScope
{
    protected $fillable = [
        'comment',
        'user_id',
        'company_id',
        'email',
        'phone',
        'name',
        'delivery_address',
        'delivery_price',
        'delivery_coordinates',
        'promocode_id',
        'bonuses_spent',
        'secret',
        'partner_sale'
    ];
    protected $availableWith = [
        'orderProducts',
        'orderProducts.product',
        'user',
        'company',
        'orderProducts.layouts',
        'orderProducts.parameterValues',
        'orderProducts.parameterValues.parameter'
    ];
    protected $appends = ['total'];
    protected $dates = ['completed_at'];

    protected $casts = ['delivery_coordinates' => 'array'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function promocode()
    {
        return $this->belongsTo(Promocode::class);
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function bonusBalanceChanges()
    {
        return $this->hasMany(BonusBalanceChange::class);
    }

    public function getItemsTotalAttribute()
    {
        return $this->orderProducts()
            ->sum(DB::raw('amount_product + amount_layout'));
    }

    public function getIsCompletedAttribute()
    {
        return $this->orderProducts->every(fn($item) => $item['status'] == 'ready');
    }

    public function getTotalAttribute()
    {
        $total = $this->orderProducts()
                ->sum(DB::raw('amount_product + amount_layout')) + $this->delivery_price - $this->bonuses_spent;
        if ($this->promocode) {
            $total -= $this->promocode->sale;
        }
        if ($this->partner_sale > 0) {
            $total -= round($total * $this->partner_sale);
        }
        return $total;
    }

    public function getContractCodeAttribute()
    {
        return $this->created_at->format('d/m-') . $this->id;
    }

    protected function filter(Builder $query, $params)
    {
        switch ($params[0]) {
            case 'companyName':
                $query->has('company', '>=', 1, $params[3], function ($q) use ($params) {
                    $q->where('name', $params[1], $params[2]);
                });
                return true;
            case 'companyInn':
                $query->has('company', '>=', 1, $params[3], function ($q) use ($params) {
                    $q->where('inn', $params[1], $params[2]);
                });
                return true;
        }
        return false;
    }

    public function getRatingAttribute()
    {
        return number_format($this->orderProducts()->average('rating'), 2);
    }


    public function getLinkAttribute()
    {
        return route('orders.show', [
            'order' => $this->id,
        ]);
    }

    public function getSecretLinkAttribute()
    {
        return route('orders.show', [
            'order' => $this->id,
            'secret' => $this->secret,
        ]);
    }
}
