<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductParameterValue extends Model
{

    protected $fillable = ['name', 'image_id', 'description', 'product_parameter_id', 'impact'];

    protected $casts = [
        'impact' => 'array'
    ];

    protected $appends = ['data'];
    protected $with = ['image'];

    public function parameter()
    {
        return $this->belongsTo(ProductParameter::class, 'product_parameter_id')
            ->withDefault([
                'name' => 'Параметр удален',
                'always_show' => null,
                'product_id' => null,
                'multiple' => null,
                'type' => null,
                'required' => null,
            ]);
    }

    public function image()
    {
        return $this->belongsTo(MediaFile::class);
    }

    public function getDataAttribute()
    {
        if ($this->pivot) {
            return json_decode($this->pivot->data, true);
        }
    }
}
