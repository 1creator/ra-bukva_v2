<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $fillable = ['user_id', 'position'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'staff_permissions');
    }
}