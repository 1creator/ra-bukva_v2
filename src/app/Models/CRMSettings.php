<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CRMSettings extends Model
{
    public $timestamps = null;
    protected $table = 'crm_settings';
    protected $fillable = ['bonus_bet', 'partner_sale', 'delivery_zones', 'free_delivery_from'];

    protected $casts = [
        'delivery_zones' => 'array',
    ];

    public static function instance()
    {
        return self::all()->first();
    }
}