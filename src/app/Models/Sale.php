<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;
use Carbon\Carbon;

class Sale extends RequestedScope
{
    protected $fillable = ['name', 'description', 'media_file_id', 'from_date', 'to_date',
        'disabled', 'starts_at', 'finishes_at'];
    protected $availableWith = ['mediaFile'];
    protected $dates = ['created_at', 'updated_at', 'starts_at', 'finishes_at'];

    public function mediaFile()
    {
        return $this->belongsTo(MediaFile::class);
    }

    public function scopeActive($query)
    {
        return $query->where('disabled', 0)
            ->where('starts_at', '<=', Carbon::now())
            ->where('finishes_at', '>', Carbon::now());
    }
}
