<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;


use App\Http\RequestedScope;

class Category extends RequestedScope
{
    protected $fillable = ['name', 'seo_name', 'meta_words', 'order_weight', 'description', 'disabled',
        'is_package', 'parent_id', 'image_id', 'h1'];
    protected $availableWith = ['products', 'children', 'children.products', 'parent', 'image', 'products.image',
        'children.image', 'children.products.image',];

    public function getRouteKeyName()
    {
        return 'seo_name';
    }

    public function image()
    {
        return $this->belongsTo(MediaFile::class);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function parents()
    {
        return $this->parent()->with(['parents']);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_categories');
    }

    public function childrenNext()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->childrenNext()->with('children.products');
    }

    public function scopeActive($query)
    {
        return $query->where('disabled', false);
    }

    public function breadCrumbs()
    {
        $breadcrumbs = [
            ['name' => $this->name, 'link' => route('categories.show', ['category' => $this->seo_name])],
        ];

        $parents = $this->parents;
        $parent = $parents;

        while ($parent) {
            array_push($breadcrumbs,
                ['name' => $parent->name, 'link' => route('categories.show', ['category' => $parent->seo_name])]);
            $parent = $parent->parent;
        }

        array_push($breadcrumbs,
            ['name' => 'Каталог услуг', 'link' => '/catalog'],
            ['name' => 'Главная', 'link' => '/']);

        return array_reverse($breadcrumbs);
    }

    public function cardListItems()
    {

        $items = [];
        foreach ($this->childrenNext()->active()->take(10)->get() as $child) {
            array_push($items,
                ['name' => $child->name, 'link' => route('categories.show', ['category' => $child->seo_name])]);
        }

        if (count($items) < 10) {
            $products = $this->products()->active()->take(10 - count($items))->get();
            foreach ($products as $product) {
                array_push($items,
                    ['name' => $product->name, 'link' => route('products.show', ['product' => $product->seo_name])]);
            }
        }

        $totalCount = $this->childrenNext()->active()->count() + $this->products()->active()->count();

        if ($totalCount > 10) {
            $countLeft = $totalCount - 10;
            array_push($items,
                ['name' => "Ещё ${countLeft} услуг...", 'link' => route('categories.show', ['category' => $this->seo_name])]);
        }

        return $items;
    }
}
