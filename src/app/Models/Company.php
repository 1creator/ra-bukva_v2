<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;
use Carbon\Carbon;

class Company extends RequestedScope
{
    protected $fillable = ['name', 'director', 'legal_address', 'mail_address', 'okpo', 'inn', 'kpp', 'bank', 'rs',
        'ks', 'bik', 'partner_status', 'partner_sale'];

    protected $availableWith = ['users'];

    protected $casts = [
        'partner_sale' => 'float',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_company');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}