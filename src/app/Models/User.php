<?php

namespace App\Models;

use App\Http\RequestedScope;
use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Traits\InteractsWithPaymentsTrait;
use App\Services\ResetPassword\Interfaces\ResetPasswordInterface;
use App\Services\ResetPassword\Traits\ResetPasswordTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;

class User extends RequestedScope implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    InteractsWithPaymentsInterface,
    ResetPasswordInterface
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable, InteractsWithPaymentsTrait,
        ResetPasswordTrait;

    protected $availableWith = ['staff', 'staff.permissions'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'second_name', 'middle_name', 'address',
        'email', 'password', 'phone', 'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'user_company');
    }

    public function isStaff()
    {
        return $this->staff()->exists();
    }

    public function bonusBalanceChanges()
    {
        return $this->hasMany(BonusBalanceChange::class);
    }

    public function getFullNameAttribute()
    {
        return $this->second_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    public function getBonusBalanceAttribute()
    {
        $received = $this->bonusBalanceChanges()
            ->where('type', 'add')
            ->where('can_be_used', true)
            ->sum('value');

        $spent = $this->bonusBalanceChanges()->where('type', 'sub')->sum('value');

        return ($received - $spent);
    }

    public function permissions()
    {
        if ($this->isStaff()) {
            if ($this->email == 'admin@ra-bukva.ru') {
                return Permission::all();
            } else {
                return $this->staff->permissions;
            }
        } else {
            return null;
        }
    }

    public function profile()
    {
        $result = collect([
            'birthday' => $this->birthday,
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'middle_name' => $this->middle_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
        ]);
        if ($this->isStaff()) {
            $result = $result->merge([
                'position' => $this->staff->position,
                'permissions' => $this->permissions()
            ]);
        } else {
            $result = $result->merge([
                'bonus_balance' => $this->bonusBalance,
                'companies' => $this->companies,
            ]);
        }
        return $result;
    }

    public function getPayerFullName(): ?string
    {
        return implode(' ', [$this->second_name, $this->first_name, $this->middle_name]);
    }
}
