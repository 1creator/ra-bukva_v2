<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;

class Promocode extends RequestedScope
{
    protected $fillable = ['id', 'name', 'sale', 'min_amount', 'max_used_count', 'expires_at'];
    public $incrementing = false;
    protected $dates = ['created_at', 'updated_at', 'expires_at'];
    protected $appends = ['used_count'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getUsedCountAttribute()
    {
        return $this->orders()->count();
    }
}