<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;
use Illuminate\Database\Eloquent\Model;

class Permission extends RequestedScope
{
    public $incrementing = false;

    public const ARTICLES_UPDATE = 'articles.update';
    public const ORDERS_VIEW = 'orders.view';
    public const ORDERS_VIEW_FOR_DESIGN = 'orders.view_for_design';
    public const ORDERS_UPDATE = 'orders.update';
    public const CATALOG_UPDATE = 'catalog.update';
    public const PROJECTS_UPDATE = 'projects.update';
    public const SALES_UPDATE = 'sales.update';
    public const MAILINGS_UPDATE = 'mailings.update';
    public const COMPANIES_VIEW = 'companies.view';
    public const COMPANIES_UPDATE = 'companies.update';
    public const STATISTIC_VIEW = 'statistic.view';
    public const USERS_VIEW = 'users.view';
    public const USERS_UPDATE = 'users.update';
    public const USERS_GRANT_STAFF_ACCESS = 'users.grant_staff_permissions';
    public const SETTINGS_UPDATE = 'settings.update';
    public const PROMOCODES_UPDATE = 'promocodes.update';
}