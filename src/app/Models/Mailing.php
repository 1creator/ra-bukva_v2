<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;
use Carbon\Carbon;

class Mailing extends RequestedScope
{
    protected $fillable = ['subject', 'mail_body', 'targets'];

    protected $casts = [
        'targets' => 'array',
    ];
}