<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;

class BonusBalanceChange extends RequestedScope
{
    protected $fillable = ['user_id', 'type', 'value', 'order_id', 'order_product_id', 'can_be_used', 'created_at'];
    protected $availableWith = ['order', 'orderProduct'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function orderProduct()
    {
        return $this->belongsTo(OrderProduct::class);
    }
}