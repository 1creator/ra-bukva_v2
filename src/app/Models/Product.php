<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Elastic\Product\ProductIndexConfigurator;
use App\Http\RequestedScope;
use ScoutElastic\Searchable;

class Product extends RequestedScope
{
    use Searchable;

    protected $indexConfigurator = ProductIndexConfigurator::class;

    // Here you can specify a mapping for model fields
    protected $mapping = [
        'properties' => [
            'name' => [
                'type' => 'text',
            ],
        ]
    ];

    protected $fillable = ['name', 'seo_name', 'meta_words', 'order_weight', 'description', 'accent', 'disabled',
        'meta_title', 'sales', 'count_calculating', 'h1', 'min_count',
        'base_price', 'base_layout_price', 'base_cost', 'production_time', 'count_name', 'count_type', 'image_id',
        'order_files_extensions', 'order_files_count', 'order_files_extensions', 'order_files_validator_url',
        'count_symbol', 'count_code',
    ];

    protected $availableWith = ['images', 'parameters', 'parameters.values', 'parameters.values.image',
        'projects', 'categories', 'image', 'recommendations', 'mediaFiles'];

    protected $casts = [
        'base_price' => 'double',
        'base_layout_price' => 'double',
        'base_cost' => 'double',
        'sales' => 'array',
        'order_files_extensions' => 'array',
    ];

    protected $appends = ['lowPrice', 'link'];
    protected $with = ['image'];


    public function scopeActive($query)
    {
        return $query->where('disabled', false);
    }

    public function scopeAccented($query)
    {
        return $query->where('accent', true)->where('disabled', false);
    }

    public function getRouteKeyName()
    {
        return 'seo_name';
    }

    public function image()
    {
        return $this->belongsTo(MediaFile::class);
    }

    public function mediaFiles()
    {
        return $this->belongsToMany(MediaFile::class, 'product_media_file');
    }

    public function parameters()
    {
        return $this->hasMany(ProductParameter::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_products');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'product_project')
            ->orderByDesc('created_at');
    }

    public function recommendations()
    {
        return $this->belongsToMany(Product::class, 'product_recommendations',
            'product_id', 'rec_product_id');
    }

    public function getProjectsSet()
    {
        $imgProjects = $this->projects()->inRandomOrder()->whereHas('mediaFile', function ($query) {
            $query->where('type', '!=', 'embed');
        })->take(12)->with('mediaFile')->get();
        $videoProjects = $this->projects()->whereHas('mediaFile', function ($query) {
            $query->where('type', '=', 'embed');
        })->take(6)->with('mediaFile')->get();

        $res = $imgProjects;

        foreach ($videoProjects as $index => $video) {
            $res->splice($index * 6, 0, [$video]);
        }
        return $res->take(12 - $videoProjects->count());
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'products_categories');
    }

    public function mainCategory()
    {
        $category = $this->categories->first();
        if (!$category) {
            $category = Category::all()->first();
        }
        return $category;
    }

    public function getMetaDescriptionAttribute()
    {
        if ($this->meta_words) {
            return $this->meta_words;
        } else {
            return $this->name . ' - рассчитать стоимость и посмотреть оптовые скидки в каталоге услуг.';
        }
    }

    public function getLowPriceAttribute()
    {
        $price = $this->base_price;
        $defaultSalesMap = $this->sales['default'] ?? [];
        if (count($defaultSalesMap) > 0) {
            $price *= 1 - floatval(end($defaultSalesMap)['sale']);
        }
        return round($price, 2);
    }

    public function breadCrumbs()
    {
        $breadcrumbs = $this->mainCategory()->breadCrumbs();

        array_push($breadcrumbs,
            ['name' => $this->name, 'link' => route('products.show', ['product' => $this->seo_name])]);

        return $breadcrumbs;
    }

    public function getLinkAttribute()
    {
        return route('products.show', ['product' => $this->seo_name]);
    }
}
