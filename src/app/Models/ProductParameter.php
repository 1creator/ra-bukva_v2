<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductParameter extends Model
{

    protected $fillable = ['name', 'always_show', 'product_id', 'multiple', 'type', 'required'];
    protected $casts = [
        'variants' => 'array'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function values()
    {
        return $this->hasMany(ProductParameterValue::class);
    }
}