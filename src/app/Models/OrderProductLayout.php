<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Http\RequestedScope;

class OrderProductLayout extends RequestedScope
{
    protected $fillable = ['order_product_id', 'media_file_id', 'status', 'comment'];
    protected $availableWith = ['orderProduct', 'mediaFile'];

    public function orderProduct()
    {
        return $this->belongsTo(OrderProduct::class);
    }

    public function mediaFile()
    {
        return $this->belongsTo(MediaFile::class);
    }
}