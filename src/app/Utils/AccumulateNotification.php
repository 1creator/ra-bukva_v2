<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 03.10.2019
 * Time: 11:06
 */

namespace App\Utils;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AccumulateNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected string $accumulationKey = 'default';
    protected int $accumulationDelay = 5;
    public string $accumulationId;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct()
    {
        $this->accumulationId = Str::random();

        $when = now()->addMinutes($this->accumulationDelay);
        $this->delay($when);

        Cache::put($this->accumulationKey, $this->accumulationId, $when->clone()->addMinutes(5));

//        Log::info('Accumulate create: ' . $this->accumulationKey . ' is ' . $this->accumulationId);
    }

    /**
     * @return bool
     */
    public function check()
    {
        $val = Cache::get($this->accumulationKey);

//        Log::info('Accumulate check: ' . $this->accumulationId .
//            ($this->accumulationId == $val ? '==' : '!=') . $val);
        return $val == $this->accumulationId;
    }
}
