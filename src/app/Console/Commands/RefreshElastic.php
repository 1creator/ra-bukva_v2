<?php

namespace App\Console\Commands;

use App\Models\Activity;
use App\Models\Article;
use App\Models\Direction;
use App\Models\Division;
use App\Models\Faq;
use App\Models\Product;
use App\Models\School;
use App\Models\Section;
use Exception;
use Illuminate\Console\Command;

class RefreshElastic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:refresh {model?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Команда обновляет все индексы и реимпортирует модели';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed|void
     */
    public function handle()
    {
        $models = $this->argument('model') ? [$this->argument('model')] : [
            Product::class,
        ];

        foreach ($models as $modelClass) {
            $model = new $modelClass();
            $indexConfiguratorClass = null;
            if (method_exists($model, 'getIndexConfigurator')) {
                $indexConfiguratorClass = get_class($model->getIndexConfigurator());
                try {
                    $this->call('elastic:drop-index', ['index-configurator' => $indexConfiguratorClass]);
//                    $this->info('Индекс ' . $indexConfiguratorClass . ' пересоздан.');
                } catch (Exception $e) {
//                    $this->info('Индекс ' . $indexConfiguratorClass . ' создается впервые.');
                }
                $this->call('elastic:create-index', ['index-configurator' => $indexConfiguratorClass]);
                $this->call('scout:import', ['model' => $modelClass]);
            } else {
                $this->error($modelClass . ' должен иметь метод getIndexConfigurator');
            }
        }
    }
}
