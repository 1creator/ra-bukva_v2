<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Notifications\ReviewRequestNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class SendReviewRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reviews:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправить письмо с просьбой оставить отзыв клиентам, чей заказ выполнен 3 дня назад.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed|void
     */
    public function handle()
    {
        $threeDaysAgo = now()->subDays(1);
        $orders = Order::whereDate('completed_at', $threeDaysAgo)
            ->where('review', null)->get();
        foreach ($orders as $order) {
            $notification = new ReviewRequestNotification($order);
            Notification::route('mail', $order->email)->notify($notification);
        }
        $res = "Successfully sent {$orders->count()} review request by email.";
        Log::info($res);
        $this->info($res);
    }
}
