<?php

namespace App\Console\Commands;

use App\Models\MediaFile;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class MoveFilesToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:to-s3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Перенос всех файлов на диск S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed|void
     */
    public function handle()
    {
        $mediaFiles = MediaFile::where('disk', '!=', 's3')->get();
        $totalCount = $mediaFiles->count();
        foreach ($mediaFiles as $key => $mediaFile) {
            try {
                $sizes = json_decode($mediaFile->getRawOriginal('sizes')) ?? collect();
                foreach ($sizes as $size) {
                    if ($size->path != $mediaFile->path) {
                        $content = Storage::disk($mediaFile->disk)->get($size->path);
                        $size->path = basename($size->path);
                        Storage::disk('s3')->put(basename($size->path), $content);
                    }
                }
                $mediaFile->sizes = $sizes;

                if ($mediaFile->path) {
                    $content = Storage::disk($mediaFile->disk)->get($mediaFile->path);
                    Storage::disk('s3')->put(basename($mediaFile->path), $content);
                    $mediaFile->path = basename($mediaFile->path);
                }

                $mediaFile->disk = 's3';
                $mediaFile->save();
                $this->info("Process: $key/$totalCount.");
            } catch (FileNotFoundException $e) {
                $this->error($e);
            }
        }
    }
}
