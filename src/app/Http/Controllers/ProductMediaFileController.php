<?php

namespace App\Http\Controllers;

use App\Models\MediaFile;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductMediaFileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index(Request $request, Product $product)
    {
        return ['response' => $product->mediaFiles];
    }

    public function show(Request $request, Product $product, MediaFile $mediaFile)
    {
        return Storage::disk($mediaFile->disk)->download($mediaFile->path, $mediaFile->name);
    }

}
