<?php


namespace App\Http\Controllers\CRUD;


use App\Elastic\Product\ProductSearchRule;
use App\Elastic\Rules\ActivitySearchRule;
use App\Models\Activity;
use App\Models\City;
use App\Models\Product;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class SearchController
{
    use ValidatesRequests;

    public function index(Request $request)
    {
        $this->validate($request, [
            'query' => 'required|max:1000',
            'search_for' => 'nullable|array',
        ]);

        $query = $request->input('query');
        $searchFor = $request->input('search_for', ['products']);
        $res = [];
        if (in_array('products', $searchFor)) {
            $products = Product::search($query)
                ->rule(ProductSearchRule::class)
                ->take(10)
                ->get();

            $res['products'] = $products->toArray();
        }

        return $res;
    }
}
