<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\Controller;
use App\Models\MediaFile;
use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FileController extends Controller
{
    public function show(Request $r, MediaFile $file)
    {
        return response(['response' => $file]);
    }

    public function store(Request $r)
    {
        $files = app(AttachmentService::class)
            ->store($r['file'], $r->input('type'), $r->input('split_pdf', false));
        return ['response' => $files];
    }
}
