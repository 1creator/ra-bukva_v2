<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Events\OrderProductStatusChanged;
use App\Http\Controllers\Controller;
use App\Models\BonusBalanceChange;
use App\Models\CRMSettings;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProductLayout;
use App\Models\Product;
use App\Notifications\OrderChanged;
use App\Services\Order\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OrderProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->authorizeResource(OrderProduct::class, 'orderProduct');
    }

    public function index(Request $request)
    {
        $this->authorize('index', OrderProduct::class);

        $result = OrderProduct::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $product = Product::findOrFail($request['product_id']);

        $calc = app(OrderService::class)->calculateProduct($product, $request['parameter_values'], $request['count'], $request['need_layout']);
        $op = app(OrderService::class)->createOrderProduct($order, $product->id, $request['need_layout'],
            $request['parameter_values'], $request['layout_ids'], $calc);

        $op->load(['product', 'parameterValues.parameter', 'layouts']);

        event(new \App\Events\OrderChanged($order));
        return ['response' => $op];
    }

    public function update(Request $request, OrderProduct $orderProduct)
    {
        $this->authorize('update', $orderProduct);

        $orderProduct->fill($request->only(['status', 'need_layout', 'amount_product', 'amount_layout']));

        $user = $orderProduct->order->user;
        if ($user && $orderProduct->isDirty('status') && $orderProduct->status == 'ready') {
            $bonusBet = CRMSettings::instance()->bonus_bet;
            $now = Carbon::now();

            BonusBalanceChange::create([
                'user_id' => $user->id,
                'type' => 'add',
                'value' => $orderProduct->total * $bonusBet,
                'order_id' => $orderProduct->order->id,
                'order_product_id' => $orderProduct->id,
                'created_at' => $now,
            ]);
        }

        $orderProduct->save();
        $orderProduct->refresh();

        event(new OrderProductStatusChanged($orderProduct));
        if ($orderProduct->order->is_completed) {
            $orderProduct->order->completed_at = Carbon::now();
            $orderProduct->order->save();
        }

        return ['response' => $orderProduct];
    }

    public function destroy(Request $request, OrderProduct $orderProduct)
    {
        $this->authorize('update', $orderProduct);

        $orderProduct->delete();

        return ['response' => [
            'order_total' => $orderProduct->order->total
        ]];
    }

    public function review(Request $request, OrderProduct $orderProduct)
    {
        $this->authorize('update', $orderProduct);

        foreach ($request->reviews as $key => $fields) {
            $model = OrderProductLayout::findOrFail($key);
            $model->update($fields);
        }

        $newStatus = $request->status;
        $orderProduct->update(['status' => $newStatus]);

        event(new OrderProductStatusChanged($orderProduct));
        return ['response' => $orderProduct];
    }

    public function storeLayouts(Request $request, OrderProduct $orderProduct)
    {
        $this->authorize('update', $orderProduct);

        $orderProduct->layoutMediaFiles()->syncWithoutDetaching($request->media_file_ids);

        if ($request->status) {
            $orderProduct->update(['status' => $request->status]);
        }

        $newLayouts = $orderProduct->layouts()
            ->whereIn('media_file_id', $request->media_file_ids)->with('mediaFile')->get();

        event(new OrderProductStatusChanged($orderProduct));
        return ['response' => [
            'status' => $orderProduct->status,
            'layouts' => $newLayouts
        ]];
    }
}
