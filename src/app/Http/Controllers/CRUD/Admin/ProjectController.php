<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->authorizeResource(Project::class, 'project');
    }

    public function index(Request $request)
    {
        $result = Project::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|max:255',
            'media_file_id' => 'required',
        ]);
        $result = Project::create($request->all());
        if ($request->has('product_ids')) {
            $result->products()->sync($request->product_ids);
        }
        return ['response' => $result];
    }

    public function update(Request $request, Project $project)
    {
        $this->validate($request, [
            'description' => 'required|max:255',
            'media_file_id' => 'required',
        ]);
        $project->update($request->only($project->getFillable()));
        if ($request->has('product_ids')) {
            $project->products()->sync($request->product_ids);
        }
        return ['response' => $project];
    }

    public function destroy(Request $request, Project $project)
    {
        $result = $project->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Project $project)
    {
        $project->loadRequested($request);
        return ['response' => $project];
    }

}
