<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Category::class, 'category');
    }

    public function index(Request $request)
    {
        $result = Category::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'seo_name' => 'required|unique:categories',
        ]);
        $result = Category::create($request->all());
        return ['response' => $result];
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name' => 'required',
            'seo_name' => "required|unique:categories,seo_name,${category['id']}",
            'parent_id' => "not_in:${category['id']}",
        ]);
        $category->update($request->only($category->getFillable()));
        return ['response' => $category];
    }

    public function destroy(Request $request, Category $category)
    {
        $result = $category->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Category $category)
    {
        $category->loadRequested($request);
        return ['response' => $category];
    }

}
