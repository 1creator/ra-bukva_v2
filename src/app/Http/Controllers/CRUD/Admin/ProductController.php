<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Elastic\Product\ProductSearchRule;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductParameter;
use App\Models\ProductParameterValue;
use App\Models\ProductSale;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Product::class, 'product');
    }

    public function index(Request $request)
    {
        if ($request->input('query')) {
            $result = Product::search($request->input('query'))
                ->rule(ProductSearchRule::class)
                ->when($request->has('limit'), function ($q) use ($request) {
                    $q->take($request->input('limit'));
                })
                ->when($request->filled('with'), function ($q) use ($request) {
                    $q->with($request->input('with'));
                })
                ->get();
        } else {
            $result = Product::requested($request)->get();

        }
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'seo_name' => 'required|unique:products',
            'base_price' => 'required|numeric',
            'base_cost' => 'required|numeric',
            'base_layout_price' => 'required|numeric',
            'count_name' => 'required',
            'count_type' => 'required',
            'sales' => 'nullable:array',
        ]);
        $product = new Product($request->all());
        $product->sales = ['default' => [], 'features' => []];
        $product->save();
//        $product->categories()->sync($request->category_ids);
//        $product->recommendations()->sync($request->rec_product_ids);
//
//        foreach ($request->parameters as $parameter) {
//            $createdParameter = null;
//            $parameter['product_id'] = $product->id;
//            $createdParameter = ProductParameter::create($parameter);
//
//            if (isset($parameter['values']) && count($parameter['values'])) {
//                $createdParameter->values()->create($parameter['values']);
//            }
//
////            foreach ($parameter['values'] as $parameterValue) {
////                $createdParameterValue = null;
////                $parameterValue['product_parameter_id'] = $createdParameter->id;
////                ProductParameterValue::create($parameterValue);
////            }
//        }
//        $product->load(['categories', 'parameters.values.image', 'image']);
//        $product->load(['image']);
        return ['response' => $product];
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'sometimes|required',
            'seo_name' => "sometimes|required|unique:products,seo_name,${product['id']}",
            'base_price' => 'sometimes|required|numeric',
            'base_cost' => 'sometimes|required|numeric',
            'base_layout_price' => 'sometimes|required|numeric',
            'count_name' => 'sometimes|required',
            'count_type' => 'sometimes|required',
            'sales' => 'sometimes|nullable:array',
        ]);
        $product->update($request->only($product->getFillable()));

        if ($request->media_file_ids) {
            $product->mediaFiles()->sync($request->media_file_ids);
        }

        if ($request->category_ids) {
            $product->categories()->sync($request->category_ids);
        }

        if ($request->rec_product_ids) {
            $product->recommendations()->sync($request->rec_product_ids);
        }

        if ($request->parameters) {

            $parameterIds = [];
            foreach ($request->parameters as $parameter) {
                $creatingParameter = null;
                if (array_key_exists('id', $parameter)) {
                    $creatingParameter = ProductParameter::find($parameter['id']);
                }
                $creatingParameter = $creatingParameter ?? new ProductParameter();

                $creatingParameter->fill($parameter);
                $creatingParameter['product_id'] = $product->id;
                $creatingParameter->save();
                array_push($parameterIds, $creatingParameter->id);

                $parameterValueIds = [];
                foreach ($parameter['values'] as $parameterValue) {
                    $creatingParameterValue = null;
                    if (array_key_exists('id', $parameterValue)) {
                        $creatingParameterValue = ProductParameterValue::find($parameterValue['id']);
                    }
                    $creatingParameterValue = $creatingParameterValue ?? new ProductParameterValue();

                    $creatingParameterValue->fill($parameterValue);
                    $creatingParameterValue['product_parameter_id'] = $creatingParameter->id;
                    $creatingParameterValue->save();

                    array_push($parameterValueIds, $creatingParameterValue->id);
                }

                $creatingParameter->values()->whereNotIn('id', $parameterValueIds)->delete();
            }
            $product->parameters()->whereNotIn('id', $parameterIds)->delete();
        }

//        $product->load(['categories', 'parameters.values.image', 'image', 'recommendations']);
        return ['response' => $product];
    }

    public function destroy(Request $request, Product $product)
    {
        $result = $product->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Product $product)
    {
        $product->loadRequested($request);
        return ['response' => $product];
    }

}
