<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Sale;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private array $rules = [
        'name' => 'required',
        'seo_name' => 'required',
        'text' => 'required',
        'image_id' => 'required|numeric',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Article::class, 'article');
    }

    public function index(Request $request)
    {
        $result = Article::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $article = Article::create($request->all());
        if ($request->has('media_file_ids')) {
            $article->mediaFiles()->sync($request->media_file_ids);
        }
        if ($request->has('product_ids')) {
            $article->products()->sync($request->product_ids);
        }
        return ['response' => $article];
    }

    public function update(Request $request, Article $article)
    {
        $this->validate($request, $this->rules);
        $article->update($request->only($article->getFillable()));
        if ($request->has('media_file_ids')) {
            $article->mediaFiles()->sync($request->media_file_ids);
        }

        if ($request->has('product_ids')) {
            $article->products()->sync($request->product_ids);
        }
        return ['response' => $article];
    }

    public function destroy(Request $request, Article $article)
    {
        $result = $article->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Article $article)
    {
        $article->loadRequested($request);
        return ['response' => $article];
    }

}
