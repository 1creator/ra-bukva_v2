<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mailing;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MailingController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function __construct()
    {
        $this->authorizeResource(Mailing::class, 'mailing');
    }

    public function index(Request $request)
    {
        $result = Mailing::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required|max:255',
            'targets' => 'array|min:1',
            'mail_body' => 'required',
        ]);

        $data = $request->all();

        $mailing = Mailing::create($data);

        return ['response' => $mailing];
    }
}
