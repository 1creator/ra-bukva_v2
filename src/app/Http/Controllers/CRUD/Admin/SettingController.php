<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\CRMSettings;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        $settings = CRMSettings::instance();
        return ['response' => $settings];
    }

    public function update(Request $request)
    {
        CRMSettings::instance()->update($request->all());

        $settings = CRMSettings::instance();
        return ['response' => $settings];
    }
}
