<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Promocode;
use App\Models\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PromocodeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Promocode::class, 'promocode');
    }

    public function index(Request $request)
    {
        $result = Promocode::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'sale' => 'required:numeric',
            'min_amount' => 'required:numeric',
            'max_used_count' => 'required:numeric',
            'expires_at' => 'required:date',
        ]);

        $data = $request->all();

        $id = null;
        do {
            $id = strtoupper(Str::random(8));
        } while (Promocode::where('id', $id)->exists());
        $data['id'] = $id;

        $result = Promocode::create($data);

        return ['response' => $result];
    }

    public function update(Request $request, Promocode $promocode)
    {
        $this->validate($request, [
            'sale' => 'required:numeric',
            'min_amount' => 'required:numeric',
            'max_used_count' => 'required:numeric',
            'expires_at' => 'required:date',
        ]);
        $promocode->update($request->only($promocode->getFillable()));
        return ['response' => $promocode];
    }

    public function destroy(Request $request, Promocode $promocode)
    {
        $result = $promocode->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Promocode $promocode)
    {
        $promocode->loadRequested($request);
        return ['response' => $promocode];
    }

}
