<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Sale;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Sale::class, 'sale');
    }

    public function index(Request $request)
    {
        $result = Sale::requested($request)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'media_file_id' => 'required',
            'starts_at' => 'required|date',
            'finishes_at' => 'required|date',
        ]);
        $result = Sale::create($request->all());
        return ['response' => $result];
    }

    public function update(Request $request, Sale $sale)
    {
        $this->validate($request, [
            'name' => 'required',
            'media_file_id' => 'required',
        ]);
        $sale->update($request->only($sale->getFillable()));
        return ['response' => $sale];
    }

    public function destroy(Request $request, Sale $sale)
    {
        $result = $sale->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Sale $sale)
    {
        $sale->loadRequested($request);
        return ['response' => $sale];
    }

}
