<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    private array $rules = [
        'name' => 'required|max:128',
        'director' => 'required|max:128',
        'legal_address' => 'required|max:1023',
        'mail_address' => 'required|max:1023',
        'okpo' => 'nullable|digits_between:4,14',
        'inn' => "digits_between:6,24|unique:companies",
        'kpp' => 'digits:9',
        'bank' => 'string|max:255',
        'bik' => 'digits:9',
        'rs' => 'digits:20',
        'ks' => 'digits:20',
    ];

    public function __construct()
    {
        $this->authorizeResource(Company::class, 'company');
    }

    public function index(Request $request)
    {
        $companies = Company::requested($request)->get();
        return ['response' => $companies];
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $data = $request->all();
        $data['partner_sale'] = $data['partner_sale'] ?? 0;
        $company = Auth::user()->companies()->create($data);

        return ['response' => $company];
    }

    public function show(Request $request, Company $company)
    {
        return ['response' => $company];
    }

    public function update(Request $request, Company $company)
    {
        $this->validate($request, array_merge($this->rules, [
            'inn' => "digits_between:6,24|unique:companies,inn,${company['id']}",
        ]));

        $data = $request->only($company->getFillable());
        $data['partner_sale'] = $data['partner_sale'] ?? 0;
        $company->update($data);

        return ['response' => $company];
    }
}
