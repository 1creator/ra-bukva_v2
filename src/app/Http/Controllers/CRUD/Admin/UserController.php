<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Http\Controllers\Controller;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    public function index(Request $request)
    {
        $users = User::requested($request)->get();
        return ['response' => $users];
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->only($user->getFillable()));

        if ($request->staff) {
            $staffModel = $user->staff;
            if (!$staffModel) $staffModel = new Staff(['user_id' => $user->id]);

            $staffModel->position = $request->staff['position'];
            $staffModel->save();

            $staffModel->permissions()->sync($request->staff['permissions']);
        } else if ($request->is_staff == false) {
            $user->staff()->delete();
        }

        return ['response' => $user];
    }

    public function show(Request $request, User $user)
    {
        $user->loadRequested($request);
        return ['response' => $user];
    }

    public function getSelf()
    {
        return ['response' => Auth::user()];
    }

    public function updateSelf(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'email' => ['sometimes', 'string', 'email', 'max:255',
                "unique:users,email,${user['id']}"],
            'password' => ['sometimes', 'string', 'min:6', 'confirmed'],
        ]);

        $data = $request->only($user->getFillable());

        if (array_key_exists('password', $data)) {
            $data['password'] = Hash::make($data['password']);
        }

        $user->update($data);

        return ['response' => $user];
    }
}
