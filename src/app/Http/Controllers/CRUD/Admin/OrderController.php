<?php

namespace App\Http\Controllers\CRUD\Admin;

use App\Events\OrderChanged;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\CRMSettings;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Permission;
use App\Notifications\OrderCreated;
use App\Notifications\OrderReady;
use App\Rules\PromocodeUsageCount;
use App\Rules\PromocodeUsageMinAmount;
use App\Rules\PromocodeUsagePeriod;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Order::class, 'order');
    }

    public function index(Request $request)
    {
        $result = Order::requested($request);

        $userPermissions = Auth::user()->staff->permissions;
        if (!$userPermissions->contains(Permission::ORDERS_VIEW) && $userPermissions->contains(Permission::ORDERS_VIEW_FOR_DESIGN)) {
            $result->whereHas('orderProducts', function ($query) {
                $query->where('status', 'layout')
                    ->orWhere('status', 'layout_rejected')
                    ->orWhere('status', 'layout_agreement');
            });
        }

        $result = $result->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:15',
            'order_products' => 'required|array|min:1',
            'company_id' => 'sometimes|exists:companies,id',
        ]);

        $calculations = app(OrderService::class)->calculateProducts($request['order_products']);
        $total = array_sum(array_column($calculations, 'total'));

        $this->validate($request, [
            'promocode_id' => [
                'bail',
                'nullable',
                'exists:promocodes,id',
                new PromocodeUsageCount,
                new PromocodeUsagePeriod,
                new PromocodeUsageMinAmount($total),
            ],
        ]);

        $data = $request->all();
        $data['secret'] = Str::random(12);

        if ($request->filled('company_id')) {
            $company = Company::findOrFail($request->company_id);

            $data['company_id'] = $company->id;
            if ($company->partner_sale) {
                $data['partner_sale'] = $company->partner_sale;
            }
        }

        $order = Order::create($data);

        app(OrderService::class)->createOrderProducts($order, $request['order_products'], $calculations);

        if ($order->delivery_price > 0) {
            $deliveryFreeFrom = CRMSettings::instance()->free_delivery_from;
            if ($order->itemsTotal >= $deliveryFreeFrom) {
                $order->delivery_price = 0;
                $order->save();
            }
        }

        if ($request->offer) {
            OrderProduct::where('order_id', $order->id)->update(['status' => 'offer']);
        } else if ($order->email) {
            Notification::route('mail', $order->email)
                ->notify(new OrderCreated($order));
        }

        event(new OrderCreated($order));
        return ['response' => $order];
    }

    public function update(Request $request, Order $order)
    {
        //todo add authorize
//        $this->authorize('update', $order);

        if ($request->filled('order_products')) {
            $this->validate($request, [
                'order_products' => 'array|min:1'
            ]);

            $order->orderProducts()->delete();

            $calculations = app(OrderService::class)->calculateProducts($request['order_products']);
            app(OrderService::class)->createOrderProducts($order, $request['order_products'], $calculations);
        }

        $this->validate($request, [
            'promocode_id' => [
                'bail',
                'nullable',
                'exists:promocodes,id',
                new PromocodeUsageCount,
                new PromocodeUsagePeriod,
                new PromocodeUsageMinAmount($order->total),
            ],
            'company_id' => 'sometimes|nullable|exists:companies,id',
        ]);

        $order->update($request->only($order->getFillable()));

        $order->refresh();
        $order->load(['orderProducts.product', 'company']);

        event(new OrderChanged($order));
        return ['response' => $order];
    }

    public function destroy(Request $request, Order $order)
    {
        $result = $order->delete();
        return ['response' => $result];
    }

    public function show(Request $request, Order $order)
    {
        $order->loadRequested($request);
        return ['response' => $order];
    }

    public function notify(Request $request, Order $order)
    {
        $channels = [];
        $inputChannels = $request->input('channels', ['mail']);
        if (in_array('mail', $inputChannels)) array_push($channels, OrderReady::VIA_MAIL);
        if (in_array('sms', $inputChannels)) array_push($channels, OrderReady::VIA_SMS);
        $notification = new OrderReady($order, $channels);

        Notification::route('mail', $order->email)
            ->route('smscru', $order->phone)
            ->notify($notification);

        return ['response' => 1];
    }
}
