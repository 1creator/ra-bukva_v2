<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\Controller;
use App\Models\MediaFile;
use App\Notifications\Admin\CallbackRequested;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class CallbackController extends Controller
{
    public function store(Request $r)
    {
        Notification::route('viber', env('VIBER_ADMIN_ID'))
            ->notify(new CallbackRequested(['name' => $r->name, 'phone' => $r->phone]));

        return response(['response' => true]);
    }
}
