<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function __construct()
    {
    }

    public function index()
    {
        $companies = Auth::user()->companies;
        return ['response' => $companies];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:128',
            'director' => 'required|max:128',
            'legal_address' => 'required|max:1023',
            'mail_address' => 'required|max:1023',
            'okpo' => 'nullable|digits_between:4,14',
            'inn' => "digits_between:6,24|unique:companies",
            'kpp' => 'digits:9',
            'bank' => 'string|max:255',
            'bik' => 'digits:9',
            'rs' => 'digits:20',
            'ks' => 'digits:20',
        ]);

        $data = $request->all();
        $data['partner_sale'] = 0;

        if ($request->partner) {
            $data['partner_status'] = 'requested';
        }

        $company = Auth::user()->companies()->create($data);

        return ['response' => $company];
    }

    public function show(Request $request, Company $company)
    {
        return ['response' => $company];
    }

    public function update(Request $request, Company $company)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'director' => 'required|max:255',
            'legal_address' => 'required|max:1023',
            'mail_address' => 'required|max:1023',
            'okpo' => 'nullable|digits_between:4,14',
            'inn' => "digits_between:6,24|unique:companies,inn,${company['id']}",
            'kpp' => 'digits:9',
            'bank' => 'string',
            'bik' => 'digits:9',
            'rs' => 'digits:20',
            'ks' => 'digits:20',
        ]);


        $data = $request->only($company->getFillable());

        if ($request->partner && ($company->partner_status != 'accepted')) {
            $data['partner_status'] = 'requested';
        }

        $company->update($data);

        return ['response' => $company];
    }
}
