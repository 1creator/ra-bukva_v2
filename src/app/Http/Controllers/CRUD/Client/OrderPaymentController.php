<?php


namespace App\Http\Controllers\CRUD\Client;


use App\Models\Order;
use App\Services\Payment\AnonymousPayer;
use App\Services\Payment\Interfaces\PaymentInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class OrderPaymentController
{
    public function getForm(Request $request, Order $order)
    {
        if (!$order->orderProducts->every(fn($item) => $item->status == 'payment'))
            throw ValidationException::withMessages([
                'order_products' => 'Оплата невозможна, так как не все позиции заказа в статусе "ждёт оплаты"'
            ]);

        $user = Auth::user();
        if (!$user) {
            $user = new AnonymousPayer(null, $order->phone, $order->email);
        }

        $payment = app(PaymentInterface::class, ['payer' => $user])
            ->makeIframe($order->total, "Оплата заказа ${order['id']}");
        $order->payment_id = $payment->id;
        $order->save();
        return $payment->confirmation->confirmation_token;
    }
}
