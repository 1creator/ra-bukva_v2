<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function __construct()
    {
    }

    public function getSelf()
    {
        return ['response' => Auth::user()->profile()];
    }

    public function updateSelf(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'email' => ['sometimes', 'string', 'email', 'max:255',
                "unique:users,email,${user['id']}"],
            'password' => ['sometimes', 'string', 'min:6', 'confirmed'],
        ]);

        $data = $request->only($user->getFillable());

//        if (array_key_exists('birthday', $data)) {
//            try {
//                Carbon::parse($date);
//            } catch (\Exception $e) {
//                echo 'invalid date, enduser understands the error message';
//            }
//        }

        if (array_key_exists('password', $data)) {
            $data['password'] = Hash::make($data['password']);
        }

        $user->update($data);

        return ['response' => $user];
    }

    public function getBonusChanges(Request $request)
    {
        $user = Auth::user();

        $changes = $user->bonusBalanceChanges()->with(['order', 'orderProduct.product'])->get();

        return ['response' => $changes];
    }
}
