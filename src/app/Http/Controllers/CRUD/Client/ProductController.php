<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $result = Product::requested($request)->get();
        return ['response' => $result];
    }

    public function show(Request $request, Product $product)
    {
        $product->loadRequested($request);
        return ['response' => $product];
    }

}
