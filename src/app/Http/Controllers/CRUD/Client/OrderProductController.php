<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProductLayout;
use App\Models\Product;
use App\Models\ProductParameterValue;
use App\Notifications\Admin\OrderProductStatusChanged;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OrderProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(OrderProduct::class, 'orderProduct');
    }

    public function index(Request $request)
    {

        $result = OrderProduct::requested($request)->get();
        return ['response' => $result];
    }

    public function update(Request $request, OrderProduct $orderProduct)
    {
        $orderProduct->update($request->only(['status', 'need_layout']));
        return ['response' => $orderProduct];
    }

    public function review(Request $request, OrderProduct $orderProduct)
    {
        foreach ($request->reviews as $key => $fields) {
            $model = OrderProductLayout::findOrFail($key);
            $model->update($fields);
        }

        $newStatus = $request->status;
        $orderProduct->update(['status' => $newStatus]);

        Notification::route('viber', env('VIBER_ADMIN_ID'))
            ->notify(new OrderProductStatusChanged($orderProduct));

        return ['response' => $orderProduct];
    }

    public function storeLayouts(Request $request, OrderProduct $orderProduct)
    {
        $orderProduct->layoutMediaFiles()->syncWithoutDetaching($request->media_file_ids);

        if ($request->status) {
            $orderProduct->update(['status' => $request->status]);
        }

        $newLayouts = $orderProduct->layouts()
            ->whereIn('media_file_id', $request->media_file_ids)->with('mediaFile')->get();

        Notification::route('viber', env('VIBER_ADMIN_ID'))
            ->notify(new OrderProductStatusChanged($orderProduct));

        return ['response' => [
            'status' => $orderProduct->status,
            'layouts' => $newLayouts
        ]];
    }
}
