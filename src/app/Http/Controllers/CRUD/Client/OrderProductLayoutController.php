<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use App\Models\MediaFile;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProductLayout;
use App\Models\Product;
use App\Models\ProductParameterValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrderProductLayoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(Request $request)
    {
//        $result = OrderProduct::requested($request)->get();
//        return ['response' => $result];
    }

    public function update(Request $request, OrderProductLayout $orderProductLayout)
    {
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'order_product_id' => 'required',
            'file' => 'required|file',
        ]);

        $path = $request->file('file')->store('/public/files');
        $file = MediaFile::create([
            'full_size' => Storage::url($path),
            'type' => 'image',
        ]);

        $file['pivot'] = OrderProductLayout::create([
            'order_product_id' => $request->order_product_id,
            'media_file_id' => $file->id
        ]);


        return ['response' => $file];
    }

    public function destroy(Request $request, OrderProductLayout $orderProductLayout)
    {
//        return ['response' => $orderProductLayout->delete()];
    }
}
