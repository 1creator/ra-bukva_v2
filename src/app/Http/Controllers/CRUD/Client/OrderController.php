<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use App\Mail\OrderDocuments;
use App\Mail\OrderInvoice;
use App\Models\BonusBalanceChange;
use App\Models\Company;
use App\Models\CRMSettings;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Notifications\Admin\OrderCreated as AdminOrderCreated;
use App\Notifications\OrderCreated;
use App\Rules\PromocodeUsageCount;
use App\Rules\PromocodeUsageMinAmount;
use App\Rules\PromocodeUsagePeriod;
use App\Services\Order\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Order::class, 'order');
    }

    public function index(Request $request)
    {
        $this->middleware('auth');
        $result = Order::requested($request)->where('user_id', Auth::user()->id)->get();
        return ['response' => $result];
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'order_products' => 'required|array|min:1',
            'name' => 'required|string|min:3|max:15',
            'email' => 'required|email',
            'phone' => 'required|string|max:20',
        ]);

        $calculations = app(OrderService::class)->calculateProducts($request['order_products']);
        $total = array_sum(array_column($calculations, 'total'));


        $this->validate($request, [
            'promocode_id' => [
                'bail',
                'nullable',
                'exists:promocodes,id',
                new PromocodeUsageCount,
                new PromocodeUsagePeriod,
                new PromocodeUsageMinAmount($total),
            ],
        ]);

        $user = Auth::user();

        $data = $request->all();
        $data['secret'] = Str::random(12);

        if ($user) {
            $data['user_id'] = $user->id;
            $this->validate($request, [
                'bonuses_spent' => 'sometimes|max:' . $user->bonus_balance,
            ]);
        }

        if ($request->filled('company')) {
            $this->validate($request, [
                'company.inn' => 'digits_between:6,24',
            ]);

            $company = $request->company;
            $company = Company::where('inn', $company['inn'])->first();

            if (!$company) {
                $this->validate($request, [
                    'company.name' => 'required|max:255',
                    'company.director' => 'required|max:255',
                    'company.legal_address' => 'required|max:1023',
                    'company.mail_address' => 'required|max:1023',
                    'company.okpo' => 'nullable|digits_between:4,14',
                    'company.inn' => "digits_between:6,24|unique:companies,inn",
                    'company.kpp' => 'digits:9',
                    'company.bank' => 'string|max:255',
                    'company.bik' => 'digits:9',
                    'company.rs' => 'digits:20',
                    'company.ks' => 'digits:20',
                ]);

                $company = Company::create($request->get('company'));
            }

            $data['company_id'] = $company->id;
            if ($company->partner_sale) {
                $data['partner_sale'] = $company->partner_sale;
            }
        }

        $order = Order::create($data);

        app(OrderService::class)->createOrderProducts($order, $request['order_products'], $calculations);

        if ($user) {
            $now = Carbon::now();

            if ($request->bonuses_spent > 0) {
                BonusBalanceChange::create([
                    'user_id' => $user->id,
                    'type' => 'sub',
                    'value' => $request->bonuses_spent,
                    'order_id' => $order->id,
                    'created_at' => $now,
                ]);
            }
        }

        if ($order->delivery_price > 0) {
            $deliveryFreeFrom = CRMSettings::instance()->free_delivery_from;
            if ($order->itemsTotal >= $deliveryFreeFrom) {
                $order->delivery_price = 0;
                $order->save();
            }
        }

        if ($request->offer) {
            OrderProduct::where('order_id', $order->id)->update(['status' => 'offer']);
        } else {
            Notification::route('mail', $order->email)
                ->notify(new OrderCreated($order));
        }

        Notification::route('viber', env('VIBER_ADMIN_ID'))
            ->notify(new AdminOrderCreated($order));

        $order->load('orderProducts');
        return ['response' => $order];
    }

    public function update(Request $request, Order $order)
    {
        $this->authorize('update', $order);

        $order->update($request->only(['comment', 'email', 'phone', 'delivery_address']));

        if ($request->filled('company')) {
            $this->validate($request, [
                'company.inn' => 'digits_between:6,24',
            ]);

            $companyInn = $request->get('company')['inn'];
            $company = Company::where('inn', $companyInn)->first();

            if (!$company) {
                $this->validate($request, [
                    'company.name' => 'required|max:64',
                    'company.director' => 'required|max:64',
                    'company.legal_address' => 'required',
                    'company.inn' => "digits_between:6,24|unique:companies,inn",
                    'company.kpp' => 'digits:9',
                    'company.bank' => 'string',
                    'company.bik' => 'digits:9',
                    'company.rs' => 'digits:20',
                    'company.ks' => 'digits:20',
                ]);

                $company = Company::create($request->get('company'));
            }

            $data['company_id'] = $company->id;
            if ($company->partner_sale) {
                $data['partner_sale'] = $company->partner_sale;
            }
        }

        if ($request->filled('order_products')) {
            $this->validate($request, [
                'order_products' => 'array|min:1'
            ]);

            $order->orderProducts()->delete();

            $calculations = app(OrderService::class)->calculateProducts($request['order_products']);
            app(OrderService::class)->createOrderProducts($order, $request['order_products'], $calculations);
        }

        return ['response' => $order];
    }

    public function destroy(Request $request, Order $order)
    {
        $result = $order->delete();

        $user = Auth::user();

        return ['response' => $result];
    }

    public function show(Request $request, Order $order)
    {
        $order->loadRequested($request);
        return ['response' => $order];
    }

    public function sendInvoiceToEmail(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        $this->validate($request, [
            'to' => 'email|required'
        ]);

        $order->load(['user', 'company']);
        Mail::to($request->to)->send(new OrderInvoice($order));

        return response(['response' => 1]);
    }

    public function sendDocumentsToEmail(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        $this->validate($request, [
            'to' => 'email|required'
        ]);

        $order->load(['user', 'company']);
        Mail::to($request->to)->send(new OrderDocuments($order, $request->documents));

        return response(['response' => 1]);
    }
}
