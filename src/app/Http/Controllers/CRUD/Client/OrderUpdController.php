<?php


namespace App\Http\Controllers\CRUD\Client;


use App\Models\MediaFile;
use App\Models\Order;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class OrderUpdController
{
    public function store(Request $request, Order $order)
    {
        try {
            $attachment = MediaFile::findOrFail($request['attachment_id']);
            $content = Storage::disk($attachment->disk)->get($attachment->path);
            $datetime = $order->created_at->format('d-m-yy');
            $fileName = "Заказ ${order['id']} от $datetime." . File::extension($attachment['path']);
            $fileName = Str::ascii($fileName);
            $path = "orders/${order['id']}/${fileName}";
            Storage::disk('local')->put($path, $content);
            $order['client_upd'] = Storage::disk('local')->path($path);
            $order->save();
            $attachment->delete();

            return 1;
        } catch (FileNotFoundException $e) {
            abort(500, 'Ошибка на сервере');
        }
    }
}
