<?php

namespace App\Http\Controllers\CRUD\Client;

use App\Http\Controllers\Controller;
use App\Mail\OrderDocuments;
use App\Mail\OrderInvoice;
use App\Models\BonusBalanceChange;
use App\Models\Company;
use App\Models\CRMSettings;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Notifications\Admin\OrderCreated as AdminOrderCreated;
use App\Notifications\OrderCreated;
use App\Rules\PromocodeUsageCount;
use App\Rules\PromocodeUsageMinAmount;
use App\Rules\PromocodeUsagePeriod;
use App\Services\Order\OrderService;
use App\Services\Review\ReviewService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class OrderReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Order::class, 'order');
    }

    public function store(Request $request, Order $order)
    {
        app(ReviewService::class)->storeReview($order, $request->all());
    }
}
