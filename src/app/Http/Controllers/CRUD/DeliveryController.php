<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\Controller;
use App\Models\CRMSettings;
use App\Models\MediaFile;
use App\Notifications\Admin\CallbackRequested;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class DeliveryController extends Controller
{
    public function index(Request $r)
    {
        $info = CRMSettings::instance();
        return response(['response' => [
            'zones' => $info->delivery_zones,
            'free_delivery_from' => $info->free_delivery_from,
        ]]);
    }
}
