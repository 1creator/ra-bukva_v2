<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Product;
use App\Models\Sale;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function show(Request $request, Article $article)
    {
        $sliderProducts = Product::all();
        return view('article', ['article' => $article, 'sliderProducts' => $sliderProducts]);
    }

}
