<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\Review\ReviewService;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::active()->take(12);
        return view('welcome', ['products' => $products]);
    }

    public function show(Product $product)
    {
        $reviews = app(ReviewService::class)->getReviewsForProduct($product);
        $rating = $reviews->map(function ($review) use ($product) {
            return $review['items']->firstWhere('product_id', $product->id)['rating'];
        })->average();

        $product->load(['parameters.values', 'image', 'projects', 'mediaFiles']);


        return view('product', [
            'product' => $product,
            'average_rating' => number_format($rating, 1),
            'calculator_data' => json_encode(keysToCamelCase($product->toArray())),
            'reviews' => $reviews
        ]);
    }

}
