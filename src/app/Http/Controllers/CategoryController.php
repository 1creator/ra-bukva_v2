<?php

namespace App\Http\Controllers;

use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $categories = Category::whereNull('parent_id')->active()->get();
        return view('categories', ['categories' => $categories]);
    }

    public function show(Category $category)
    {
        $category->load([
            'children' => function ($query) {
                $query->active();
            },
            'products' => function ($query) {
                $query->active();
            }]);
        return view('category', ['category' => $category]);
    }
}
