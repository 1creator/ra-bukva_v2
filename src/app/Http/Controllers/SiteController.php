<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CRMSettings;
use App\Models\Product;
use App\Models\Sale;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $products = Product::accented()->take(12)->get();
        return view('welcome', ['products' => $products]);
    }

    public function sales()
    {
        $bonusBet = CRMSettings::instance()['bonus_bet'] * 100;
        return view('sales', ['bonusBet' => $bonusBet]);
    }

    public function delivery()
    {
        $crmSettings = CRMSettings::instance();
        $deliveryZones = $crmSettings->delivery_zones;
        $freeDeliveryFrom = $crmSettings->free_delivery_from;
        return view('delivery', [
            'deliveryZones' => $deliveryZones,
            'freeDeliveryFrom' => $freeDeliveryFrom,
        ]);
    }

    public function payment()
    {
        return view('payment');
    }

    public function contacts()
    {
        return view('contacts');
    }

    public function userCabinet()
    {
        return view('layouts.cabinet');
    }

    public function catalog()
    {
        return view('catalog');
    }

    public function checkout()
    {
        $deliveryInfo = CRMSettings::instance();
        return view('checkout', [
            'deliveryZones' => $deliveryInfo->delivery_zones,
            'freeDeliveryFrom' => $deliveryInfo->free_delivery_from,
        ]);
    }

    public function terms()
    {
        return view('terms');
    }

    public function layoutRequirements()
    {
        return view('layout-requirements');
    }

    public function sitemap()
    {
        //->toAtomString()
        $categories = Category::active()->get();
        $products = Product::active()->get();

        $pages = [
            [
                'loc' => url('/'),
            ],
            [
                'loc' => url('/catalog'),
            ],
            [
                'loc' => url('/sales'),
            ],
            [
                'loc' => url('/payment'),
            ],
            [
                'loc' => url('/delivery'),
            ],
            [
                'loc' => url('/contacts'),
            ],
            [
                'loc' => url('/terms'),
            ],
        ];

        foreach ($categories as $category) {
            $page = [
                'loc' => route('categories.show', ['category' => $category->seo_name]),
            ];
            if ($category->updated_at) {
                $page['lastmod'] = $category->updated_at->toAtomString();
            }

            array_push($pages, $page);
        }

        foreach ($products as $product) {
            $page = [
                'loc' => route('products.show', ['product' => $product->seo_name]),
            ];
            if ($product->updated_at) {
                $page['lastmod'] = $product->updated_at->toAtomString();
            }

            array_push($pages, $page);
        }

        return response()
            ->view('sitemap',
                ['pages' => $pages],
                200,
                ['Content-Type' => 'text/xml']);
    }

    public function yml()
    {
        //->toAtomString()
        $categories = Category::active()->get();
        $products = Product::active()->get();

        return response()
            ->view('yml',
                ['categories' => $categories, 'products' => $products],
                200,
                ['Content-Type' => 'text/xml']);
    }
}
