<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Permission;
use App\Services\DocGenerator\DocGeneratorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function show(Request $request, Order $order)
    {
        $this->authorize('view', $order);
        return view('layouts.cabinet', ['order' => $order]);
    }

    public function invoiceHtml(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return app(DocGeneratorService::class)->getInvoiceAsHtml($order);
    }

    public function updHtml(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return app(DocGeneratorService::class)->getUPDAsHtml($order);
    }

    public function emptyUpd(Request $request, Order $order)
    {
        $this->authorize('view', $order);
        $path = app(DocGeneratorService::class)->getUPDAsPdf($order, false);
        $fileName = "Заказ ${order['id']} от " . $order->created_at->format('d-m-yy') . ".pdf";
        return response()->download(storage_path('app/' . $path), $fileName);
    }

    public function updPdf(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        if (!$order->client_upd && (!Auth::user() || !Auth::user()->staff)) {
            return redirect(route('orders.upload-upd', ['order' => $order->id, 'secret' => $order->secret]));
        }

        return Storage::response(app(DocGeneratorService::class)->getUPDAsPdf($order));
    }

    public function clientUpd(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return response()->download($order['client_upd']);
    }

    public function uploadUpd(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return view('layouts.cabinet');
    }

    public function invoicePdf(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return Storage::response(app(DocGeneratorService::class)->getInvoiceAsPdf($order));
    }

    public function offerHtml(Request $request, Order $order)
    {
        $this->authorize('view', $order);
        return app(DocGeneratorService::class)->getOfferAsHtml($order);

        return Storage::response(app(DocGeneratorService::class)->getOfferAsPdf($order));
    }

    public function offerPdf(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return Storage::response(app(DocGeneratorService::class)->getOfferAsPdf($order));
    }

    public function contractDocx(Request $request, Order $order)
    {
        $this->authorize('view', $order);

        return Storage::download(app(DocGeneratorService::class)->getContractAsDocx($order));
    }

    public function exportUPDArchive(Request $request)
    {
        $this->authorize('export-upd');

        $from = $request->get('from');
        $to = $request->get('to');
        $archive = app(DocGeneratorService::class)->exportUpdArchive($from, $to);
        return response()->download($archive)->deleteFileAfterSend();
    }
}
