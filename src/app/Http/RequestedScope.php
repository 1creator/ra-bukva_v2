<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 27.12.2018
 * Time: 23:19
 */

namespace App\Http;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @method static Builder requested(Request $request)
 * @method public Model requested(Request $request)
 */
class RequestedScope extends Model
{
    protected $availableWith = [];

    public function scopeRequested(Builder $query, Request $request)
    {
        if (is_array($request->with)) {
            foreach ($request->with as $item) {
                if (in_array($item, $this->availableWith)) {
                    $query->with($item);
                }
            }
        }

        if (is_array($request->with_count)) {
            foreach ($request->with_count as $item) {
                $query->withCount($item);
            }
        }

        if ($request->offset || $request->limit) {
            $query->offset($request->offset ? $request->offset : 0);
            $query->limit($request->limit ? $request->limit : 10);
        }

        if ($request->order_by) {
            $arr = explode(":", $request->order_by);
            if (!($this->orderBy($query, $arr[0], $arr[1])))
                $this->orderByDefault($query, $arr[0], $arr[1]);
        }

        if ($request->filters) {
            foreach ($request->filters as $params) {
                $params = json_decode($params);
                if (!$this->filter($query, $params))
                    $this->filterDefault($query, $params);
            }
        }

        return $query;
    }

    private function orderByDefault(Builder $query, $prop, $direction)
    {
        $query->orderBy($prop, $direction ? $direction : 'asc');
    }

    private function filterDefault(Builder $query, $params)
    {
        $query->where($params[0], $params[1], $params[2], $params[3] ?? 'and');
    }

    protected function orderBy(Builder $query, $prop, $direction)
    {
        return false;
    }

    protected function filter(Builder $query, $params)
    {
        return false;
    }

    public function loadRequested(Request $request)
    {
        if ($request->with) {
            $items = [];
            foreach ($request->with as $item) {
                if (in_array($item, $this->availableWith)) {
                    array_push($items, $item);
                }
            }
            $this->load($items);
        }

        if ($request->with_count) {
            foreach ($request->with_count as $item) {
                $this->loadCount($item);
            }
        }

        return $this;
    }
}
