<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Observers\OrderObserver;
use App\Observers\OrderProductObserver;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        setlocale(LC_ALL, 'ru_RU.utf8');
        Carbon::setLocale('ru_RU');

        Order::observe(OrderObserver::class);
        OrderProduct::observe(OrderProductObserver::class);
    }
}
