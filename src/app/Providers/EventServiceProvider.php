<?php

namespace App\Providers;

use App\Events\OrderChanged;
use App\Listeners\NotificationSendingListener;
use App\Listeners\OrderChangedListener;
use App\Listeners\PaymentListener;
use App\Services\Payment\Events\PaymentRefund;
use App\Services\Payment\Events\PaymentSucceeded;
use App\Services\Review\ReviewCreated;
use App\Services\Review\ReviewEventListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Notifications\Events\NotificationSending;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NotificationSending::class => [
            NotificationSendingListener::class,
        ],
        PaymentSucceeded::class => [
            PaymentListener::class,
        ],
        PaymentRefund::class => [
            PaymentListener::class,
        ],
        OrderChanged::class => [
            OrderChangedListener::class,
        ],
        ReviewCreated::class => [
            ReviewEventListener::class,
        ],
//        Registered::class => [
//            SendEmailVerificationNotification::class,
//        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
