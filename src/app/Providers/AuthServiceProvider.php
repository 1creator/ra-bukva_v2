<?php

namespace App\Providers;

use App\Models\OrderProduct;
use App\Models\Permission;
use App\Policies\OrderProductPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        OrderProduct::class => OrderProductPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::guessPolicyNamesUsing(function ($modelClass) {
            return 'App\\Policies\\' . class_basename($modelClass) . 'Policy';
        });

        Gate::define('export-upd', function ($user) {
            return $user && $user->staff && $user->staff->permissions->contains(Permission::STATISTIC_VIEW);
        });

        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->email == 'admin@ra-bukva.ru') {
                return true;
            }
        });
    }
}
