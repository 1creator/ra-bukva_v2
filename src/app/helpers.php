<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 24.06.2019
 * Time: 2:32
 */


function keysToCamelCase($array)
{
    $arr = [];
    foreach ($array as $key => $value) {
        $key = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));

        if (is_array($value))
            $value = keysToCamelCase($value);

        $arr[$key] = $value;
    }
    return $arr;
}
