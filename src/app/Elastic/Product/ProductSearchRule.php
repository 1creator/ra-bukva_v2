<?php


namespace App\Elastic\Product;


use ScoutElastic\SearchRule;

class ProductSearchRule extends SearchRule
{
    public function buildQueryPayload()
    {
        return [
            'must' => [
                'multi_match' => [
                    'query' => $this->builder->query,
                    'fields' => ['name'],
                    'fuzziness' => 'auto',
                ]
            ]
        ];
    }
}
