<?php

namespace App\Elastic\Product;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class ProductIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    /**
     * @var array
     */
    protected $settings = [
        "analysis" => [
            "filter" => [
                "russian_stop" => [
                    "type" => "stop",
                    "stopwords" => "_russian_"
                ],
                "russian_stemmer" => [
                    "type" => "stemmer",
                    "language" => "russian"
                ],
                "snowball" => [
                    "type" => "snowball",
                    "language" => "Russian"
                ],
            ],
            "analyzer" => [
                "rebuilt_russian" => [
                    "tokenizer" => "standard",
                    "filter" => [
                        "lowercase",
                        "russian_morphology",
                        "english_morphology",
                        "russian_stop",
//                        "russian_keywords",
//                        "russian_stemmer",
                    ]
                ]
            ]
        ]
//        'analysis' => [
//            'analyzer' => [
//                'my_html_analyzer' => [
//                    'type' => 'custom',
//                    'tokenizer' => 'standard',
//                    "char_filter" => [
//                        "html_strip",
//                    ]
//                ]
//            ]
//        ]
    ];
}
