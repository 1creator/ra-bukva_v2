<?php

namespace App\Notifications\Admin;

use App\Models\OrderProduct;
use App\Services\Viber\ViberChannel;
use App\Services\Viber\ViberNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderProductStatusChanged extends Notification implements ShouldQueue, ViberNotification
{
    use Queueable;

    private OrderProduct $orderProduct;

    /**
     * Create a new notification instance.
     *
     * @param OrderProduct $orderProduct
     */
    public function __construct(OrderProduct $orderProduct)
    {
        $this->orderProduct = $orderProduct;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ViberChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toText($notifiable)
    {
        $message = "Обновлён статус заказа!";
        $message .= "\nID Заказа: " . $this->orderProduct->order->id;
        $message .= "\nПозиция заказа: " . $this->orderProduct->product->name;
        $message .= "\nНовый статус: " . __('order.' . $this->orderProduct->status);

        $message .= "\n\n" . url('/admin/orders/' . $this->orderProduct->order->id);

        return $message;
    }

    public function toViber($notifiable)
    {
        return $this->toText($notifiable);
    }
}
