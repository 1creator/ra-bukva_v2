<?php

namespace App\Notifications\Admin;

use App\Services\Viber\ViberChannel;
use App\Services\Viber\ViberNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CallbackRequested extends Notification implements ShouldQueue, ViberNotification
{
    use Queueable;

    private array $payload;

    /**
     * Create a new notification instance.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ViberChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toText($notifiable)
    {
        $message = "Заполнена форма обратной связи на сайте!\n";

        $message .= "\nИмя клиента: " . $this->payload['name'];
        $message .= "\nТелефон клиента: " . $this->payload['phone'];

        return $message;
    }

    public function toViber($notifiable)
    {
        return $this->toText($notifiable);
    }
}
