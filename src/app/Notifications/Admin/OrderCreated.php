<?php

namespace App\Notifications\Admin;

use App\Models\Order;
use App\Services\Viber\ViberChannel;
use App\Services\Viber\ViberNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderCreated extends Notification implements ShouldQueue, ViberNotification
{
    use Queueable;

    private Order $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ViberChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Заказ успешно создан')
            ->markdown('mail.orders.created', ['order' => $this->order]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toText($notifiable)
    {
        $message = "Создан новый заказ!\nID Заказа: " . $this->order->id . "\nСумма заказа: " . $this->order->total . "₽";

        $message .= "\n\nТелефон клиента: " . $this->order->phone;
        $message .= "\nEmail клиента: " . $this->order->email;

        foreach ($this->order->orderProducts as $orderProduct) {
            $message .= "\n\n" . $orderProduct->product->name . ($orderProduct->need_layout ? ' + макет' : '') . ': ' .
                $orderProduct->count . ' ' . $orderProduct->product->count_type;
        }

        $message .= "\n\n" . url('/admin/orders/' . $this->order->id);

        return $message;
    }

    public function toViber($notifiable)
    {
        return $this->toText($notifiable);
    }
}
