<?php

namespace App\Notifications;

use App\Models\Order;
use App\Services\DocGenerator\DocGeneratorService;
use App\Utils\AccumulateNotification;
use Illuminate\Notifications\Messages\MailMessage;

class OrderChanged extends AccumulateNotification
{
    private Order $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->accumulationKey = 'orders.' . $this->order->id . '.change';

        parent::__construct();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $onPayment = $this->order->orderProducts->every(fn($value, $key) => $value->status == 'payment');
        $attachInvoice = $onPayment && $this->order->company_id;

        $message = (new MailMessage)
            ->subject('Изменения в вашем заказе')
            ->view('mail.orders.status-changed', [
                'order' => $this->order,
                'onPayment' => $onPayment,
                'attachInvoice' => $attachInvoice,
            ]);

        if ($attachInvoice) {
            $invoicePdf = app(DocGeneratorService::class)->getInvoiceAsPdf($this->order);
            $invoicePdf = storage_path('app/' . $invoicePdf);
            $message->attach($invoicePdf);
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
