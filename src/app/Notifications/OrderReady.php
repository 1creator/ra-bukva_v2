<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\SmscRu\SmscRuChannel;
use NotificationChannels\SmscRu\SmscRuMessage;

class OrderReady extends Notification implements ShouldQueue
{
    use Queueable;

    private Order $order;
    private array $channels;

    const VIA_MAIL = 'mail';
    const VIA_SMS = SmscRuChannel::class;


    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param string[] $channels
     */
    public function __construct(Order $order, $channels = [self::VIA_MAIL])
    {
        $this->order = $order;
        $this->channels = $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Заказ успешно выполнен')
            ->view('mail.orders.ready', ['order' => $this->order]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toSmscRu($notifiable)
    {
        $id = $this->order->id;
        $link = $this->order->secret_link;
        return SmscRuMessage::create("Заказ №${id} успешно выполнен: ${link}");
    }
}
