<?php

namespace App\Mail;

use App\Models\Order;
use App\Services\DocGenerator\DocGeneratorService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderDocuments extends Mailable
{
    use Queueable, SerializesModels;

    private $order, $documents;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     * @param array $documents
     */
    public function __construct(Order $order, array $documents)
    {
        $this->order = $order;
        $this->documents = $documents;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $docHelper = app(DocGeneratorService::class);

        $this->subject("Документы к заказу");

        $docNames = [];
        foreach ($this->documents as $document) {
            switch ($document) {
                case 'invoice':
                    $invoicePdf = $docHelper->getInvoiceAsPdf($this->order);
                    $invoicePdf = storage_path('app/' . $invoicePdf);
                    $this->attach($invoicePdf);
                    array_push($docNames, 'Счёт на оплату');
                    break;
                case 'contract':
                    $contract = $docHelper->getContractAsDocx($this->order);
                    $contract = storage_path('app/' . $contract);
                    $this->attach($contract);
                    array_push($docNames, 'Договор оказания услуг');
                    break;
            }
        }

        $this->view('mail.orders.documents', ['order' => $this->order, 'docNames' => $docNames]);

        return $this;
    }
}
