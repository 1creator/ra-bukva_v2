<?php

namespace App\Mail;

use App\Models\Order;
use App\Services\DocGenerator\DocGeneratorService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderInvoice extends Mailable
{
    use Queueable, SerializesModels;

    private Order $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $invoicePdf = app(DocGeneratorService::class)->getInvoiceAsPdf($this->order);
        $invoicePdf = storage_path('app/' . $invoicePdf);

        return $this->markdown('mail.orders.invoice', ['order' => $this->order])
            ->attach($invoicePdf)
            ->subject("Счёт на оплату заказа");
    }
}
