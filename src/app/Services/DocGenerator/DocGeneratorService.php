<?php


namespace App\Services\DocGenerator;

use App\Models\CRMSettings;
use App\Models\Order;
use App\Services\QrCode\QrCodeGeneratorInterface;
use Barryvdh\DomPDF\Facade as DomPDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\TemplateProcessor;
use ZipArchive;

class DocGeneratorService
{
    private function makeProductsTableXml(Order $order)
    {
        $document_with_table = new PhpWord();
        $section = $document_with_table->addSection();
        $table = $section->addTable([
            'width' => 100 * 50,
            'unit' => TblWidth::PERCENT,
        ]);
        $styleFontHeader = [
            'bold' => true,
            'size' => 12
        ];
        $pStyle = [
            'indent' => 0
        ];
        $styleCell = [
            'borderColor' => '000000',
            'borderSize' => 6,
            'cellSpacing' => 10
        ];

        $table->addRow();
        $table->addCell(500, $styleCell)->addText("№", $styleFontHeader, $pStyle);
        $table->addCell(4500, $styleCell)->addText("Товары (работы, услуги)", $styleFontHeader, $pStyle);
        $table->addCell(1500, $styleCell)->addText("Кол-во", $styleFontHeader, $pStyle);
        $table->addCell(500, $styleCell)->addText("Ед.", $styleFontHeader, $pStyle);
        $table->addCell(1500, $styleCell)->addText("Сумма", $styleFontHeader, $pStyle);

        foreach ($order->orderProducts as $index => $orderProduct) {
            $table->addRow();
            $table->addCell(500, $styleCell)->addText($index + 1);

            $orderProductText = $orderProduct->product->name .
                ($orderProduct->need_layout ? ' + разработка макета' : '') .
                "<w:br />";
            foreach ($orderProduct->parameters() as $parameter) {
                $orderProductText .= $parameter->name . ': ';

                if (in_array($parameter->type, ['tabs', 'list'])) {
                    $formattedValues = $parameter->user_values->map(function ($item) {
                        return $item['name'];
                    })->implode(', ');

                    $orderProductText .= $formattedValues;
                } elseif (in_array($parameter->type, ['text', 'number'])) {
                    $formattedValues = $parameter->user_values->map(function ($item) {
                        return $item['data']['input'];
                    })->implode(', ');

                    $orderProductText .= $formattedValues;
                }

                $orderProductText .= "<w:br />";
            }
            $table->addCell(4500, $styleCell)->addText($orderProductText);
            $table->addCell(1500, $styleCell)->addText($orderProduct->count);
            $table->addCell(500, $styleCell)->addText($orderProduct->product->count_type);

            $price = number_format($orderProduct->total, 2, '.', ' ');
            $table->addCell(1500, $styleCell)->addText("$price ₽");
        }

        return $this->getTableAsXml($document_with_table);
    }

    private function makeReplaces(TemplateProcessor $template, Order $order)
    {
        $template->setValue('order.total', number_format($order->total, 2, '.', ' '));
        $template->setValue('order.created_at', $order->created_at->formatLocalized('%d %B %Y г.'));
        $template->setValue('contract.code', $order->contractCode);
        $template->setValue('order.id', $order->id);


        $bonusesSpent = $order->bonuses_spent > 0 ?
            'Использовано бонусов: ' . $order->bonuses_spent . ' ₽' : '';
        $template->setValue('order.bonuses_spent', $bonusesSpent);

        $partnerSale = $order->partner_sale > 0 ?
            'Скидка партнёра: ' . round($order->partner_sale * 100) . '%' : '';
        $template->setValue('order.partner_sale', $partnerSale);

        $template->setValue('order.total', $order->total);
        $template->setValue('products.count', $order->orderProducts->count());

        $template->setValue('client.phone', $order->phone);
        $template->setValue('client.email', $order->email);

        $template->setValue('company.name', $order->company->name);
        $template->setValue('company.director', $order->company->director);
        $template->setValue('company.inn', $order->company->inn);
        $template->setValue('company.kpp', $order->company->kpp);
        $template->setValue('company.bank', $order->company->bank);
        $template->setValue('company.bik', $order->company->bik);
        $template->setValue('company.legal_address', $order->company->legal_address);
        $template->setValue('company.mail_address', $order->company->legal_address);
        $template->setValue('company.rs', $order->company->rs);
        $template->setValue('company.ks', $order->company->ks);

        $template->setValue('products.table', $this->makeProductsTableXml($order));
    }

    private function createOrderFolderIfNotExists(Order $order)
    {
        $path = 'orders/' . $order->id;
        if (!Storage::exists($path)) {
            Storage::makeDirectory($path, 0777, true, true);
        }
    }

    public function getTableAsXml($document)
    {
        $objWriter = IOFactory::createWriter($document, 'Word2007');
        $fullXml = $objWriter->getWriterPart('Document')->write();
        return preg_replace('/^[\s\S]*(<w:tbl\b.*<\/w:tbl>).*/', '$1', $fullXml);
    }

    public function getContractAsDocx(Order $order)
    {
        $order->load(['user', 'company', 'orderProducts']);

        $template = new TemplateProcessor(public_path('/doc_templates/contract.docx'));
        $this->makeReplaces($template, $order);

        $this->createOrderFolderIfNotExists($order);

        $url = 'orders/' . $order->id . '/contract.docx';
        $template->saveAs(storage_path('app/' . $url));

        return $url;
    }

    public function exportUpdArchive($from, $to)
    {
        $files = Order::where('completed_at', '>=', $from)
            ->where('completed_at', '<=', $to)
            ->where('client_upd', '!=', null)
            ->select(['id', 'completed_at', 'client_upd'])
            ->get()
            ->pluck('client_upd');

        $zip = new ZipArchive;

        $now = Carbon::now('Asia/Yekaterinburg')->toDateTimeString();
        $fileName = Storage::disk('local')->path("exports/$now.zip");
        if ($zip->open($fileName, ZipArchive::CREATE) === TRUE) {
            foreach ($files as $filePath) {
                $relativeNameInZipFile = basename(Str::ascii($filePath));
                $zip->addFile($filePath, $relativeNameInZipFile);
            }

            $zip->close();
        }
        return $fileName;
    }

    public function getInvoiceAsHtml(Order $order)
    {
        $order->load(['user', 'company']);
        $data = ['order' => $order];


        if ($order->company) {
            $qrData = 'ST00012|Name=ООО РА "Буква Плюс"|PersonalAcc=40702810167170000266|' .
                'BankName= ЗАПАДНО-СИБИРСКИЙ БАНК ПАО СБЕРБАНК Г.ТЮМЕНЬ|BIC=047102651|' .
                'CorrespAcc=30101810800000000651|PayeeINN=8602256565|KPP=860201001|' .
                'Sum=' . $data['order']['total'] * 100 . '|' .
                'Purpose=Оплата заказа №' . $data['order']['id'];
            $qrCode = app(QrCodeGeneratorInterface::class)->generate($qrData);
            $data['qr'] = $qrCode;
            return view('documents.invoice-for-company', $data)->render();
        } else {
            $qrData = 'ST00012|Name=ИП Медведева Ю.С.|PersonalAcc=40802810967170010822|' .
                'BankName=ЗАПАДНО-СИБИРСКИЙ БАНК ПАО СБЕРБАНК|BIC=047102651|' .
                'CorrespAcc=30101810800000000651|PayeeINN=550208842788|' .
                'Sum=' . $data['order']['total'] * 100 . '|' .
                'Purpose=Оплата заказа №' . $data['order']['id'];
            $qrCode = app(QrCodeGeneratorInterface::class)->generate($qrData);
            $data['qr'] = $qrCode;
            return view('documents.invoice-for-individual', $data)->render();
        }
    }

    public function getInvoiceAsPdf(Order $order)
    {
        $this->createOrderFolderIfNotExists($order);

        $url = 'orders/' . $order->id . '/invoice.pdf';
        $content = $this->htmlToPdf($this->getInvoiceAsHtml($order));
        Storage::put($url, $content);

        return $url;
    }

    public function getUPDAsHtml(Order $order, $signed = true)
    {
        $order->load(['user', 'company']);
        $data = [
            'listCount' => 1,
            'shippingDate' => $order->completed_at ? $order->completed_at->toArray() : $order->created_at->toArray(),
            'contract' => [
                'fullName' => $order->contractCode,
            ],
            'order' => $order,
            'orderItems' => $order->orderProducts,
            'signed' => $signed,
        ];

        if ($order->company()->exists()) {
            $data['seller'] = [
                'name' => ' ООО РА "Буква Плюс"',
                'director' => 'Чернов П.А.',
                'inn' => '8602256565',
                'kpp' => '860201001',
                'address' => ' 628403, Россия, ХМАО-Югра, г. Сургут, ул. 30 лет Победы, 33А.',
                'sign' => "https://ra-bukva.com/images/bukva_sign.png",
                'stamp' => "https://ra-bukva.com/images/bukva_stamp.png",
                'position' => 'Ген. директор',
            ];
            $data['client'] = [
                'isCompany' => true,
                'name' => $order->company->name,
                'inn' => $order->company->inn,
                'kpp' => $order->company->kpp,
                'address' => $order->company->legal_address,
            ];
        } else {
            $data['seller'] = [
                'name' => 'ИП Медведева Ю.С.',
                'director' => 'Медведева Ю.С.',
                'inn' => '550208842788',
                'kpp' => '',
                'address' => ' 628403, Россия, ХМАО-Югра, г. Сургут, ул. 30 лет Победы, 33А.',
                'sign' => "https://ra-bukva.com/images/ip_sign.png",
                'stamp' => "https://ra-bukva.com/images/ip_stamp.png",
                'position' => 'Руководитель',
            ];
            $data['client'] = [
                'isCompany' => false,
                'name' => 'Розничный покупатель (' . $order->phone . ' / ' . $order->email . ')',
                'inn' => '-',
                'kpp' => '-',
                'address' => '-',
            ];
        }

        return view('documents.upd', $data)->render();
    }

    public function getUPDAsPdf(Order $order, $signed = true)
    {
        $this->createOrderFolderIfNotExists($order);

        $content = $this->htmlToPdf($this->getUPDAsHtml($order, $signed), true);
        $url = 'orders/' . $order->id . '/upd.pdf';
        Storage::put($url, $content);

        return $url;
    }

    public function getOfferAsHtml(Order $order)
    {
        $order->load(['user', 'company']);
        $data = [
            'order' => $order,
            'freeDeliveryFrom' => CRMSettings::instance()->free_delivery_from,
        ];

        $this->createOrderFolderIfNotExists($order);
        return view('documents.offer', $data);
    }

    public function getOfferAsPdf(Order $order)
    {
        $content = $this->htmlToPdf($this->getOfferAsHtml($order));

        $url = 'orders/' . $order->id . '/offer.pdf';

        Storage::put($url, $content);

        return $url;
    }

    public function htmlToPdf($html, $landscape = false)
    {
        $pdf = DomPDF::setOptions([
            'isHtml5ParserEnabled' => false,
            'isRemoteEnabled' => true,
        ])
            ->loadHTML($html);

        if ($landscape) $pdf->setPaper('a4', 'landscape');

        $ctx = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $pdf->getDomPDF()->setHttpContext($ctx);

        return $pdf->download()->getOriginalContent();
    }
}
