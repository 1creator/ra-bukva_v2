<?php

namespace App\Services\DocGenerator;

use App\Services\Payment\Controllers\PaymentController;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Services\TransactionService;
use App\Services\Payment\Services\YandexKassaService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class DocGeneratorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DocGeneratorService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
