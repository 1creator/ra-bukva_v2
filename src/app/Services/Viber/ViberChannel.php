<?php

namespace App\Services\Viber;


class ViberChannel
{
    public function send($notifiable, ViberNotification $notification)
    {
        $message = $notification->toViber($notifiable);

        $token = null;
        if (property_exists($notifiable, 'routes')) {
            $token = $notifiable->routes['viber'] ?? null;
        }
        if (!$token && method_exists($notifiable, 'getViberId')) {
            $token = $notifiable->getViberId();
        }
        $viber = new Viber();
        $viber->sendMessage($token, $message);
    }
}
