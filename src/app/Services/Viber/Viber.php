<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 17.07.2019
 * Time: 17:09
 */

namespace App\Services\Viber;


class Viber
{
    private $urlApi = "https://chatapi.viber.com/pa/";

    private $token;

    public function __construct()
    {
        $this->token = config('services.viber.token');
    }

    public function postMessage($from, array $sender, $text)
    {
        $data['from'] = $from;
        $data['sender'] = $sender;
        $data['type'] = 'text';
        $data['text'] = $text;
        return $this->call_api('post', 'post_message', $data);
    }

    public function sendMessage($receiver, $text)
    {
        $data['sender'] = ['name' => config('app.name')];
        $data['receiver'] = $receiver;
        $data['type'] = 'text';
        $data['text'] = $text;
        return $this->call_api('post', 'send_message', $data);
    }

    private function call_api($type, $apiMethod, $data)
    {
        $url = $this->urlApi . $apiMethod;

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\nX-Viber-Auth-Token: " . $this->token . "\r\n",
                'method' => $type,
                'content' => json_encode($data)
            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        return json_decode($response);
    }
}
