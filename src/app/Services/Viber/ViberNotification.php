<?php


namespace App\Services\Viber;


interface ViberNotification
{
    public function toViber($notifiable);
}
