<?php


namespace App\Services\Review;


use App\Models\Order;

class ReviewCreated
{
    public Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
