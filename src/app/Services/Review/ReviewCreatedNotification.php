<?php

namespace App\Services\Review;

use App\Models\Order;
use App\Services\Viber\ViberChannel;
use App\Services\Viber\ViberNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReviewCreatedNotification extends Notification implements ShouldQueue, ViberNotification
{
    use Queueable;

    private Order $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ViberChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toText($notifiable)
    {
        $message = "Пользователь написал отзыв на заказ!\nID Заказа: " . $this->order->id . "\nОценка: " . $this->order->rating;

        $message .= "\n\nТелефон клиента: " . $this->order->phone;
        $message .= "\nEmail клиента: " . $this->order->email;

        $message .= "\n" . $this->order->review;

        $message .= "\n\n" . url('/admin/orders/' . $this->order->id);

        return $message;
    }

    public function toViber($notifiable)
    {
        return $this->toText($notifiable);
    }
}
