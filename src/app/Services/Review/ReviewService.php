<?php

namespace App\Services\Review;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class ReviewService
{
    public function storeReview(Order $order, $data)
    {
        Validator::make($data, [
            'review' => 'required|min:100|max:2000',
            'orderProducts.*.rating' => 'numeric|max:5',
            'orderProducts.*.id' => 'exists:order_products',
        ], [
            'orderProducts.*.rating.numeric' => 'Пожалуйста, поставьте оценку во всех позициях заказа.',
        ])->validate();

        $order->review = $data['review'];
        foreach ($data['orderProducts'] as $orderProduct) {
            $op = OrderProduct::findOrFail($orderProduct['id']);
            $op->rating = $orderProduct['rating'];
            $op->save();
        }
        $order->save();

        event(new ReviewCreated($order));

        return $order;
    }

    public function getReviewsForProduct(Product $product)
    {
        return Order::whereHas('orderProducts', function (Builder $q) use ($product) {
            $q->whereNotNull('rating')
                ->where('product_id', $product->id);
        })
            ->with(['orderProducts.product.image', 'user'])
            ->latest()
            ->get()
            ->map(function ($order) {
                $items = $order->orderProducts->map(function ($item) {
                    return [
                        'rating' => $item->rating,
                        'product_id' => $item->product->id,
                        'product_name' => $item->product->name,
                        'product_link' => $item->product->link,
                        'product_image' => $item->product->image->thumbnail ?? null,
                    ];
                });

                $clientName = $order->name ?? $order->user->first_name ?? 'Имя не указано';

                return [
                    'body' => $order['review'],
                    'date' => $order->created_at->format('d.m.Y'),
                    'client_name' => $clientName,
                    'average_rating' => number_format($items->average('rating'), 1),
                    'items' => $items
                ];
            });
    }
}
