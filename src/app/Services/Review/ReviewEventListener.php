<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.10.2019
 * Time: 14:07
 */

namespace App\Services\Review;


use App\Utils\AccumulateNotification;
use Illuminate\Notifications\Events\NotificationSending;
use Illuminate\Support\Facades\Notification;

class ReviewEventListener
{
    public function handle($event)
    {
        if ($event instanceof ReviewCreated) {
            $notification = new ReviewCreatedNotification($event->order);

            Notification::route('viber', config('services.viber.admin_token'))
                ->notify($notification);
        }
    }
}
