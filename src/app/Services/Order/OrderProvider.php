<?php

namespace App\Services\Order;

use App\Services\Payment\Controllers\PaymentController;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Services\TransactionService;
use App\Services\Payment\Services\YandexKassaService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class OrderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OrderService::class);
        $this->app->bind(ImpactResolver::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
