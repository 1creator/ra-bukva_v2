<?php


namespace App\Services\Order;


use App\Models\Product;

class OrderService
{
    public function saleFor(array $parameterValues, Product $product, float $count)
    {
        $salesMap = $this->saleMapFor($product, $parameterValues);

        $salesCount = count($salesMap);
        if (!$count || $salesCount == 0) return 0;

        $sales = collect($salesMap);

        if ($count < $sales->first()['count']) {
            return 0;
        }

        if ($count >= $sales->last()['count']) {
            return $sales->last()['sale'];
        }

        $saleIndex = $sales->search(function ($item, $key) use ($count) {
            return $item['count'] > $count;
        });

        return $sales->get($saleIndex - 1)['sale'];
    }

    public function saleMapFor(Product $product, array $parameterValues)
    {
        $parameterValues = collect($parameterValues);
        $productsSales = $product->sales;

        if ($productsSales && array_key_exists('features', $productsSales)) {
            $valueIds = $parameterValues->pluck('id')->toArray();
            $mostFeature = null;

            foreach ($productsSales['features'] as $feature) {
                if (!array_diff($feature['values'], $valueIds)) {
                    if (!$mostFeature || (count($feature['values']) > count($mostFeature['values']))) {
                        $mostFeature = $feature;
                    }
                }
            }
            if ($mostFeature) {
                return $mostFeature['sales'];
            }
        }

        return $productsSales['default'] ?? [];
    }

    public function calculateProduct(Product $product, array $parameterValues, $count, $needLayout)
    {
        $calc = [
            'count' => $product->count_calculating ? 1 : $count,
            'cost' => $product->base_cost,
            'price' => $product->base_price,
            'amount_layout' => $product->base_layout_price,
            'files_count' => $product->order_files_count,
            'extra_amount' => 0
        ];

        foreach ($parameterValues as $parameterValue) {
            (new ImpactResolver($parameterValue['id']))->resolve($calc, $parameterValue['data'] ?? null);
        }

        if ($calc['count'] < $product->min_count) {
            $calc['count'] = $product->min_count;
        }

        $calc['sale'] = $this->saleFor($parameterValues, $product, $calc['count']);

        $amountProduct = round($calc['price'] * $count * (1 - $calc['sale'])) + $calc['extra_amount'];
        $amountLayout = $needLayout ? $calc['amount_layout'] : 0;
        $total = $amountProduct + $amountLayout;
        $totalCost = $calc['cost'] * $count + $amountLayout;

        return [
            'total' => $total,
            'total_cost' => $totalCost,
            'amount_product' => $amountProduct,
            'amount_layout' => $amountLayout,
            'price' => $calc['price'],
            'cost' => $calc['cost'],
            'sale' => $calc['sale'],
            'count' => $calc['count'],
            'files_count' => $calc['files_count'],
        ];
    }

    public function calculateProducts($orderProducts)
    {
        $calculations = [];
        foreach ($orderProducts as $key => $orderProduct) {
            $product = Product::findOrFail($orderProduct['product_id']);

            $calc = app(OrderService::class)
                ->calculateProduct($product, $orderProduct['parameter_values'], $orderProduct['count'],
                    $orderProduct['need_layout']);
            $calculations[$key] = $calc;
        }

        return $calculations;
    }

    public function createOrderProducts($order, $orderProducts, $calculations)
    {
        $collection = collect();
        foreach ($orderProducts as $key => $orderProduct) {
            $model = self::createOrderProduct($order, $orderProduct['product_id'],
                $orderProduct['need_layout'], $orderProduct['parameter_values'], $orderProduct['layout_ids'],
                $calculations[$key]);
            $collection->add($model);
        }
        return $collection;
    }

    public function createOrderProduct($order, $productId, $needLayout, $parameterValues, $layoutIds, $calculation)
    {
        $createdOrderProduct = $order->orderProducts()->create([
            'product_id' => $productId,
            'need_layout' => $needLayout,
            'count' => $calculation['count'],
            'amount_product' => $calculation['amount_product'],
            'amount_layout' => $calculation['amount_layout'],
            'cost' => $calculation['total_cost'],
            'sale_percentage' => $calculation['sale'],
        ]);

        $syncModels = [];
        foreach ($parameterValues as $paramValue) {
            $syncModels[$paramValue['id']] = ['data' => json_encode($paramValue['data'] ?? null)];
        }

        $createdOrderProduct->parameterValues()->sync($syncModels);
        $createdOrderProduct->layoutMediaFiles()->sync($layoutIds);

        return $createdOrderProduct;
    }
}
