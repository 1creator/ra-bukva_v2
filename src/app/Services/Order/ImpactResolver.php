<?php


namespace App\Services\Order;

use App\Models\ProductParameterValue;
use Exception;

class ImpactResolver
{
    private $model;

    public function __construct($productParameterValueId)
    {
        $this->model = ProductParameterValue::findOrFail($productParameterValueId);
    }

    public function resolve(array & $calc, $data)
    {
//        echo (json_encode($this->model->impact));

        $resolvers = [
            'price+=value' => function (& $calc, $impact, $data) {
                $calc['price'] += $impact['value'];
            },
            'price*=value' => function (& $calc, $impact, $data) {
                $calc['price'] *= $impact['value'];
            },
            'price*=value*letter_count' => function (& $calc, $impact, $data) {
                $calc['price'] *= $impact['value'] *
                    (strlen($data['input']) - substr_count($data['input'], ' '));
            },
            'price*=value*input' => function (& $calc, $impact, $data) {
                $calc['price'] *= $impact['value'] * $data['input'];
            },

            'cost+=value' => function (& $calc, $impact, $data) {
                $calc['cost'] += $impact['value'];
            },
            'cost*=value' => function (& $calc, $impact, $data) {
                $calc['cost'] *= $impact['value'];
            },
            'cost*=value*letter_count' => function ($calc, $impact, $data) {
                $calc['cost'] *= $impact['value'] *
                    (strlen($data['input']) - substr_count($data['input'], ' '));
            },
            'cost*=value*input' => function (& $calc, $impact, $data) {
                $calc['cost'] *= $impact['value'] * $data['input'];
            },

            'layout_price+=value' => function (& $calc, $impact, $data) {
                $calc['amount_layout'] += $impact['value'];
            },
            'layout_price*=value' => function (& $calc, $impact, $data) {
                $calc['amount_layout'] *= $impact['value'];
            },

            'count*=value*input' => function (& $calc, $impact, $data) {
                $calc['count'] *= $impact['value'] * $data['input'];
            },
            'count*=value*letter_count' => function (& $calc, $impact, $data) {
                $calc['count'] *= $impact['value'] *
                    (strlen($data['input']) - substr_count($data['input'], ' '));
            },

            'amount+=value' => function (& $calc, $impact, $data) {
                $calc['extra_amount'] += $impact['value'];
            },

            'files_count+=value' => function (& $calc, $impact, $data) {
                $calc['files_count'] += $impact['value'];
            },
            'files_count*=value*input' => function (& $calc, $impact, $data) {
                $calc['files_count'] *= $impact['value'] * $data['input'];
            },
        ];

        try {
            foreach ($this->model->impact as $impact) {
                $resolvers[$impact['action']]($calc, $impact, $data);
            }
        } catch (Exception $e) {

        }
    }
}
