<?php


namespace App\Services\Payment;


use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Traits\InteractsWithPaymentsTrait;

class AnonymousPayer implements InteractsWithPaymentsInterface
{
    use InteractsWithPaymentsTrait;

    public ?string $fullName, $email, $phone;

    public function __construct(?string $fullName = null, ?string $phone = null, ?string $email = null)
    {
        $this->fullName = $fullName;
        $this->phone = $phone;
        $this->email = $email;
    }

    public function getPayerId(): int
    {
        return 1;
    }

    public function getPayerFullName(): ?string
    {
        return $this->fullName;
    }
}
