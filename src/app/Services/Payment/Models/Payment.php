<?php


namespace App\Services\Payment\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail(string $string)
 */
class Payment extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    protected $casts = [
        'payment_method' => 'array',
        'confirmation' => 'array',
    ];
    protected $attributes = [
        'payment_method' => '[]',
        'confirmation' => '[]',
    ];


    public function owner()
    {
        return $this->morphTo('owner');
    }

    public function getConfirmationAttribute($val)
    {
        return json_decode($val);
    }
}
