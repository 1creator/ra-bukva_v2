<?php


namespace App\Services\Payment\Interfaces;


use App\Services\Payment\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

interface PaymentInterface
{
    public function makeIframe(int $amount, string $description): Payment;

    public function makeRedirect(int $amount, string $description, string $return_url): Payment;

    public function resolveWebHook(Request $request);
}
