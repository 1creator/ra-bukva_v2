<?php


namespace App\Services\ResetPassword\Controllers;


use App\Models\User;
use App\Services\ResetPassword\Interfaces\ResetPasswordInterface;
use App\Services\ResetPassword\Services\ResetPasswordService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use InvalidArgumentException;

class ResetPasswordController
{
    use ValidatesRequests;

    public function __construct()
    {
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'email' => 'required_without:phone|exists:users,email|email',
            'phone' => 'required_without:email|exists:users,phone|regex:/^((7)+([0-9]){10})$/',
        ]);

        $type = $request->has('email') ?
            ResetPasswordService::RESET_WITH_EMAIL : ResetPasswordService::RESET_WITH_PHONE;

        $user = User::where($type, $request->input($type))->firstOrFail();

        if (!$user instanceof ResetPasswordInterface)
            throw new InvalidArgumentException('Model must implement ResetPasswordInterface');

        return app(ResetPasswordService::class)->send($user, ResetPasswordService::RESET_WITH_EMAIL);
    }

    public function reset(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'token' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $model = app(ResetPasswordService::class)
            ->verify($request->input('token'), $request->input('code'));
        $model[$model->getPasswordField()] = Hash::make($request->input('password'));
        $model->save();
        return $model;
    }
}
