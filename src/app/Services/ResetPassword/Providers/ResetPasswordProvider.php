<?php

namespace App\Services\ResetPassword\Providers;

use App\Services\ResetPassword\Services\ResetPasswordService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ResetPasswordProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ResetPasswordService::class);
        $this->app->when(ResetPasswordService::class)
            ->needs(ResetPasswordService::class)
            ->give(function () {
                return Auth::user();
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::group([
            'prefix' => 'crud',
            'middleware' => 'api',
            'namespace' => 'App\Services\ResetPassword\Controllers'
        ], function () {
            Route::get('reset-password', 'ResetPasswordController@send');
            Route::post('reset-password', 'ResetPasswordController@reset');
        });
    }
}
