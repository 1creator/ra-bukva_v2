<?php


namespace App\Services\Attachment\Services;


use App\Models\MediaFile;
use App\Services\Attachment\Interfaces\AttachmentInterface;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Imagick;
use mikehaertl\pdftk\Pdf;
use Spatie\ImageOptimizer\OptimizerChain;

class AttachmentService implements AttachmentInterface
{
    public function store($file, $type, $splitPdf = false): \Illuminate\Support\Collection
    {
        return $type === 'embed'
            ? collect($this->storeEmbed($file))
            : collect($this->storeFile($file, $splitPdf));
    }

    public function storeFile(UploadedFile $file, $splitPdf = false): \Illuminate\Support\Collection
    {
        $disk = config('services.upload_disk', 'public');
        $createdMediaFiles = collect();
        $name = method_exists($file, 'getClientOriginalName')
            ? $file->getClientOriginalName()
            : $file->getFilename();
        $extension = method_exists($file, 'getClientOriginalExtension')
            ? $file->getClientOriginalExtension()
            : $file->extension();
        $fileHash = str_replace('.' . $file->extension(), '', $file->hashName());
        $fileName = $fileHash . '.' . $extension;

        // Приходится пересохранять файл локально, чтобы у него было клиентское расширение.
        // Иначе иногда не генерируется превью, т.к. imagick парсит CDR как BIN

        $file = Storage::disk('temp')->putFileAs('', $file, $fileName);
        $file = new File(Storage::disk('temp')->path($file));

        if ($file->getMimeType() == 'application/pdf' && $splitPdf) {
            $files = $this->splitPDF($file);
            \Illuminate\Support\Facades\File::delete($file);
        } else {
            $files = collect([$file]);
        }

        foreach ($files as $file) {
            app(OptimizerChain::class)->optimize($file);
            $path = Storage::disk($disk)->putFileAs('', $file, $file->getFilename());

            $mf = new MediaFile([
                'name' => $name,
                'type' => $file->getMimeType(),
                'path' => $path,
                'disk' => $disk,
            ]);

            $sizes = collect();

            if ($thumbnail = $this->makeThumbnail($file, 300)) {
                $thumbnailFile = new File($thumbnail['path']);
                $thumbnail['path'] = Storage::disk($disk)->putFile('', $thumbnailFile);
                $sizes->push($thumbnail);
                \Illuminate\Support\Facades\File::delete($thumbnailFile);
            }

            if (static::isWebImage($mf->type)) {
                $im = new Imagick();
                $im->readImage($file);
                $sizes->push([
                    'path' => $path,
                    'width' => $im->getImageWidth(),
                    'height' => $im->getImageHeight(),
                ]);
                // $mf->path = null;
                $im->destroy();
            }
            if ($sizes->isNotEmpty()) {
                $mf->sizes = $sizes;
            }

            $mf->save();
            $createdMediaFiles->push($mf);
            \Illuminate\Support\Facades\File::delete($file);
        }

        return $createdMediaFiles;
    }

    public function storeEmbed($file)
    {
        return MediaFile::create([
            'name' => 'Внешнее содержимое',
            'type' => 'embed',
            'path' => $file,
        ]);
    }

    static public function isWebImage($type): bool
    {
        return in_array($type, [
            'image/apng',
            'image/bmp',
            'image/gif',
            'image/x-icon',
            'image/jpeg',
            'image/png',
            'image/svg+xml',
            'image/tiff',
            'image/webp',
        ]);
    }

    private function splitPDF($file, $format = 'pdf')
    {
        $files = collect();
        $im = new Imagick();
        switch ($format) {
            case 'jpg':
            case 'jpeg':
                $im->setResolution(300, 300);
                $im->readImage($file);

                $im->setAntiAlias(false);
                $im->setColorspace(Imagick::COLORSPACE_CMYK);
                $im->setBackgroundColor("#ffffff");
                $im->setImageCompression(imagick::COMPRESSION_JPEG);
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(11); // Imagick::ALPHACHANNEL_REMOVE
                $im->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
                $im->setImageFormat('png');


                foreach ($im as $imFile) {
                    $tmp = $this->getTempFileName() . '.png';
                    if ($imFile->writeImage($tmp)) {
                        $files->push($tmp);
                    }
                }

                $im->destroy();
                break;
            case 'pdf':
                $im->pingImage($file);
                $pagesCount = $im->getNumberImages();

                $pdf = new Pdf($file->getRealPath(), [
                    'command' => '/usr/bin/pdftk',
                ]);

                $name = $this->getTempFileName();
                $pattern = $name . '_%d.pdf';

                $res = $pdf->burst($pattern);
                if ($res) {
                    for ($i = 0; $i < $pagesCount; $i++) {
                        $file = new File($name . '_' . ($i + 1) . '.pdf');
                        $files->push($file);
                    }
                }
                break;
        }

        $im->destroy();

        return $files;
    }

    private function makeThumbnail($file, $width): ?array
    {
        try {
            $im = new Imagick();
            // Если на этой команде возникает ошибка, то папке /var/www в контейнере app нужно дать права 777
            $im->readImage($file);

            $im->setAntiAlias(false);
            $im->setColorspace(Imagick::COLORSPACE_CMYK);
            $im->setBackgroundColor("#ffffff");
            $im->setImageCompression(imagick::COMPRESSION_JPEG);
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_FLATTEN);
            $im->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
            $im->setFormat('png');
            $im->setImageFormat('png');
            $height = (int)($width * $im->getImageHeight() / $im->getImageWidth());

            $im->thumbnailImage($width, $height, true);

            $path = $this->getTempFileName() . '.png';
            if ($im->writeImage($path)) {
                return [
                    'width' => $width,
                    'height' => $height,
                    'path' => $path
                ];
            }
//            exec('convert ' . $file .
//                ' -flatten -thumbnail "300x300^" -gravity center -crop 300x300+0+0 +repage ' . $thumbnailPath);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return null;
    }

    public function getTempFileName(): string
    {
        return Storage::disk('temp')->path(Str::random(32));
    }
}
