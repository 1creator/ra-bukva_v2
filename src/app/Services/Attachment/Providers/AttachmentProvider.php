<?php

namespace App\Services\Attachment\Providers;

use App\Services\Attachment\Components\AttachmentComponent;
use App\Services\Attachment\Interfaces\AttachmentInterface;
use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AttachmentProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AttachmentInterface::class, function ($app) {
            return new AttachmentService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
