<?php


namespace App\Services\QrCode;


class GoogleQrCodeGenerator implements QrCodeGeneratorInterface
{

    public function generate(string $data, int $width = 300, int $height = 300): string
    {
        return "https://chart.googleapis.com/chart?cht=qr&chs=${width}x${height}&chl=${data}&chld=M|0";
    }
}
