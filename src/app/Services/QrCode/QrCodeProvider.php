<?php

namespace App\Services\QrCode;

use Illuminate\Support\ServiceProvider;

class QrCodeProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QrCodeGeneratorInterface::class, GoogleQrCodeGenerator::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
