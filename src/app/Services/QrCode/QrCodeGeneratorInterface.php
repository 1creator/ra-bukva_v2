<?php


namespace App\Services\QrCode;


interface QrCodeGeneratorInterface
{
    public function generate(string $data, int $width = 300, int $height = 300): string;
}
