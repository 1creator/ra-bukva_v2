<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Mailing;
use Illuminate\Auth\Access\HandlesAuthorization;

class MailingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all mailings.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }

    /**
     * Determine whether the user can view the mailing.
     *
     * @param User $user
     * @param Mailing $mailing
     * @return mixed
     */
    public function view(User $user, Mailing $mailing)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }

    /**
     * Determine whether the user can create mailings.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }

    /**
     * Determine whether the user can update the mailing.
     *
     * @param User $user
     * @param Mailing $mailing
     * @return mixed
     */
    public function update(User $user, Mailing $mailing)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }

    /**
     * Determine whether the user can delete the mailing.
     *
     * @param User $user
     * @param Mailing $mailing
     * @return mixed
     */
    public function delete(User $user, Mailing $mailing)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }

    /**
     * Determine whether the user can restore the mailing.
     *
     * @param User $user
     * @param Mailing $mailing
     * @return mixed
     */
    public function restore(User $user, Mailing $mailing)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }

    /**
     * Determine whether the user can permanently delete the mailing.
     *
     * @param User $user
     * @param Mailing $mailing
     * @return mixed
     */
    public function forceDelete(User $user, Mailing $mailing)
    {
        return $user->staff->permissions->contains(Permission::MAILINGS_UPDATE);
    }
}
