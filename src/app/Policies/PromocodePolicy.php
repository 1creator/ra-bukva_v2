<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Promocode;
use Illuminate\Auth\Access\HandlesAuthorization;

class PromocodePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all promocodes.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }

    /**
     * Determine whether the user can view the promocode.
     *
     * @param User $user
     * @param Promocode $promocode
     * @return mixed
     */
    public function view(User $user, Promocode $promocode)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }

    /**
     * Determine whether the user can create promocodes.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }

    /**
     * Determine whether the user can update the promocode.
     *
     * @param User $user
     * @param Promocode $promocode
     * @return mixed
     */
    public function update(User $user, Promocode $promocode)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }

    /**
     * Determine whether the user can delete the promocode.
     *
     * @param User $user
     * @param Promocode $promocode
     * @return mixed
     */
    public function delete(User $user, Promocode $promocode)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }

    /**
     * Determine whether the user can restore the promocode.
     *
     * @param User $user
     * @param Promocode $promocode
     * @return mixed
     */
    public function restore(User $user, Promocode $promocode)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }

    /**
     * Determine whether the user can permanently delete the promocode.
     *
     * @param User $user
     * @param Promocode $promocode
     * @return mixed
     */
    public function forceDelete(User $user, Promocode $promocode)
    {
        return $user->staff->permissions->contains(Permission::PROMOCODES_UPDATE);
    }
}
