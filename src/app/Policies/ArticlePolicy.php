<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all articles.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the article.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function view(User $user, Article $article)
    {
        return true;
    }

    /**
     * Determine whether the user can create articles.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->staff->permissions->contains(Permission::ARTICLES_UPDATE);
    }

    /**
     * Determine whether the user can update the article.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function update(User $user, Article $article)
    {
        return $user->staff->permissions->contains(Permission::ARTICLES_UPDATE);
    }

    /**
     * Determine whether the user can delete the article.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function delete(User $user, Article $article)
    {
        return $user->staff->permissions->contains(Permission::ARTICLES_UPDATE);
        //
    }

    /**
     * Determine whether the user can restore the article.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function restore(User $user, Article $article)
    {
        return $user->staff->permissions->contains(Permission::ARTICLES_UPDATE);
        //
    }

    /**
     * Determine whether the user can permanently delete the article.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function forceDelete(User $user, Article $article)
    {
        return $user->staff->permissions->contains(Permission::ARTICLES_UPDATE);
        //
    }
}
