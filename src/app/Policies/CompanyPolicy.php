<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all companies.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->staff->permissions->contains(Permission::COMPANIES_VIEW);
    }

    /**
     * Determine whether the user can view the company.
     *
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function view(User $user, Company $company)
    {
        return $user->staff->permissions->contains(Permission::COMPANIES_VIEW);
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->staff->permissions->contains(Permission::COMPANIES_UPDATE);
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function update(User $user, Company $company)
    {
        return $user->staff->permissions->contains(Permission::COMPANIES_UPDATE);
        //
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function delete(User $user, Company $company)
    {
        return $user->staff->permissions->contains(Permission::COMPANIES_UPDATE);
        //
    }

    /**
     * Determine whether the user can restore the company.
     *
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function restore(User $user, Company $company)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the company.
     *
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function forceDelete(User $user, Company $company)
    {
        //
    }
}
