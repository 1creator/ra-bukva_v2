<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Sale;
use Illuminate\Auth\Access\HandlesAuthorization;

class SalePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all sales.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the sale.
     *
     * @param User $user
     * @param Sale $sale
     * @return mixed
     */
    public function view(User $user, Sale $sale)
    {
        return true;
    }

    /**
     * Determine whether the user can create sales.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->staff->permissions->contains(Permission::SALES_UPDATE);
    }

    /**
     * Determine whether the user can update the sale.
     *
     * @param User $user
     * @param Sale $sale
     * @return mixed
     */
    public function update(User $user, Sale $sale)
    {
        return $user->staff->permissions->contains(Permission::SALES_UPDATE);
    }

    /**
     * Determine whether the user can delete the sale.
     *
     * @param User $user
     * @param Sale $sale
     * @return mixed
     */
    public function delete(User $user, Sale $sale)
    {
        return $user->staff->permissions->contains(Permission::SALES_UPDATE);
    }

    /**
     * Determine whether the user can restore the sale.
     *
     * @param User $user
     * @param Sale $sale
     * @return mixed
     */
    public function restore(User $user, Sale $sale)
    {
        return $user->staff->permissions->contains(Permission::SALES_UPDATE);
    }

    /**
     * Determine whether the user can permanently delete the sale.
     *
     * @param User $user
     * @param Sale $sale
     * @return mixed
     */
    public function forceDelete(User $user, Sale $sale)
    {
        return $user->staff->permissions->contains(Permission::SALES_UPDATE);
    }
}
