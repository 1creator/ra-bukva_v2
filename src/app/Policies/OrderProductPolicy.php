<?php

namespace App\Policies;

use App\Models\OrderProduct;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list order products.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
//        return $user->staff->permissions->contains(Permission::ORDERS_VIEW)
//            || $user->staff->permissions->contains(Permission::ORDERS_VIEW_FOR_DESIGN);
    }


    /**
     * Determine whether the user can view the order product.
     *
     * @param User $user
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function view(User $user, OrderProduct $orderProduct)
    {
        return true;
//        return $user->staff->permissions->contains(Permission::ORDERS_VIEW)
//            || $user->staff->permissions->contains(Permission::ORDERS_VIEW_FOR_DESIGN);
    }

    /**
     * Determine whether the user can create order products.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the order product.
     *
     * @param User $user
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function update(User $user, OrderProduct $orderProduct)
    {
        return true;
//        return $user->staff->permissions->contains(Permission::ORDERS_VIEW)
//            || $user->staff->permissions->contains(Permission::ORDERS_VIEW_FOR_DESIGN);
    }

    /**
     * Determine whether the user can delete the order product.
     *
     * @param User $user
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function delete(User $user, OrderProduct $orderProduct)
    {
        return true;
    }

    /**
     * Determine whether the user can restore the order product.
     *
     * @param User $user
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function restore(User $user, OrderProduct $orderProduct)
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the order product.
     *
     * @param User $user
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function forceDelete(User $user, OrderProduct $orderProduct)
    {
        return true;
    }
}
