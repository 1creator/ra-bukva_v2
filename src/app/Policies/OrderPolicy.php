<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Request;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all orders.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param User $user
     * @param Order $order
     * @return mixed
     */
    public function view(?User $user, Order $order)
    {
        if ($order->secret == request('secret')) return true;

        if (!$user) return false;

        if ($order->user_id == $user->id) return true;

        if (!$user->staff) return false;

        if ($user->staff->permissions->contains(Permission::ORDERS_VIEW)) return true;

        if ($user->staff->permissions->contains(Permission::ORDERS_VIEW_FOR_DESIGN)) {
            $orderProductsInDesign = $order->orderProducts()
                ->where('status', 'layout')
                ->orWhere('status', 'layout_rejected')
                ->orWhere('status', 'layout_agreement')
                ->orWhere('status', 'layout_accepted');

            if ($orderProductsInDesign->count() > 0) return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param User $user
     * @return mixed
     */
    public function create(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param User $user
     * @param Order $order
     * @return mixed
     */
    public function update(?User $user, Order $order)
    {
        $allProductsEditable = $order->orderProducts->every(function ($value, $key) {
            return in_array($value->status, ['new', 'check']);
        });

        if ($allProductsEditable) {
            $res = $order->secret == request('secret');
            if ($res) return true;

            if (!$user) return false;

            $res = $order->user_id == $user->id;
            if ($res) return true;

            if (!$user->staff) return false;

            $res = $user->staff->permissions->contains(Permission::ORDERS_UPDATE);
            if ($res) return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param User $user
     * @param Order $order
     * @return mixed
     */
    public function delete(?User $user, Order $order)
    {
        return $user->staff->permissions->contains(Permission::ORDERS_UPDATE);

//        $allProductsEditable = $order->orderProducts->every(function ($value, $key) {
//            return in_array($value->status, ['new', 'check']);
//        });
//
//        if ($allProductsEditable) {
//            $res = $order->secret == request('secret');
//            if ($res) return true;
//
//            if (!$user) return false;
//
//            $res = $order->user_id == $user->id;
//            if ($res) return true;
//
//
//            if (!$user->staff) return false;
//
//            $res = $user->staff->permissions->contains(Permission::ORDERS_UPDATE);
//            if ($res) return true;
//        }

        return false;
    }

    /**
     * Determine whether the user can restore the order.
     *
     * @param User $user
     * @param Order $order
     * @return mixed
     */
    public function restore(User $user, Order $order)
    {
//        return $user->staff->permissions->contains(Permission::ORDERS_UPDATE);
    }

    /**
     * Determine whether the user can permanently delete the order.
     *
     * @param User $user
     * @param Order $order
     * @return mixed
     */
    public function forceDelete(User $user, Order $order)
    {
//        return $user->staff->permissions->contains(Permission::ORDERS_UPDATE);
    }
}
