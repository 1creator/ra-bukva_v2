<?php

namespace App\Observers;

use App\Models\Order;

class OrderObserver
{
    /**
     * Handle the models order "created" event.
     *
     * @param Order $modelsOrder
     * @return void
     */
    public function created(Order $modelsOrder)
    {
        //
    }

    /**
     * Handle the models order "updated" event.
     *
     * @param Order $modelsOrder
     * @return void
     */
    public function updated(Order $modelsOrder)
    {
        //
    }

    /**
     * Handle the models order "deleted" event.
     *
     * @param Order $modelsOrder
     * @return void
     */
    public function deleted(Order $modelsOrder)
    {
        //
    }

    /**
     * Handle the models order "restored" event.
     *
     * @param Order $modelsOrder
     * @return void
     */
    public function restored(Order $modelsOrder)
    {
        //
    }

    /**
     * Handle the models order "force deleted" event.
     *
     * @param Order $modelsOrder
     * @return void
     */
    public function forceDeleted(Order $modelsOrder)
    {
        //
    }
}
