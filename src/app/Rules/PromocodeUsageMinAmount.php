<?php

namespace App\Rules;

use App\Models\Promocode;
use Illuminate\Contracts\Validation\Rule;

class PromocodeUsageMinAmount implements Rule
{
    private $orderTotal = 0;
    private $promocodeMinAmount = 0;

    /**
     * Create a new rule instance.
     *
     * @param $orderTotal
     */
    public function __construct($orderTotal)
    {
        $this->orderTotal = $orderTotal;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $promocode = Promocode::findOrFail($value);
        $this->promocodeMinAmount = $promocode->min_amount;
        return $this->orderTotal >= $promocode->min_amount;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Promocode can be used only when ordering from " . $this->promocodeMinAmount . " rubles";
    }
}
