<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'smscru' => [
        'login' => env('SMSCRU_LOGIN'),
        'secret' => env('SMSCRU_SECRET'),
        'sender' => env('SMSCRU_SENDER'),
        'extra' => [
            // any other API parameters
            // 'tinyurl' => 1
        ],
    ],

    'viber' => [
        'token' => env('VIBER_TOKEN'),
        'admin_token' => env('VIBER_ADMIN_ID'),
    ],

    'upload_disk' => env('UPLOAD_DISK', 's3'),
];
