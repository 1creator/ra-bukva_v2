<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->string('name');
            $table->string('seo_name')->unique();
            $table->boolean('accent')->default(false);
            $table->string('meta_words')->nullable();
            $table->integer('order_weight')->default(0);
            $table->text('description')->nullable();
            $table->decimal('base_price')->default(0);
            $table->decimal('base_layout_price')->default(0);
            $table->decimal('base_cost')->default(0);
            $table->string('production_time')->nullable();
            $table->string('count_name')->default('Тираж');
            $table->string('count_type')->default('Шт.');
            $table->timestamps();

            $table->foreign('image_id')
                ->references('id')->on('media_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
