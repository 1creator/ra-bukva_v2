<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCompaniesTable extends Migration
{
    //todo remove this migration and related table

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name')->nullable();
            $table->string('inn')->nullable();
            $table->string('address_fact')->nullable();
            $table->string('address_legal')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('bik')->nullable();
            $table->string('kpp')->nullable();
            $table->string('bank')->nullable();
            $table->string('rs')->nullable();
            $table->string('ks')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_companies');
    }
}
