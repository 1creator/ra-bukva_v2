<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('order_files_count')->default(1);
            $table->string('order_files_extensions')->default('[".*"]');
            $table->string('order_files_validator_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('order_files_count');
            $table->dropColumn('order_files_extensions');
            $table->dropColumn('order_files_validator_url');
        });
    }
}
