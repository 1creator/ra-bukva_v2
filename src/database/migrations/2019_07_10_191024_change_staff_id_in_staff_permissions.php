<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStaffIdInStaffPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_permissions', function (Blueprint $table) {
            $table->dropForeign('staff_permissions_staff_id_foreign');

            $table->foreign('staff_id')
                ->references('id')->on('staff')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_permissions', function (Blueprint $table) {
            $table->dropForeign('staff_permissions_staff_id_foreign');

            $table->foreign('staff_id')
                ->references('id')->on('staff');
        });
    }
}
