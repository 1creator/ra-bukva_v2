<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_layouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_product_id');
            $table->unsignedBigInteger('media_file_id')->nullable();
            $table->enum('status', ['new', 'accepted', 'rejected']);
            $table->string('comment')->nullable();
            $table->timestamps();

            $table->foreign('order_product_id')
                ->references('id')->on('order_products')
                ->onDelete('cascade');

            $table->foreign('media_file_id')
                ->references('id')->on('media_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_layouts');
    }
}
