<?php

use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FormatSalesInProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (Product::all() as $product) {
            $sales = array_map(function ($item) {
                return ['count' => $item['from_count'], 'sale' => $item['percentage']];
            }, $product->sales);
            $product->sales = ['default' => $sales];
            $product->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (Product::all() as $product) {
            $sales = array_map(function ($item) {
                return ['from_count' => $item['count'], 'percentage' => $item['sale']];
            }, $product->sales['default']);
            $product->sales = $sales;
            $product->save();
        }
    }
}
