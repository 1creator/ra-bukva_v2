<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('product_parameters', function (Blueprint $table) {
            $table->string('type')->default('tabs');
            $table->boolean('multiple')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_parameters', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('multiple');
        });
    }
}
