<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->integer('count')->default(1);
            $table->integer('amount_product');
            $table->integer('amount_layout');
            $table->integer('total');
            $table->integer('cost');
            $table->decimal('sale_percentage', 3, 2);
            $table->boolean('need_layout')->nullable();
            $table->enum('status', ['new', 'check', 'payment', 'work', 'ready', 'offer',
                'layout', 'layout_agreement', 'layout_rejected', 'layout_accepted'])->default('new');
            $table->string('comment')->nullable();
            $table->unsignedBigInteger('order_id');
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('set null');

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
