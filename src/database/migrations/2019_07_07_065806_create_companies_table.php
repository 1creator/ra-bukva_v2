<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('director');
            $table->string('legal_address');
            $table->string('inn')->unique();
            $table->string('kpp');
            $table->string('bank');
            $table->string('rs');
            $table->string('ks');
            $table->string('bik');
            $table->enum('partner_status', ['requested', 'rejected', 'accepted'])->nullable();
            $table->decimal('partner_sale', 3, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
