<?php

use App\Models\ProductParameter;
use App\Models\ProductParameterValue;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class ChangeProductParameterValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_parameter_values', function (Blueprint $table) {
        });

        foreach (ProductParameterValue::all() as $value) {
            $impact = [];
            if ($value->add_price > 0) {
                array_push($impact, ['action' => 'price+=value', 'value' => $value->add_price]);
            }
            if ($value->multiply_price != 1) {
                array_push($impact, ['action' => 'price*=value', 'value' => $value->multiply_price]);
            }

            if ($value->add_cost > 0) {
                array_push($impact, ['action' => 'cost+=value', 'value' => $value->add_cost]);
            }
            if ($value->multiply_cost != 1) {
                array_push($impact, ['action' => 'cost*=value', 'value' => $value->multiply_cost]);
            }

            if ($value->add_layout_price > 0) {
                array_push($impact, ['action' => 'layout_price+=value', 'value' => $value->add_cost]);
            }

            $value->update(['impact' => $impact]);
        }

        foreach (ProductParameter::all() as $parameter) {
            $parameter->update([
                'type' => 'tabs'
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_parameter_values', function (Blueprint $table) {
        });
    }
}
