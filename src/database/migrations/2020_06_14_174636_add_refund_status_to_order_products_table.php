<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddRefundStatusToOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE order_products MODIFY COLUMN status"
            . " ENUM('new', 'check', 'payment', 'work', 'ready', 'offer', 'layout', 'layout_agreement', 'layout_rejected', 'layout_accepted', 'refunded') NOT NULL DEFAULT 'new'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
