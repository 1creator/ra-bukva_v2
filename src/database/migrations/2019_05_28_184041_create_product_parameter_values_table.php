<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductParameterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_parameter_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('product_parameter_id');
            $table->decimal('add_price')->default(0);
            $table->float('multiply_price')->default(1);
            $table->decimal('add_cost')->default(0);
            $table->float('multiply_cost')->default(1);
            $table->integer('add_layout_price')->default(0);
            $table->timestamps();

            $table->foreign('product_parameter_id')
                ->references('id')->on('product_parameters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_parameter_values');
    }
}
