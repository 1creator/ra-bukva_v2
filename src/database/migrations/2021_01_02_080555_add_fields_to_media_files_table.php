<?php

use App\Models\MediaFile;
use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddFieldsToMediaFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        Schema::table('media_files', function (Blueprint $table) {
            $table->json('sizes')->nullable();
            $table->string('disk')->default('public');
            $table->unsignedInteger('order_column')->default(0);
            $table->renameColumn('original', 'path');
        });

        $count = MediaFile::query()->count();
        foreach (MediaFile::all() as $index => $mediaFile) {
            $sizes = collect();

            if ($thumbnail = $mediaFile->getRawOriginal('thumbnail')) {
                $sizes->push([
                    'width' => 300,
                    'height' => 300,
                    'path' => str_replace('/storage', '', $thumbnail)
                ]);
            }

            if ($mediaFile->type == 'vk_video') {
                $mediaFile->type = 'embed';
            } elseif ($mediaFile->type != 'embed') {
                if (AttachmentService::isWebImage($mediaFile->type)) {
                    $sizes->push([
                        'width' => 5000,
                        'height' => 5000,
                        'path' => str_replace('/storage', '', $mediaFile->path)
                    ]);
                    $mediaFile->path = null;
                } else {
                    $mediaFile->path = str_replace('/storage', '', $mediaFile->path);
                }
            }

            if ($sizes->isNotEmpty()) {
                $mediaFile->sizes = $sizes;
            }

            $mediaFile->save();

            $output->writeln("${index}/${count}");
        }

        Schema::table('media_files', function (Blueprint $table) {
            $table->dropColumn('thumbnail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_files', function (Blueprint $table) {
            //
        });
    }
}
