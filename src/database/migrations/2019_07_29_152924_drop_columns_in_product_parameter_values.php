<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsInProductParameterValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_parameter_values', function (Blueprint $table) {
            $table->dropColumn('add_price');
            $table->dropColumn('add_cost');
            $table->dropColumn('add_layout_price');
            $table->dropColumn('multiply_price');
            $table->dropColumn('multiply_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_parameter_values', function (Blueprint $table) {
            //
        });
    }
}
