<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_settings', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->decimal('bonus_bet', 3, 2);
            $table->decimal('partner_sale', 3, 2);
        });

        DB::table('crm_settings')->insert([
            'bonus_bet' => 0.05,
            'partner_sale' => 0.1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_settings');
    }
}
