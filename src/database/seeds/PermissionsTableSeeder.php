<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['id' => 'orders.view', 'name' => 'Заказы.Просмотр всех заказов'],
            ['id' => 'orders.view_for_design', 'name' => 'Заказы.Просмотр только заказов, связанных с разработкой макета'],
            ['id' => 'orders.update', 'name' => 'Заказы.Создание новых заказов'],

            ['id' => 'catalog.update', 'name' => 'Каталог.Редактирование каталога'],

            ['id' => 'projects.update', 'name' => 'Проекты.Редактирование проектов'],

            ['id' => 'sales.update', 'name' => 'Акции.Редактирование акций'],

            ['id' => 'mailings.update', 'name' => 'Рассылки.Просмотр и создание рассылок сообщений'],

            ['id' => 'companies.view', 'name' => 'Организации.Просмотр организаций'],
            ['id' => 'companies.update', 'name' => 'Организации.Редактирование организаций'],

            ['id' => 'statistic.view', 'name' => 'Статистика.Просмотр'],

            ['id' => 'articles.update', 'name' => 'Статьи.Редактирование статей'],

            ['id' => 'users.view', 'name' => 'Пользователи.Просмотр'],
            ['id' => 'users.update', 'name' => 'Пользователи.Редактирование пользователей'],
            ['id' => 'users.grant_staff_permissions', 'name' => 'Пользователи.Назначение сотрудников и настройка прав'],

            ['id' => 'settings.update', 'name' => 'Настройки.Изменение настроек CRM'],

            ['id' => 'promocodes.update', 'name' => 'Промокоды.Редактирование промокодов'],
        ]);
    }
}
