<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                'name' => 'Типография',
                'seo_name' => 'tipografiya',
                'order_weight' => 0,
                'description' => 'Описание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание раздела',
            ]
        ]);

        Category::insert([
            [
                'name' => 'Наружная реклама',
                'seo_name' => 'narujnaya-reklama',
                'order_weight' => 10,
                'description' => 'Описание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание раздела',
            ]
        ]);

        Category::insert([
            [
                'name' => 'Реклама',
                'seo_name' => 'reklama',
                'order_weight' => 10,
                'description' => 'Описание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание раздела',
            ]
        ]);

        Category::insert([
            [
                'name' => 'Выставочные стенды',
                'seo_name' => 'vistavochnie-stendi',
                'order_weight' => 10,
                'description' => 'Описание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание раздела',
            ]
        ]);

        Category::insert([
            [
                'name' => 'Открытия под ключ',
                'seo_name' => 'otkritiya-pod-kluych',
                'order_weight' => 10,
                'description' => 'Описание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание раздела',
            ]
        ]);

        Category::insert([
            [
                'name' => 'Сувенирная продукция',
                'seo_name' => 'suvenirnaya-produkciya',
                'order_weight' => 10,
                'description' => 'Описание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание разделаОписание раздела',
            ]
        ]);
    }
}
