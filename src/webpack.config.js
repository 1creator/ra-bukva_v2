const path = require('path');
const webpack = require('webpack');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('./resources')
        }
    },
    // output: {
    //     chunkFilename: 'js/chunks/[name].js?id=[chunkhash]',
    // },
    plugins: [
        new webpack.ContextReplacementPlugin(
            /moment[\/\\]locale/,
            // A regular expression matching files that should be included
            /(ru)\.js/
        )
    ]
};
