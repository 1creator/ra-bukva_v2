const config = require("./webpack.config");
const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(config);
// if (mix.inProduction()) {
//     mix.bundleAnalyzer();
// }

mix.sourceMaps(false);

mix.version();

// mix.disableSuccessNotifications();
// mix.polyfill({
//     enabled: true,
//     useBuiltIns: "usage",
//     targets: {"firefox": "50", "ie": 11}
// });

// mix.dumpWebpackConfig();

mix.vue({version: 2});

mix
    .js("resources/js/staff.js", "public/js")
    .js("resources/js/client.js", "public/js")
    .sass("resources/sass/vendor.scss", "public/css")
    .sass("resources/sass/app.scss", "public/css")
    .extract(["swiper", "moment", "vue", "vuex", "vue-router", "axios"]);

// .browserSync('192.168.10.10');
