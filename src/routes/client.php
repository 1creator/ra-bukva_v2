<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 09.10.2019
 * Time: 23:38
 */


//Crud routes
use Illuminate\Support\Facades\Route;

Route::get('/cabinet/{any}', 'SiteController@userCabinet')->where('any', '.*')->middleware(['auth']);

Route::group(['prefix' => 'crud', 'as' => 'crud'], function () {
    Route::post('/login', 'CRUD\LoginController@login');
    Route::post('/register', 'CRUD\RegisterController@register');

    Route::apiResource('products', 'CRUD\Client\ProductController');

    Route::apiResource('/files', 'CRUD\FileController');

    Route::get('/delivery', 'CRUD\DeliveryController@index');

    Route::post('/client/orders/{order}/review', 'CRUD\Client\OrderReviewController@store');
    Route::post('/client/orders/{order}/upload-upd', 'CRUD\Client\OrderUpdController@store');
    Route::get('/client/orders/{order}/payment-form', 'CRUD\Client\OrderPaymentController@getForm');
    Route::post('/client/orders/{order}/documents/send-to-email', 'CRUD\Client\OrderController@sendDocumentsToEmail');
    Route::post('/client/orders/{order}/invoice/send-to-email', 'CRUD\Client\OrderController@sendInvoiceToEmail');
    Route::apiResource('client/orders', 'CRUD\Client\OrderController');

    Route::put('client/order-product-layouts', 'CRUD\Client\OrderProductLayoutController@updateBulk');
    Route::apiResource('client/order-product-layouts', 'CRUD\Client\OrderProductLayoutController');

    Route::post('client/order-products/{orderProduct}/layouts',
        'CRUD\Client\OrderProductController@storeLayouts');
    Route::put('client/order-products/{orderProduct}/review',
        'CRUD\Client\OrderProductController@review');
    Route::apiResource('client/order-products', 'CRUD\Client\OrderProductController');

    Route::post('/callback', 'CRUD\CallbackController@store');

    Route::any('/search', 'CRUD\SearchController@index');

    //Client routes, require auth
    Route::group([
        'prefix' => 'client',
        'namespace' => 'CRUD\Client',
        'middleware' => ['auth'],
    ], function () {
        Route::get('/users/self/bonus-changes', 'UserController@getBonusChanges');
        Route::get('/users/self', 'UserController@getSelf');
        Route::put('/users/self', 'UserController@updateSelf');
        Route::apiResource('/companies', 'CompanyController');
    });


});
