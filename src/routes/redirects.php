<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\CheckAdminAccess;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

$redirects = [
    '/product/каталог-на-скобе/' => 'https://ra-bukva.ru/products/katalog-na-skobe',
    '/product/каталог-на-пружине/' => 'https://ra-bukva.ru/products/katalog-na-pruzine',
    '/product/пластиковые-карты/' => 'https://ra-bukva.ru/products/plastikovie-karti',
    '/product/кубарики/' => 'https://ra-bukva.ru/products/kubariki',
    '/product/визитки/' => 'https://ra-bukva.ru/products/vizitki',
    '/product/открытки/' => 'https://ra-bukva.ru/products/otkritki',
    '/product/настольный-перекидной-календарь/' => 'https://ra-bukva.ru/products/nastolnii-perekidnoi-kalendar',
    '/product/настенный-календарь/' => 'https://ra-bukva.ru/products/nastennii-kalendar',
    '/product/настольный-календарь/' => 'https://ra-bukva.ru/products/nastolnii-perekidnoi-kalendar',
    '/product/карманный-календарь/' => 'https://ra-bukva.ru/products/karmannii-kalendar',
    '/product/буклет-с-2-сгибами/' => 'https://ra-bukva.ru/products/eurobuklet',
    '/product/буклет-с-1-сгибом/' => 'https://ra-bukva.ru/products/buklet-odin-sgib',
    '/product/стандартный-каталог/' => 'https://ra-bukva.ru/products/katalog-na-pruzine',
    '/product/конверты/' => 'https://ra-bukva.ru/products/konverti',
    '/product/блокнот-на-пружине/' => 'https://ra-bukva.ru/products/bloknot-na-pruzine',
    '/product/плакаты/' => 'https://ra-bukva.ru/products/plakat-afisha',
    '/product/тест/' => 'https://ra-bukva.ru/catalog/bukleti',
    '/product/листовки/' => 'https://ra-bukva.ru/products/listovki',
    '/product/пакеты-типа-майка/' => 'https://ra-bukva.ru/products/packet-maika',
    '/product/пакеты-с-вырубной-ручкой/' => 'https://ra-bukva.ru/products/packet-virub-ruchka',
    '/product/интерьерная-печать-на-пленке-в-сургут/' => 'https://ra-bukva.ru/products/interiernaya-pechat-na-plenke',
    '/product/широкоформатная-печать-на-баннере-в-с/' => 'https://ra-bukva.ru/products/pechat-na-bannere',
    '/product/акриловые-магниты/' => 'https://ra-bukva.ru/products/akrilovie-magniti',
    '/product/виниловые-магниты/' => 'https://ra-bukva.ru/products/vinilovie-magniti',
    '/product/бумажные-пакеты-с-логотипом/' => 'https://ra-bukva.ru/products/bumazhnie-paketi-s-logotipom',
    '/product/сертификаты/' => 'https://ra-bukva.ru/products/sertifikati',
];


//'/product/световые-рекламные-вывески/' => '',
//    '/product/папки-с-логотипом/' => '',
//    '/product/воблер/' => '',
//    '/product/дорхенгер-крючок-на-дверь/' => '',
//    '/product/световой-короб/' => '',
//    '/product/сканирование-документов/' => '',
//    '/product/печать-фотографий/' => '',
//    '/product/наклейки/' => '',
//'/product/печать-на-холсте-в-сургуте/' => '',

foreach ($redirects as $old => $new) {
    Route::get($old, function () use ($new) {
        return redirect($new, 301);
    });
}
