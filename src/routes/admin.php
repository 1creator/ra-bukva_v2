<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 09.10.2019
 * Time: 23:35
 */

//Admin routes ,require auth and admin permissions
use App\Http\Middleware\CheckAdminAccess;
use Illuminate\Support\Facades\Route;

Route::get('/admin/{any}', function () {
    return view('layouts.cabinet');
})->where('any', '.*')->middleware(['auth', CheckAdminAccess::class]);

Route::group(['prefix' => 'crud', 'as' => 'crud'], function () {
    Route::group([
        'prefix' => 'admin',
        'namespace' => 'CRUD\Admin',
        'middleware' => ['auth', CheckAdminAccess::class],
    ], function () {
        Route::apiResource('categories', 'CategoryController');
        Route::apiResource('products', 'ProductController');
        Route::apiResource('sales', 'SaleController');
        Route::apiResource('articles', 'ArticleController');
        Route::apiResource('projects', 'ProjectController');
        Route::post('orders/{order}/notify', 'OrderController@notify');
        Route::apiResource('orders', 'OrderController');
        Route::apiResource('promocodes', 'PromocodeController');
        Route::apiResource('companies', 'CompanyController');

        Route::post('order-products/{orderProduct}/layouts',
            'OrderProductController@storeLayouts');
        Route::put('order-products/{orderProduct}/review',
            'OrderProductController@review');
        Route::apiResource('order-products', 'OrderProductController');

        Route::put('order-product-layouts', 'OrderProductLayoutController@updateBulk');
        Route::apiResource('order-product-layouts', 'OrderProductLayoutController');

        Route::put('settings', 'SettingController@update');
        Route::apiResource('settings', 'SettingController');

        Route::get('/users/self', 'UserController@getSelf');
        Route::put('/users/self', 'UserController@updateSelf');
        Route::apiResource('/users', 'UserController');

        Route::apiResource('mailings', 'MailingController')->only(['index', 'store']);

        Route::get('/permissions', 'PermissionController@index');
    });
});
