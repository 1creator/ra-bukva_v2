<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 09.10.2019
 * Time: 23:37
 */

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/sitemap.xml', 'SiteController@sitemap');
Route::get('/yml.xml', 'SiteController@yml');

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->back();
});

Route::get('/', 'SiteController@index');
Route::get('/articles/{article}', 'ArticleController@show')->name('articles.show');
Route::get('/sales', 'SiteController@sales')->name('sales');
Route::get('/terms', 'SiteController@terms')->name('terms');
Route::get('/layout-requirements', 'SiteController@layoutRequirements')->name('layout-requirements');
Route::get('/catalog', 'CategoryController@index')->name('categories');
Route::get('/catalog/{category}', 'CategoryController@show')->name('categories.show');
Route::get('products/{product}/files', 'ProductMediaFileController@index')->name('products.files');
Route::get('products/{product}/files/{mediaFile}', 'ProductMediaFileController@show')->name('products.files.show');
Route::get('/products/{product}', 'ProductController@show')->name('products.show');
Route::get('/delivery', 'SiteController@delivery')->name('delivery');
Route::get('/payment', 'SiteController@payment')->name('payment');
Route::get('/contacts', 'SiteController@contacts')->name('contacts');
Route::get('/checkout', 'SiteController@checkout')->name('checkout');

Route::get('/checkout', 'SiteController@checkout')->name('checkout');

Route::get('/orders/{order}/empty-upd', 'OrderController@emptyUpd');
Route::get('/orders/{order}/upd.pdf', 'OrderController@updPdf');
Route::get('/orders/{order}/client-upd', 'OrderController@clientUpd');
Route::get('/orders/{order}/invoice', 'OrderController@invoiceHtml');
Route::get('/orders/{order}/invoice.pdf', 'OrderController@invoicePdf');
Route::get('/orders/{order}/offer.pdf', 'OrderController@offerPdf');
Route::get('/orders/{order}/offer-html', 'OrderController@offerHtml');
Route::get('/orders/{order}/contract.docx', 'OrderController@contractDocx');
Route::get('/orders/{order}/upload-upd', 'OrderController@uploadUpd')->name('orders.upload-upd');
Route::get('/orders/{order}', 'OrderController@show')->name('orders.show');
Route::get('/exportUPDArchive', 'OrderController@exportUPDArchive');

