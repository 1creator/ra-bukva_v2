@extends('layouts.app')
@push('head')
    <title>Оформление заказа</title>
@endpush
@section('content')
    <div class="container">
        <order-checkout
                :delivery-zones="{{json_encode($deliveryZones)}}"
                :free-delivery-from="{{$freeDeliveryFrom}}"
        ></order-checkout>
    </div>
@endsection
