@extends('layouts.app')
@push('head')
    <title>Оплата изделий</title>
    <meta name="description"
          content="Любые виды оплаты - это про нас! Вы можете оплатить услуги безналичным платежом, банковской картой или наличными средствами.">
@endpush
@section('content')
    <div class="container">
        @include('components.breadcrumbs',['items' => [
            ['name' => 'Главная', 'link' => '/'],
            ['name' => 'Оплата'],
        ]])
        <div class="text-center mb-5">
            <i class="icofont-wallet icofont-3x text-danger"></i>
            <h1>СПОСОБЫ ОПЛАТЫ</h1>
        </div>
        <div class="row text-center">
            <div class="col-12 col-lg-4 mb-5 mb-lg-0 payment-type">
                <i class="icofont-bank-alt"></i>
                <h3 class="text-uppercase">Безналичный расчёт</h3>
                <div class="h5">Для юридических лиц и ИП</div>
            </div>
            <div class="col-12 col-lg-4 mb-5 mb-lg-0 payment-type">
                <i class="icofont-credit-card"></i>
                <h3 class="text-uppercase">Банковской картой</h3>
                <div class="h5">Для физических лиц</div>
            </div>
            <div class="col-12 col-lg-4 payment-type">
                <img src="/images/sberbank.svg">
                <h3 class="text-uppercase">По квитанции в Сбербанке</h3>
                <div class="h5">Для физических лиц</div>
            </div>
        </div>
        <hr class="my-5">
        <div class="row">
            <div class="col-12 col-md-6 mb-3 mb-md-0 text-justify" style="text-indent: 2rem">
                <p>
                    Обращаем Ваше внимание, что мы приступаем к изготовлению заказа после поступления денежных средств
                    на наш расчетный счет.
                </p>
                <p>
                    Для юридических лиц и индивидуальных предпринимателей мы предлагаем услугу постановки заказа в
                    работу,
                    не дожидаясь поступления оплаты. При наличии договора на оказание услуг.
                </p>
            </div>
            <div class="col-12 col-md-6">
                <table class="ml-md-5">
                    <tbody>
                    <tr>
                        <td class="text-danger pr-5">ИНН:</td>
                        <td>8602256565</td>
                    </tr>
                    <tr>
                        <td class="text-danger">КПП:</td>
                        <td>860201001</td>
                    </tr>
                    <tr>
                        <td class="text-danger" style="vertical-align: top">Банк:</td>
                        <td>ЗАПАДНО-СИБИРСКИЙ БАНК<br/>
                            ПАО СБЕРБАНК Г. ТЮМЕНЬ
                        </td>
                    </tr>
                    <tr>
                        <td class="text-danger">Р/с:</td>
                        <td>40702810167170000266</td>
                    </tr>
                    <tr>
                        <td class="text-danger">К/с:</td>
                        <td>30101810800000000651</td>
                    </tr>
                    <tr>
                        <td class="text-danger">БИК:</td>
                        <td>047102651</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection