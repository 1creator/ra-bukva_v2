<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<yml_catalog date="{{\Carbon\Carbon::now()}}">
    <shop>
        <name>РА Буква ПЛЮС</name>
        <company>ООО РА Буква ПЛЮС</company>
        <url>https://ra-bukva.com/</url>
        <email>welcome@1creator.ru</email>
        <currencies>
            <currency id="RUR" rate="1"/>
        </currencies>
        <categories>
            @foreach($categories as $category)
                @if($category->parent)
                    <category
                            id="{{$category->id}}" parentId="{{$category->parent->id}}">{{$category->name}}</category>
                @else
                    <category
                            id="{{$category->id}}">{{$category->name}}</category>
                @endif
            @endforeach
        </categories>
        <delivery-options>Доставка полиграфии по городу - бесплатно!</delivery-options>
        <offers>
            @foreach($products as $product)
                <offer id="{{$product->id}}">
                    <name>{{$product->name}}</name>
                    <url>{{route('products.show',['product'=>$product->seo_name])}}</url>
                    <price>{{$product->base_price}}</price>
                    <currencyId>RUR</currencyId>
                    @if($product->mainCategory())
                        <categoryId>{{$product->mainCategory()->id}}</categoryId>
                    @endif
                    @if($product->image)
                        <picture>{{url($product->image->thumbnail)}}</picture>
                    @endif
                    <store>true</store>
                    <pickup>true</pickup>
                    <description>
                        <![CDATA[
                        {!! $product->description !!}
                        ]]>
                    </description>
                </offer>
            @endforeach
        </offers>
    </shop>
</yml_catalog>
