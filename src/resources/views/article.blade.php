@extends('layouts.app')
@push('head')
    @if($article->name)
        <title>{{$article->name}}</title>
    @endif
    <meta name="description"
          content="{{Str::limit(strip_tags($article->text),100)}}">
    <link rel="canonical" href="{{route('articles.show', ['article'=>$article->seo_name])}}"/>
@endpush
@section('content')
    <div class="container mb-5">
        <h1>{{$article->name}}</h1>
        {!! $article->text !!}
    </div>
    <div class="container">
        <div class="mb-5">
            @include('components.slider-recommendations', ['products'=>$sliderProducts])
        </div>
    </div>
    <div class="mb-n5">
        @include('components.slider-sales')
    </div>
@endsection