@extends('layouts.app')
@push('head')
    <title>Контакты</title>
    <meta name="description"
          content="📞 Контактная информация рекламного агентства Буква ПЛЮС. Адрес, карта проезда, номера телефонов.">
@endpush
@section('content')
    <div class="container">
        @include('components.breadcrumbs',['items' => [
            ['name' => 'Главная', 'link' => '/'],
            ['name' => 'Контакты'],
        ]])
        <div class="text-center mb-5">
            <i class="icofont-ui-contact-list icofont-3x text-danger"></i>
            <h1>КОНТАКТЫ</h1>
            <div class="h5">
                УЛ. 30 ЛЕТ ПОБЕДЫ, 33А<br/>
                ОФИС 105, 107<br/>
                ТЕЛ.: 8 (3462) 950-250<br/>
                EMAIL: 950250@bk.ru
            </div>
        </div>
        <div class="mb-5">
            <table class="mx-auto">
                <tbody>
                <tr>
                    <td class="text-danger text-right pr-5 py-2">Режим работы офиса:</td>
                    <td class="py-2">По будням с 10<sup>00</sup> до 18<sup>00</sup></td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2">Юридический адрес:</td>
                    <td class="py-2">628403, Тюменская обл., ХМАО-Югра, г. Сургут, ул. 30 лет Победы, 33а</td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2">КПП:</td>
                    <td class="py-2">860201001</td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2" style="vertical-align: top">Банк:</td>
                    <td class="py-2">ЗАПАДНО-СИБИРСКИЙ БАНК<br/>
                        ПАО СБЕРБАНК Г. ТЮМЕНЬ
                    </td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2">Р/с:</td>
                    <td class="py-2">40702810167170000266</td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2">К/с:</td>
                    <td class="py-2">30101810800000000651</td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2">БИК:</td>
                    <td class="py-2">047102651</td>
                </tr>
                <tr>
                    <td class="text-danger text-right pr-5 py-2" style="vertical-align: top">Генеральный директор:</td>
                    <td class="py-2">Чернов Павел Анатольевич<br/>
                        Действует на основании устава.
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    @include('components.map')
@endsection