@extends('layouts.app')
@push('head')
    <title>Каталог услуг типографии и рекламного агентства</title>
    <meta name="description"
          content="Рассчитать стоимость любых видов печати и рекламных услуг с учётом оптовых и индивидуальных скидок.">
@endpush
@section('content')
    <div class="container">
        @include('components.breadcrumbs',['items' => [
            ['name' => 'Главная', 'link' => '/'],
            ['name' => 'Каталог услуг'],
        ]])
        <div class="text-center mb-5">
            <h1>КАТАЛОГ НАШИХ УСЛУГ</h1>
            <div class="h5">ДЛЯ ВАШЕГО БИЗНЕСА</div>
        </div>
        <div class="row">
            @foreach($categories as $category)
                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4">
                    @include('components.card-category', ['category'=>$category])
                </div>
            @endforeach
        </div>
    </div>
@endsection