<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{mix('/css/vendor.css')}}" rel="stylesheet">
    <link href="{{mix('/css/app.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    @if(Auth::user())
        <meta name="user" content="{{Auth::user()->profile()->toJson()}}">
    @endif
    @stack('head')
</head>
<body>
<div id="app">
    <nav class="navbar">
        <div class="container">
            <div class="navbar__top">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="/images/logo.svg" alt="Буква Плюс - Рекламное агенство в Сургуте">
                </a>
                <div>
                    <div class="mb-3 d-flex flex-column flex-md-row align-items-center justify-content-md-end">
                        <div class="navbar__item">
                            <a href="tel:+7(3462)950-250" class="navbar__link navbar__link_contact">
                                <img src="{{asset('images/icons/phone.svg')}}"/>8 (3462) 950-250
                            </a></div>
                        <div class="navbar__item">
                            <a href="mailto:950250@bk.ru" class="navbar__link navbar__link_contact">
                                <img src="{{asset('images/icons/mail.svg')}}"/>950250@bk.ru
                            </a>
                        </div>
                    </div>
                    <div class="mb-3 d-flex flex-column flex-md-row
                            align-items-center justify-content-md-end">
                        <top-cart class="mr-md-4 mb-3 mb-md-0">
                            <button class="btn btn-primary">
                                <i class="icofont-cart-alt"></i>
                                Мой заказ
                            </button>
                        </top-cart>
                        <top-profile>
                            <button class="btn btn-primary">
                                <i class="icofont-user-suited"></i>
                                Личный кабинет
                            </button>
                        </top-profile>
                    </div>
                </div>
            </div>
            <div class="navbar__bottom">
                <search-component class="navbar__search"></search-component>
                <ul class="navbar__links">
                    <li class="navbar__item">
                        <a class="navbar__link" href="{{ route('categories') }}">УСЛУГИ</a>
                    </li>
                    <li class="navbar__item">
                        <a class="navbar__link" href="{{ route('sales') }}">АКЦИИ И СКИДКИ</a>
                    </li>
                    <li class="navbar__item">
                        <a class="navbar__link" href="{{ route('payment') }}">ОПЛАТА</a>
                    </li>
                    <li class="navbar__item">
                        <a class="navbar__link" href="{{ route('delivery') }}">ДОСТАВКА</a>
                    </li>
                    <li class="navbar__item">
                        <a class="navbar__link" href="{{ route('contacts') }}">КОНТАКТЫ</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-3 py-md-5">
        @yield('content')
    </main>
    <messages-component></messages-component>
    <footer>
        <div class="container">
            <div class="row nav mb-3 mb-md-n3">
                <div class="col-12 col-lg-2">
                    <div class="h4 mb-4">О компании</div>
                    <a href="/" class="d-block nav-link">Главная</a>
                    <a href="/about" class="d-block nav-link">О нас</a>
                    <a href="/contacts" class="d-block nav-link">Контакты</a>
                    <a href="/delivery" class="d-block nav-link">Доставка</a>
                    <a href="/payment" class="d-block nav-link">Оплата</a>
                </div>
                <div class="col-12 col-lg-2">
                    <div class="h4 mb-4">Услуги</div>
                    <a href="/catalog" class="d-block nav-link">Все услуги</a>
                    <a href="/sales" class="d-block nav-link">Акции</a>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="h4 mb-4">Другое</div>
                    <a href="/layout-requirements" class="d-block nav-link">Требования к макетам</a>
                    <a href="/terms" class="d-block nav-link">Пользовательское соглашение</a>
                </div>
                <div class="col-12 col-lg-4 offset-lg-1 d-flex flex-column">
                    <div class="h4 mb-3">&nbsp;</div>
                    <a href="tel:+7(3462)950-250" class="nav-link h4 font-weight-bold mr-md-3 mb-3">
                        <i class="icofont-phone-circle mr-1"></i>8 (3462) 950-250
                    </a>
                    <a href="mailto:950250@bk.ru" class="nav-link h4 font-weight-bold mb-3">
                        <i class="icofont-email mr-1"></i>950250@bk.ru
                    </a>
                    <div class="mb-5">
                        <a class="h4 mr-3" href="https://vk.com/bukvaplus"><i class="icofont-vk"></i></a>
                        <a class="h4 mr-3" href="https://www.facebook.com/BukvaPlus"><i
                                class="icofont-facebook"></i></a>
                        <a class="h4" href="https://www.instagram.com/bukvaplus/"><i class="icofont-instagram"></i></a>
                    </div>
                    <button type="submit" class="btn btn-primary justify-content-center"
                            @click="d_showFeedbackModal=true">Заказать обратный звонок
                    </button>
                </div>
            </div>
            <a class="dev-banner" href="https://1creator.ru">
                Разработка и продвижение - <img src="/images/1creator.svg" alt="Разработка сайта от 1creator.ru"/>
            </a>
        </div>
        <feedback-modal v-if="d_showFeedbackModal"
                        @close="d_showFeedbackModal=false"></feedback-modal>
    </footer>
</div>

<script src="{{mix('/js/manifest.js')}}"></script>
<script src="{{mix('/js/vendor.js')}}"></script>
<script src="https://api-maps.yandex.ru/2.1/?apikey=fa9c6110-8dae-4b6f-8ea3-1d5760909e33&lang=ru_RU"
        type="text/javascript">
</script>

@if(Auth::user() && Auth::user()->isStaff())
    <script src="{{mix('/js/staff.js')}}" type="text/javascript"></script>
@else
    <script src="{{mix('/js/client.js')}}" type="text/javascript"></script>
@endif
@stack('scripts')

@if(env('APP_ENV') == 'production')
    <meta name="yandex-verification" content="b95647c56632ba03"/>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
        ym(54519451, "init", {clickmap: true, trackLinks: true, accurateTrackBounce: true}); </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/54519451" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript> <!-- /Yandex.Metrika counter -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144246288-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag("js", new Date());

        gtag("config", "UA-144246288-1");
    </script>
    <script src="https://code.jivosite.com/widget.js" jv-id="939xngDbqh"></script>
    <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="https://yastatic.net/share2/share.js"></script>
@endif
</body>
</html>
