@extends('layouts.app')
@push('head')
    <title>Личный кабинет | Буква Плюс</title>
    <meta name="description"
          content="Рассчитать стоимость любых видов печати и рекламных услуг с учётом оптовых и индивидуальных скидок.">
@endpush
@section('content')
    <router-view></router-view>
@endsection
