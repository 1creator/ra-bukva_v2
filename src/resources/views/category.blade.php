@extends('layouts.app')
@push('head')
    @if($category->meta_title)
        <title>{{$category->meta_title}}</title>
    @else
        <title>{{$category->name}} в Сургуте - заказать онлайн</title>
    @endif
    @if($category->meta_words)
        <meta name="description"
              content="{{$category->meta_words}}">
    @else
        <meta name="description"
              content="{{$category->name}} - рассчитать стоимость с учетом скидок в каталоге услуг.">
    @endif
    <link rel="canonical" href="{{route('categories.show', ['category'=>$category->seo_name])}}"/>
@endpush
@section('content')
    {{--todo keywords--}}
    <div class="container">
        @include('components.breadcrumbs',['items' => $category->breadCrumbs()])
        <div class="text-center mb-5">
            <h1>{{$category->h1??$category->name}}</h1>
        </div>
        <div class="row">
            @foreach($category->children as $child)
                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4">
                    @include('components.card-category', ['category'=>$child])
                </div>
            @endforeach
            @foreach($category->products as $product)
                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4">
                    @include('components.card-product', ['product'=>$product])
                </div>
            @endforeach
        </div>

        <div class="text-justify">
            {!!$category->description!!}
        </div>
    </div>
@endsection