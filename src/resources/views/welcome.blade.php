@extends('layouts.app')
@push('head')
    <title>Заказать любые виды печати и рекламы в РА Буква ПЛЮС</title>
    <meta name="description"
          content="🅱️ Рекламное агентство с собственной типографией в Сургуте. Низкие цены и отличное качество с бесплатной доставкой - убедитесь сами!">
@endpush
@section('content')
    <div class="mb-5 mt-n5">
        @include('components.slider-sales')
    </div>
    <div class="container">
        <div>
            <div class="text-center mb-4">
                <div class="mb-3">
                    <img src="/images/logo-mini.png" alt="Логотип Буква ПЛЮС">
                </div>
                <h1 class="text-uppercase">
                    Рекламное агентство <br class="d-none d-md-block"/>
                    с собственной типографией<br>
                    <span class="text-primary">Буква&nbsp;ПЛЮС</span>
                </h1>
            </div>
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1 text-justify" style="text-indent: 2rem;">
                    <p>
                        Наш издательский-производственный комплекс — отличное и удобное решение для выполнения самого
                        широкого
                        среза
                        задач.
                    </p>
                    <p>
                        Если вы ищете надежного партнера для вашего бизнеса в сфере изготовления качественной
                        полиграфической,
                        световой, демонстрационной, лазерной, графической, объемной, стеклянной, неформатной, уличной и
                        интерьерной
                        продукции, к вашим услугам издательский-производственный комплекс компании Буква ПЛЮС. За долгое
                        время,
                        мы
                        накопили знания и опыт работы на телевидение и ТВ рекламе а так же прокат роликов на ТВ,
                        радиореклама и
                        прокат роликов на радио, размещения в газете и журналах, статьи, цветные вставки, размещения на
                        автобусах,
                        брендирование автотранспорта, придорожные конструкции вдоль дорог и многое другое, необходимое
                        для
                        эффективного взаимодействия с заказчиком. Нашими клиентами стали более 7 000 компании из
                        Сургута,
                        Тюмени,
                        Екатеринбурга, Ханты-Мансийска, Москвы и Санкт-Петербурга а так же, других регионов России, а
                        наши
                        отзывы
                        становятся лучшей гарантией качества работы. Да, мы ими гордимся!
                    </p>
                    <p>
                        Производственный процесс в нашей компании отлажен годами работы, что позволяет нам гарантировать
                        качество и
                        сроки выполнения заказов. Вы можете быть уверены, что получите готовый заказ именно в тот срок,
                        который
                        был
                        указан при расчете. Мы работаем на самом современном производственном оборудовании, которое
                        обеспечивает
                        высокую скорость и стабильное качество, а команда наших профессионалов контролирует процесс на
                        всех
                        его
                        этапах. Своим клиентам мы предлагаем удобный формат онлайн-заказа продукции, быстрый
                        автоматический
                        расчет
                        стоимости заказа с учетом различных параметров. Только нашим клиентам мы предлагаем услуги
                        бесплатной
                        доставки готовой продукции. Работать с нами выгодно и удобно. Убедитесь в этом сами!
                    </p>
                </div>
            </div>
            <hr class="my-5">
        </div>
        <div class="text-center mb-5">
            <div class="h2">ВЫБЕРИТЕ ИЗДЕЛИЕ</div>
            <h3 class="h5">УЗНАЙТЕ СТОИМОСТЬ РАБОТ И ВРЕМЯ ИСПОЛНЕНИЯ</h3>
        </div>
        <div class="row">
            @foreach($products as $product)
                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4">
                    @include('components.card-product', ['product'=>$product])
                </div>
            @endforeach
        </div>
        <div class="text-center">
            <a class="btn btn-primary btn-lg" href="{{route('categories')}}">Смотреть все услуги</a>
        </div>
        <hr class="my-5">
        <div>
            <div class="text-center">
                <h3 class="h2">НАШИ КЛИЕНТЫ</h3>
                <div class="h5">НАМ ДОВЕРЯЮТ</div>
            </div>
            <div class="swiper-container clients-logo-slider">
                <div class="swiper-wrapper mb-4">
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-administraciya-surguta"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-apteka-zdorovogo-cheloveka"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-askona"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-aura"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-cinema-park"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-dns"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-etalon"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-fix-price"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-gazprom"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-kfc"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-kia"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-kuhni-mariya"></div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-lada"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-laplandia"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-lenta"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-lexus"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-mazda"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-megafon"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-mnogo-mebeli"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-monetka"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-mvideo"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-okey"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-planeta-zdorovo"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-radio-dorojnoe"></div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-renault"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-reso"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-resurs-auto"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-rostelekom"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-rzhd"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-sitimoll"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-sogaz"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-swarovski"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-toyota"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-travelers-coffee"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-volkswagen"></div>
                            </div>
                            <div class="col-4 col-sm-3 col-md-2">
                                <div class="clients-atlas-vse-dlya-svarki"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <hr class="my-5">
        <div class="text-center mb-4">
            <h3 class="h2">НАШИ ПРЕИМУЩЕСТВА</h3>
            <div class="h5">ОЧЕВИДНЫ</div>
        </div>
        <div class="row text-justify">
            <div class="col-12 col-lg-4 d-flex mb-4 mb-lg-0">
                <i class="icofont-tick-mark icofont-3x text-danger mr-3"></i>
                <div class="pt-2">
                    <h4 class="h3">КАЧЕСТВО</h4>
                    <div>
                        Мы собрали самую современную и “умную” технику на производстве. Это позволяет получить
                        лучшее
                        качество печати и сборки. Именно поэтому, все работы мы сопровождаем более длительной
                        гарантией, чем
                        у конкурентов.
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 d-flex mb-4 mb-lg-0">
                <i class="icofont-speed-meter icofont-3x text-danger mr-3"></i>
                <div class="pt-2">
                    <h4 class="h3">СКОРОСТЬ</h4>
                    <div>
                        Мы автоматизировали производственные мощности, что позволяет нам выполнять заказы в
                        кратчайшие
                        сроки. А бесплатная доставка в пределах города, позволит Вам получить готовую продукцию с
                        запасом
                        времени.
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 d-flex">
                <i class="icofont-like icofont-3x text-danger mr-3"></i>
                <div class="pt-2">
                    <h4 class="h3">ЛОЯЛЬНОСТЬ</h4>
                    <div>
                        В каждый момент времени, мы слушаем и слышим заказчика и реализуем именно тот проект, что
                        необходим.
                        Наши специалисты готовы прийти на помощь, подсказать, рассказать и предложить то решение,
                        что
                        наиболее полно удовлетворит потребности заказчика.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection