@component('mail::message')
# Заказ успешно создан

Добрый день! Ваш заказ успешно создан и скоро будет нами проверен.
Вы всегда можете отслеживать выполнение заказа <a href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">на этой странице</a> @if($order->user)или <a href="{{url('/cabinet/orders')}}">в личном кабинете</a>@endif.

@component('mail::table')
| # | Услуга | Кол-во | Стоимость |
|:---|:--------|:--------|-----------:|
@foreach($order->orderProducts as $index=>$orderProduct)
| {{$index+1}} | {{$orderProduct->product->name}} {{$orderProduct->need_layout?' + макет':''}} | {{$orderProduct->count}} {{$orderProduct->product->count_type}} | {{$orderProduct->total}} ₽ |
@endforeach
|  | | Всего | {{$order->total}} ₽ |
@endcomponent

@component('mail::button', ['url' => route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])])
Открыть страницу заказа
@endcomponent

<small>
С уважением,<br>
Команда рекламного агентства Буква-Плюс<br>
<a href="mailto:950250@bk.ru">950250@bk.ru</a>  <a href="tel:83462950250">+7(3462)950-250</a>
</small>
@endcomponent

