<div style="background:white;color: #484848; font-size: 14px;">
    @include('mail.orders.header')
    <div style="padding: 2rem;border: 5px solid #e31e2b;border-radius: 20px;margin: 30px 0 30px 0;">
        <div>
            <div style="margin-bottom: 15px;">К этому письму приложены следующие электронные копии документов
                <a style="color: #e31e2b;font-weight: bold;"
                   href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">заказа №{{$order->id}}</a>
                от {{$order->created_at->locale('ru')->isoFormat('DD.MM.YYYY')}}:
            </div>
        </div>
        <ol style="margin-bottom: 15px;">
            @foreach($docNames as $index=>$docName)
                <li>{{$docName}}</li>
            @endforeach
        </ol>
        <div style="text-align: center;margin-bottom: 15px;">
            <a style="color: #ffffff;text-decoration: none;
                display: inline-block;padding: 7px 15px; border-radius: 10px;
                font-weight: bold;background: #e31e2b;"
               href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">На страницу
                заказа</a>.
        </div>
        <div style="margin-bottom: 15px;color: #e31e2b;">
            <i>Пожалуйста, не отвечайте на данное письмо - оно сформировано автоматически.</i>
        </div>
    </div>
    @include('mail.orders.footer')
</div>