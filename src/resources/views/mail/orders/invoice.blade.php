@component('mail::message')
# Счёт на оплату заказа #{{$order->id}}

Добрый день. К этому письму приложена электронная копия счёта на оплату заказа №{{$order->id}}, совершенного на сайте рекламного агентства Буква ПЛЮС.

<small>
С уважением,<br>
Команда рекламного агентства Буква-Плюс<br>
<a href="mailto:950250@bk.ru">950250@bk.ru</a>  <a href="tel:83462950250">+7(3462)950-250</a>
</small>
@endcomponent

