<div style="background:white;color: #484848; font-size: 14px;">
    @include('mail.orders.header')
    <div style="padding: 2rem;border: 5px solid #e31e2b;border-radius: 20px;margin: 30px 0 30px 0;">
        <div>
            <div style="margin-bottom: 15px;">
                Вы или кто-то другой запросил код восстановления пароля на сайте ra-bukva.com:
                <h1>{{ $code }}</h1>
            </div>
        </div>
        <div style="margin-bottom: 15px;color: #e31e2b;">
            <i>Просто проигнорируйте это письмо если вы не запрашивали восстановление пароля.</i>
        </div>
    </div>
    @include('mail.orders.footer')
</div>
