@component('mail::message')
# Есть изменения в вашем заказе

Статус позиции вашего заказа обновлён.

{{$orderProduct->product->name}} {{$orderProduct->need_layout?' + макет':''}}<br>
Новый статус: {{__('order.'.$orderProduct->status)}}

@component('mail::button', ['url' => route('orders.show',['order'=>$orderProduct->order->id, 'secret'=>$orderProduct->order->secret])])
Открыть страницу заказа
@endcomponent

<small>
С уважением,<br>
Команда рекламного агентства Буква-Плюс<br>
<a href="mailto:950250@bk.ru">950250@bk.ru</a>  <a href="tel:83462950250">+7(3462)950-250</a>
</small>
@endcomponent

