<div style="background:white;color: #484848; font-size: 14px;">
    @include('mail.orders.header')
    <div style="padding: 2rem;border: 5px solid #e31e2b;border-radius: 20px;margin: 30px 0 30px 0;">
        <div>
            <div style="margin-bottom: 15px;">Статус <a style="color: #e31e2b;font-weight: bold;"
                                                        href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">заказа
                    №{{$order->id}}</a>
                от {{$order->created_at->locale('ru')->isoFormat('DD.MM.YYYY')}} изменён.<br/>
            </div>
        </div>
        <table style="width: 100%; margin-bottom: 15px;">
            <thead>
            <tr style="font-weight: bold;color: #039c32;">
                <td>Указанные работы</td>
                <td>Кол-во</td>
                <td style="text-align: right;">Статус</td>
            </tr>
            </thead>
            <tbody>
            @foreach($order->orderProducts as $index=>$orderProduct)
                <tr>
                    <td>{{$orderProduct->product->name}} {{$orderProduct->need_layout?' + макет':''}}</td>
                    <td>
                        {{$orderProduct->count}} {{$orderProduct->product->count_type}}
                    </td>
                    <td style="text-align: right;">{{__('order.'.$orderProduct->status)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($onPayment??false)
            <p>Все позиции заказа находятся в статусе <b>"Ждёт оплаты"</b>.</p>
            <p>Вы можете произвести оплату прямо на странице заказа.
                @if($attachInvoice??false)
                    Для юридических лиц мы также прикрепили счёт на оплату во вложении.
                @endif
            </p>
        @endif
        <div style="margin-bottom: 15px;">
        </div>
        <div style="text-align: center;margin-bottom: 15px;">
            <a style="color: #ffffff;text-decoration: none;
                display: inline-block;padding: 7px 15px; border-radius: 10px;
                font-weight: bold;background: #e31e2b;"
               href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">На страницу
                заказа</a>.
        </div>
        <div style="margin-bottom: 15px;color: #e31e2b;">
            <i>Пожалуйста, не отвечайте на данное письмо - оно сформировано автоматически.</i>
        </div>
    </div>
    @include('mail.orders.footer')
</div>
