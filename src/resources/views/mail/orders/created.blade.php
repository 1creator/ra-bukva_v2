<div style="background:white;color: #484848; font-size: 14px;">
    @include('mail.orders.header')
    <div style="padding: 2rem;border: 5px solid #e31e2b;border-radius: 20px;margin: 30px 0 30px 0;">
        <div>
            <div style="margin-bottom: 15px;"><b>Ваш заказ оформлен!</b></div>
            <div style="margin-bottom: 15px;">Ваш <a style="color: #e31e2b;font-weight: bold;"
                                                     href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">заказ
                    №{{$order->id}}</a>
                от {{$order->created_at->locale('ru')->isoFormat('DD.MM.YYYY')}} оформлен и ожидает проверки.<br/>
                После успешной проверки и поступления денежных средств мы приступим к работе.
            </div>
            <div style="margin-bottom: 15px;">Вы можете отслеживать выполнение
                <a style="color: #e31e2b;font-weight: bold;"
                   href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">На странице
                    заказа</a>.
            </div>
            <div style="margin-bottom: 15px;">При возникновении каких-либо вопросов наш менеджер свяжется с Вами по
                почте
                <a href="mailto:{{$order->email}}" style="color: #e31e2b;font-weight: bold;">
                    {{$order->email}}</a> или по
                номеру <a href="tel:$order->phone" style="color: #e31e2b;font-weight: bold;">{{$order->phone}}</a>.
            </div>
        </div>
        <table style="width: 100%; margin-bottom: 15px;">
            <thead>
            <tr style="font-weight: bold;color: #039c32;">
                <td>Указанные работы</td>
                <td>Кол-во</td>
                <td style="text-align: right;">Стоимость</td>
            </tr>
            </thead>
            <tbody>
            @foreach($order->orderProducts as $index=>$orderProduct)
                <tr>
                    <td>{{$orderProduct->product->name}} {{$orderProduct->need_layout?' + макет':''}}</td>
                    <td>
                        {{$orderProduct->count}} {{$orderProduct->product->count_type}}
                    </td>
                    <td style="text-align: right;">{{$orderProduct->total}} ₽</td>
                </tr>
            @endforeach
            @if($order->delivery_coordinates)
                <tr>
                    <td>Доставка</td>
                    <td>1 шт.</td>
                    <td style="text-align: right;">{{number_format($order->delivery_price, 2,',',' ')}} ₽</td>
                </tr>
            @endif
            <tr style="text-align: right;">
                <td colspan="3">
                    Общая стоимость заказа:
                    <span style="margin-left:10px; color: #039c32;">{{$order->total}} ₽</span>
                </td>
            </tr>
            </tbody>
        </table>
        <div style="margin-bottom: 15px;">
            <b style="color: #e31e2b;">Способ оплаты:</b> Счет на оплату.<br/>
            <i><b style="color: #e31e2b;">Внимание!</b> Передача осуществляется при наличии акта или товарной накладной.
                Заверенные синей печатью.</i>
        </div>
        <div style="margin-bottom: 15px;">
            @if($order->delivery_address)
                <b style="color: #e31e2b;">Способ передачи:</b> Доставка по городу.<br/>
                <b style="color: #e31e2b;">Адрес доставки:</b> {{$order->delivery_address}}.<br/>
            @else
                <b style="color: #e31e2b;">Способ передачи:</b> Самовывоз.<br/>
            @endif
        </div>
        <div style="margin-bottom: 15px;color: #e31e2b;">
            <i>Пожалуйста, не отвечайте на данное письмо - оно сформировано автоматически.</i>
        </div>
    </div>
    @include('mail.orders.footer')
</div>