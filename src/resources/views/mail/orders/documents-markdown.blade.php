@component('mail::message')
# Документы к заказу #{{$order->id}}

Добрый день.<br>
К этому письму приложены следующие электронные копии документов заказа, совершенного на сайте рекламного агентства Буква ПЛЮС:<br>
@foreach($docNames as $index=>$docName)
{{$index+1}}. {{$docName}}<br>
@endforeach

<small>
С уважением,<br>
Команда рекламного агентства Буква-Плюс<br>
<a href="mailto:950250@bk.ru">950250@bk.ru</a>  <a href="tel:83462950250">+7(3462)950-250</a>
</small>
@endcomponent

