<div style="background:white;color: #484848; font-size: 14px;">
    @include('mail.orders.header')
    <div style="padding: 2rem;border: 5px solid #e31e2b;border-radius: 20px;margin: 30px 0 30px 0;">
        <div>
            <div style="margin-bottom: 15px;"><b>Ваш заказ выполнен!</b></div>
            <div style="margin-bottom: 15px;">Ваш <a style="color: #e31e2b;font-weight: bold;"
                                                     href="{{route('orders.show',['order'=>$order->id, 'secret'=>$order->secret])}}">заказ
                    №{{$order->id}}</a>
                от {{$order->created_at->locale('ru')->isoFormat('DD.MM.YYYY')}} выполнен.<br/>
                Пожалуйста, заберите заказ.
            </div>
        </div>
        <table style="width: 100%; margin-bottom: 15px;">
            <thead>
            <tr style="font-weight: bold;color: #039c32;">
                <td>Указанные работы</td>
                <td>Кол-во</td>
                <td style="text-align: right;">Статус</td>
            </tr>
            </thead>
            <tbody>
            @foreach($order->orderProducts as $index=>$orderProduct)
                <tr>
                    <td>{{$orderProduct->product->name}} {{$orderProduct->need_layout?' + макет':''}}</td>
                    <td>
                        {{$orderProduct->count}} {{$orderProduct->product->count_type}}
                    </td>
                    <td style="text-align: right;">{{__('order.'.$orderProduct->status)}}</td>
                </tr>
            @endforeach
            @if($order->delivery_coordinates)
                <tr>
                    <td>Доставка</td>
                    <td>1 шт.</td>
                    <td style="text-align: right;">{{number_format($order->delivery_price, 2,',',' ')}} ₽</td>
                </tr>
            @endif
            </tbody>
        </table>
        <div style="margin-bottom: 15px;">
            <i><b style="color: #e31e2b;">Внимание!</b> Передача осуществляется при наличии акта или товарной накладной.
                Заверенные синей печатью.</i>
        </div>
        <div style="margin-bottom: 15px;color: #e31e2b;">
            <i>Пожалуйста, не отвечайте на данное письмо - оно сформировано автоматически.</i>
        </div>
    </div>
    @include('mail.orders.footer')
</div>