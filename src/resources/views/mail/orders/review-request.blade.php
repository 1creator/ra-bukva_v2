<div style="background:white;color: #484848; font-size: 14px;">
    @include('mail.orders.header')
    <div style="padding: 2rem;border: 5px solid #e31e2b;border-radius: 20px;margin: 30px 0 30px 0;">
        <div>
            <div style="margin-bottom: 15px;">
                <h1>Добрый день!</h1>
                <p>
                    Несколько дней назад мы выполнили для вас заказ на сайте ra-bukva.com.
                </p>
                <p>
                    Мы каждый день стараемся улучшить предоставляемый сервис и качество обслуживания. Пожалуйста,
                    оставьте отзыв о вашем заказе на странице заказа. Это займёт не более 3 минут.
                </p>
            </div>
        </div>
        <div style="margin-bottom: 15px;color: #e31e2b;">
            <i>Это письмо сгенерировано автоматически, пожалуйста, не отвечайте на него.</i>
        </div>
        <div style="text-align: center;margin-bottom: 15px;">
            <a style="color: #ffffff;text-decoration: none;
                display: inline-block;padding: 7px 15px; border-radius: 10px;
                font-weight: bold;background: #e31e2b;"
               href="{{ $orderUrl }}">На страницу заказа</a>.
        </div>
    </div>
    @include('mail.orders.footer')
</div>
