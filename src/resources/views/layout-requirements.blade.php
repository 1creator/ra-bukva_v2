@extends('layouts.app')
@push('head')
    <title>Требования к загружаемым макетам | Буква ПЛЮС</title>
@endpush
@section('content')
    {{--todo закрыть от индекса--}}
    <div class="container text-justify">
        @include('components.breadcrumbs',['items' => [
            ['name' => 'Главная', 'link' => '/'],
            ['name' => 'Требования к макетам'],
        ]])
        <h1>ТРЕБОВАНИЯ К МАКЕТАМ</h1>
        <p class="mb-5">Для Вашего и нашего удобства а так же экономии времени и сроков выполнения заказа, пожалуйста
                        ознакомьтесь и
                        обязательно покажите данную страницу Вашим дизайнерам!</p>
        <div class="mb-5">
            <div class="h3">PDF</div>
            <ul>
                <li>Cовместимость файлов с форматом PDF 1.3 (Adobe Acrobat 4.0);</li>
                <li>допустимое суммарное количество краски Total Ink Limit = 300%;</li>
                <li>разрешение не менее 300 dpi и не более 450 dpi для цветных и полутоновых растровых изображений;</li>
                <li>разрешение не менее 1200 dpi и не более 1800 dpi для монохромных растровых изображений;</li>
                <li>ZIP-сжатие растровых изображений;</li>
                <li>цветовое пространство только CMYK или Grayscale, RGB недопустимо;</li>
                <li>отсутствие сжатия нежелательно, а JPEG – недопустимо;</li>
                <li>всем дополнительным цветам назначены цвета Pantone;</li>
                <li>все используемые шрифты внедрены (embedded), в т. ч. с подстановкой (subset);</li>
                <li>установка симметричных «вылетов за обрез» (bleed) равных 3 мм, для многополосных изделий 5 мм;</li>
            </ul>
            <p>
                Избегайте преобразований градиентных заливок или контуров и других аналогичных объектов в «smooth shade»
                – такие объекты нередко ошибочно обрабатываются многими программами спуска полос и растровыми
                процессорами (Raster Image Processor – RIP), мы не гарантируем качественную печать таких объектов – их
                рекомендуется растрировать до преобразования макета в PDF-формат.
            </p>
            <p>При создании PDF, в настройках управления цветом (Color Management) рекомендуется установить «No
               Сonvertion».
               Цветовые IСС профили в файл pdf включать не следует.
            </p>
            <p>Все макеты должны быть композитными, при необходимости с дополнительными «спотовыми» (spot colors)
               красками. Цветоделенные макеты не принимаются.
            </p>
            <p>Предоставляйте макеты в соответствующей ориентации, книжные в книжной, альбомные в альбомной.
            </p>
            <p>Макет должен быть предоставлен пополосно в одном файле. Исключение составляют обложки для КБС – в этом
               случае обложка должна быть сверстана разворотом, а корешок выделен технологическими метками.
               Желательно, чтобы PDF-файлы соответствовали стандарту PDF/X-1a:2001.</p>
        </div>
        <div class="mb-5">
            <div class="h3">TIFF ИЛИ PSD</div>
            <ul>
                <li>«вылеты за обрез» 5 мм,
                <li>допустимое суммарное количество краски Total Ink Limit = 300%</li>
                <li>разрешение не менее 300 dpi и не более 450 dpi для цветных и полутоновых растровых изображений;</li>
                <li>разрешение не менее 1200 dpi и не более 2400 dpi для монохромных растровых изображений;</li>
                <li>разрешение в файле должно быть задано исключительно в «точках на дюйм» (dpi);</li>
                <li>если используется черный текст (100% Black) и файл создается в Photoshop, то рекомендуется для этого
                    текстового слоя устанавливать атрибут «Наложение» (Multiply).
                </li>
                <li>файл не должен содержать слоев. При сохранении из Photoshop должна быть отключена опция «Слои»
                    (Layers) и установлено LZW-сжатие.
                </li>
                <li>цветовое пространство только CMYK или Grayscale, RGB недопустимо.</li>
            </ul>
        </div>
        <div class="mb-5">
            <div class="h3">CDR</div>
            <ul>
                <li>Формат документа должен соответствовать обрезному формату изделия.</li>
                <li>Все тексты должны быть переведены в кривые.</li>
                <li>При использовании таких эффектов, как прозрачность, тень, линза, gradient mesh и т.п. все элементы,
                    содержащие перечисленные эффекты, необходимо растрировать с фоном в единый объект.
                </li>
                <li>Разрешается использовать только CMYK цвета. Палитра CMYK255 в CorelDraw! не является палитрой CMYK и
                    не должна использоваться.
                </li>
                <li>Все включенные в CorelDraw! макет растровые изображения должны быть внедрены в файл.</li>
                <li>Недопустимо использование эффекта прозрачных линз.</li>
                <li>Недопустимо использование объектов, импортированных с помощью OLE или DDE.</li>
                <li>Недопустимо оставлять в макете объекты типа “symbol”, нужно разбивать их на объекты. В противном
                    случае не гарантируется корректная печать макета.
                </li>
            </ul>
            <p>
                Требования к вылетам, растровым изображениям и прочим элементам такие же, как и к макетам в формате PDF.
            </p>
        </div>
        <div class="mb-5">
            <div class="h3">ОБЩИЕ ПРАВИЛА ВЁРСТКИ</div>
            <p>Располагайте макет по центру листа. Размер листа должен быть равен послеобрезному формату (то есть не
               нужно класть, например, визитку 90х50 на лист А4 – положите её на лист 90х50). Оборот кладите на
               следующий лист или в другой файл, а не рядом с лицом. Направляющие, рамки и т.п. не являются показателем
               обрезного формата.</p>
            <p>Лицо и оборот одного изделия должны быть на отдельных страницах, разные изделия должны быть в разных
               файлах.</p>
            <p>Предоставляйте макеты в соответствующей ориентации, книжные в книжной, альбомные в альбомной.</p>
            <p>Комментарии к макету, содержащиеся в файле вёрстки, игнорируются. Пожалуйста, пишите их менеджеру в
               сопроводительном письме.</p>
            <p>На макет с последующей вырубкой должен быть наложен контур вырубного штампа (на отдельном слое). При этом
               нужно понимать, что штамп мы изготавливаем согласно вашему контуру. Поэтому если у вас в штампе должна
               быть биговка, её нужно показать пунктирной линией, а не сплошной. В противном случае такая линия
               воспринимается как вырубной нож. Файл штампа должен был выполнен в векторном виде.</p>
            <p>Не обозначайте в pdf спотовыми цветами лак, тиснение, штампы и прочую постпечатную обработку. При приёме
               макета автоматика может некорректно обработать такие файлы, и что-нибудь может слететь. Вместо этого
               лак, штампы высылайте отдельными pdf файлами.</p>
            <p>Выборочный лак, конгрев, тиснение и т. п. должны быть в векторном виде, окрашены в 100% чёрного и
               находиться на отдельном слое, точно над той областью в макете, на которую наносятся.</p>
            <p>Если какой-либо элемент верстки вплотную подходит к краю, то он должен быть выпущен за обрез. Вынос за
               обрезной формат должен быть 3 мм, для многостраничных изданий 5 мм.</p>
            <p>Располагайте значимую информацию не ближе 3 мм от линии реза.</p>
            <p>Мы печатаем лицо и оборот, исходя из расположения и порядка полос в вашем файле. Недопустимо
               предоставлять лицо и оборот в разных ориентациях (например, лицо календарика – в книжной, а оборот – в
               альбомной).</p>
            <p>Для корректной фальцовки в буклетах с двумя фальцами третья (внутренняя) полоса должна быть меньше на 2-3
               мм (например, полосы в евробуклете: оборот 100х100х97, лицо 97х100х100, неправильно: 99х99х99).</p>
            <p>При разработке макета для печати на бумаге плотностью более 150 г/м2, с дальнейшей фальцовкой, не
               рекомендуется использовать в местах сгибов плотную, темную заливку, так как могут возникнуть заломы и
               трещины в области сгиба, что не лучшим образом отразится на внешнем виде вашего изделия.</p>
        </div>
        <div class="mb-5">
            <div class="h3">КРАСОЧНОСТЬ И ОВЕРПРИНТЫ</div>
            <p>Все объекты должны быть в CMYK. Нельзя использовать RGB и другие цветовые модели, не преобразованные
               цвета переводятся автоматически, при этом возможно искажение цвета.</p>
            <p>Если необходима печать дополнительными красками, в том числе металлизированными, такими как серебро и
               бронза, они указываются в макете по шкале PANTONE solid. Их использование оговаривается с нашим
               менеджером.</p>
            <p>Сумма красок не должна превышать 300%.</p>
            <p>Процент содержания каждой краски должен быть не менее 5%, цветозаполнение 1%-4% не гарантируется.</p>
            <p>Процент содержания каждой краски более 90% будет выглядеть как сплошная заливка. Для хорошего визуального
               отличия между объектами разница краски у них должна быть не менее 15%.</p>
            <p>Крупные по площади чёрные объекты красьте глубоким чёрным (например, c25 m20 y20 k100). Никогда не
               окрашивайте в составной чёрный мелкий текст.</p>
            <p>Необходимо следить за использованием атрибутов Overprint и Knockout. В общем случае атрибут Overprint
               отключается, и ставится только на черные объекты. Черными считаются объекты, которым назначен цвет 100%
               Black, без других составляющих. Применение атрибута Overprint к другим цветам, должно быть оговорено при
               передаче файлов в работу.</p>
            <p>Проследите, чтобы под крупными по площади чёрными векторными объектами не было объектов другого цвета или
               покрасьте их в глубокий чёрный. В противном случае они могут проступить из-под чёрной краски.</p>
            <p>Не рекомендуется в растяжках комбинировать разные цветовые пространства. Например, растяжка из c100m0y0k0
               в grayscale:100 (голубой в чёрный) может стать чёрно-белой.</p>
        </div>
        <div class="mb-5">
            <div class="h3">РАСТРОВЫЕ ФОРМАТЫ И СВЯЗАННЫЕ С МАКЕТОМ ФАЙЛЫ</div>
            <p>Для растровых объектов необходимым и достаточным является разрешение, в 1,5–2 раза превышающее линиатуру
               (в нашей типографии 175 lpi) и соответственно должно находиться в диапазоне 260–350 dpi, кроме
               Black&White объектов (до 1200 dpi). Мы оставляем за собой право уменьшить избыточное разрешение до 350
               dpi.</p>
            <p>Запрещается использовать OLE-объекты (например, таблицы Excel, текст из Word, картинки, скопированные
               через клипборд (ctrl+c / ctrl+v) в вёрстку), используйте команду „Import” („Place”).</p>
            <p>Все связанные с макетом файлы должны быть собраны в одну папку, эта же папка должна содержать файл
               верстки.</p>
            <p>Нельзя при работе в CorelDRAW пользоваться внешними линками (Externally linked bitmap). Все изображения
               должны быть внедрены в вёрстку.</p>
            <p>Нельзя сохранять в растровом файле слои (Layers), альфа-каналы и цветовой профиль (ICC Profile). Склейте
               слои командой Flatten layers, при записи снимите галку “Include ICC-profile”.</p>
            <p>Растровый файл должен быть сохранен в режиме 8 bit (в программе Photoshop это можно проверить в меню
               Image\Mode).</p>
            <p>Не применяйте LZW-компрессию в изображениях. Это усложняет автоматическую проверку макета и может
               привести к пропаже картинки.</p>
        </div>
        <div class="mb-5">
            <div class="h3">ЭФФЕКТЫ И ОБТРАВЛЕННЫЕ ИЗОБРАЖЕНИЯ</div>
            <p>Недопустимо использование встроенных Pattern, Texture и Postscript заливок, элементы с такими заливками
               необходимо растрировать с фоном в единый объект.</p>
            <p>Растровые изображения с прозрачным фоном и/или повернутые на угол, отличный от 90, 180 или 270 градусов,
               должны быть растрированы с фоном в единый объект.</p>
            <p>При использовании таких эффектов, как прозрачность, тень, линза, gradient mesh и т.п. все элементы,
               содержащие перечисленные эффекты, необходимо растрировать с фоном в единый объект.</p>
            <p>В СorelDRAW все эффекты (кроме PowerClip) и сложные градиенты должны быть отделены командой Break Apart
               или растрированы в единый объект. С контейнера PowerClip нельзя снимать блокировку содержимого (Lock
               Contents to PowerClip): при перемещении PowerClip содержимое должно перемещаться вместе с ним.</p>
            <p>Максимальное количество точек в векторном объекте не должно превышать 3000, в противном случае подобный
               объект должен быть упрощен или разбит на несколько. Большое количество точек может привести к потере
               объекта.</p>
            <p>Не используйте прозрачность с растровыми объектами, окрашенными в спотовые цвета (monotone, duotone и
               т.д.). Это может привести к пропаданию объектов.</p>
        </div>
        <div class="mb-5">
            <div class="h3">ЛИНИИ И МЕЛКИЕ ОБЪЕКТЫ</div>
            <p>Мелкие объекты, мелкий текст и тонкие линии выглядят лучше, если они окрашены только одной из четырех
               составляющих CMYK (или пантоном с плотностью краски 100%). Составной цвет может привести к появлению
               цветных ореолов вокруг покрашенных им объектов.</p>
            <p>Не рекомендуется делать мелкие белые объекты, мелкий белый текст и тонкие белые линии на фоне, состоящем
               из нескольких красок, так как они могут не пропечататься или пропечататься частично.</p>
            <p>Толщина одноцветной линии должна быть больше 0,08 мм, линии меньшей толщины могут не пропечататься или
               пропечататься частично. При использовании в макете столь тонких линий учтите, что мы автоматически
               увеличиваем толщину всех линий до 0,08 мм, если она меньше этого значения. Толщина многоцветных линий и
               белых линий на составном фоне должна быть не меньше 0,176 мм.</p>
            <p>Если не избежать использования в линиях нескольких цветов или цвет один, но не 100%, делайте толщину
               линий максимально возможной.</p>
        </div>
        <div class="mb-5">
            <div class="h3">МНОГОСТРАНИЧНЫЕ ИЗДАНИЯ</div>
            <p>Каждая полоса каталога должна быть на отдельной странице, нельзя предоставлять полосы разворотами (1-2,
               2-3 и т.д.) или спуском (8-1, 2-7 и т.д.). Исключение – каталоги с нестандартной фальцовкой (например,
               лесенкой).</p>
            <p>Все полосы должны находятся в одном файле, проследите, чтобы их последовательность, включая обложку, была
               верной. Мы определяем последовательность полос, исходя из Вашего файла.</p>
            <p>Издания на пружину должны учитывать ширину отверстий под пружину и их расположение на изделии, чтобы
               значимая информация не попадала на отверстия. Отверстия делаются на расстоянии 7 мм от края изделия и
               имеют диаметр 4 мм.</p>
            <p>Обложка для сборки на термоклей должна предоставляться разворотом (4+1, 2+3 страницы обложки) с учетом
               толщины корешка. На внутренней стороне корешок плюс 3 мм слева и справа от него должны быть белыми (без
               краски, лака и др. нанесений), т.к. на это место будет наноситься клей и обложка будет как бы
               «охватывать» блок издания. Если у вас на внутренней полосе обложки и полосе блока свёрстана картинка
               разворотом, учтите это, чтобы на границе обложки и блока картинка состыковывалась как должно.</p>
        </div>
        <div class="mb-5">
            <div class="h3">УФ-ЛАК</div>
            <p>Минимальная толщина линии 0,5 мм. Вылеты 3 мм. В местах фальцовок, биговок и на корешке каталогов на
               скрепку необходимо делать выборку 1,5-2 мм. Для каталогов на термоклей необходима выборка по всей
               площади корешка плюс по 3 мм для клея с обеих сторон корешка. Клеевые клапаны у диджипаков, ключниц,
               коробок, папок не должны лакироваться.</p>
            <p>Требования к объемному лаку: максимальная площадь сплошной заливки 50х50 мм, минимальная толщина линий
               для объемного лака – 1 мм. При подготовке макета нужно помнить, что лак не должен доходить до линии реза
               на 1,5-2 мм, в местах биговок, фальцовок обязательна выворотка 3-4 мм. Клеевые клапаны у диджипаков,
               ключниц, коробок, папок не должны лакироваться.</p>
            <p>При наложении УФ-лака на офсетное изображение следует избегать толщины линии менее трёх миллиметров, при
               этом треппинг изображения на УФ-лак должен составлять 0,3 мм.</p>
            <p>Если изображение, на которое накладывается выборочный УФ-лак, светлее, чем фон, то пленки на лак
               выводятся МЕНЬШЕ этого изображения на 0,3 мм с каждой стороны. Если изображение темнее, чем фон, то
               пленки на лак выводятся БОЛЬШЕ на 0,3 мм с каждой стороны.</p>
            <p>Если лак не накладывается поверх изображения, а печатается независимо, то минимальная толщина линии
               составляет 0,5 мм.</p>
            <p>При создании макета следует избегать большого разброса мелких объектов на площади печатного листа.
               Желательно, чтобы лакируемые объекты располагались группой в одной части листа, либо заполняли всю
               площадь листа.</p>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 mb-5">
                <div class="h3">ТИСНЕНИЕ</div>
                <p>
                    Ширина линий тиснения должна быть не менее 0,5 мм, расстояние между линиями тиснения должно быть не
                    менее 0,8 мм. Расстояние от плашки до тонкой линии должно быть не менее 1 мм. В противном случае
                    типография не гарантирует чёткости при тиснении.
                </p>
            </div>
            <div class="col-12 col-md-6 mb-5">
                <div class="h3">ШРИФТЫ</div>
                <p>Наличие шрифтов допустимо только в программе InDesign или в PS-/PDF-файлах. Во всех остальных случаях
                   переводите шрифты в кривые.</p>
                <p>Не используйте системные шрифты, такие как Arial, Courier, Times, Symbol, Windings, Tahoma и т.п.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 mb-5">
                <div class="h3">КОНГРЕВ</div>
                <p>
                    Толщина линии для конгрева не должна быть менее 0,3 мм. Разница между самыми глубокими и самыми
                    мелкими элементами клише не должна превышать 0,2 мм.</p>
            </div>
            <div class="col-12 col-md-6 mb-5">
                <div class="h3">ЛАМИНАЦИЯ</div>
                <p>Цвет после ламинации неизбежно немного меняется: становится насыщенней, чуть темнеет под глянцевой
                   плёнкой и несколько тускнеет под матовой.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 mb-5 mb-md-0">
                <div class="h3">КОНВЕРТЫ</div>
                <p>
                    Сверху у конверта есть зона, на котором печать невозможна. Технологическое поле составляет 12
                    мм.</p>
            </div>
            <div class="col-12 col-md-6">
                <div class="h3">КОРОБКИ</div>
                <p>Клеевой клапан на коробках должен быть пустым: ни краска, ни лак не могут присутствовать в месте, где
                   наносится клей.</p>
            </div>
        </div>
    </div>
@endsection
