<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Коммерческое предложение</title>
    <style>

        html {
            font-size: 14px;
        }

        .small {
            font-size: 12px;
        }

        .mb-15px {
            margin-bottom: 15px;
        }

        hr {
            margin: 15px 0;
        }

        table {
            border-collapse: collapse;
        }

        table.v-middle td {
            vertical-align: middle;
        }

        table.table_simple thead td {
            border-bottom: 3px solid black;
        }

        table td {
            padding: 15px;
        }

        table tr td:first-of-type {
            padding-left: 0;
        }

        table tr td:last-of-type {
            padding-right: 0;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>
<div>
    <table class="v-middle">
        <tbody>
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <img src="https://ra-bukva.com/images/logo.svg" style="width: 300px">
            </td>
            <td style="width: 50%;vertical-align: top;" class="small">
                <div>
                    <span>Рекламно-производственный комплекс Буква-Плюс
                        - надежный партнер для вашего бизнеса!</span>
                </div>
                <div>
                    <span>https://ra-bukva.com | 950250@bk.ru</span>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <table width="100%" class="small">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <div>
                    <b>Исполнитель:</b>
                    ООО РА "Буква Плюс",
                </div>
                <div>
                    <b>Адрес местонахождения:</b>
                    628403, Тюменская обл., ХМАО-Югра, г. Сургут, ул. 30 лет Победы, 33а
                </div>
                <div>
                    <b>ИНН /КПП:</b>
                    8602256565 / 860201001
                </div>
                <div>
                    <b> Р/счет:</b>
                    40702810167170000266
                </div>
                <div>
                    <b> Наименование банка:</b>
                    ЗАПАДНО-СИБИРСКИЙ БАНК ПАО СБЕРБАНК Г. ТЮМЕНЬ
                </div>
                <div>
                    <b> К/счет:</b>
                    30101810800000000651
                </div>
                <div>
                    <b> БИК:</b>
                    047102651
                </div>
                <div>
                    <b> Телефон:</b>
                    8 (3462) 950-250
                </div>
                <div>
                    <b>Email:</b>
                    950250@bk.ru
                </div>
            </td>
        </tr>
    </table>
    <hr width=100% color=#cccccc size=0.5>
    <div class="text-center">
        <b>Коммерческое предложение №{{$order->id}} от {{$order->created_at->locale('ru')->isoFormat('DD.MM.YYYY')}}</b>
    </div>
    <table style="width: 100%; margin-bottom: 30px;" class="table_simple">
        <thead>
        <tr>
            <td></td>
            <td><b>Наименование услуги</b></td>
            <td><b>Количество</b></td>
            <td align="right"><b>Стоимость</b></td>
        </tr>
        </thead>
        <tbody>
        @foreach($order->orderProducts as $index=>$orderProduct)
            <tr>
                <td>{{$index+1}}</td>
                <td>
                    <b>{{$orderProduct->product->name}} {{$orderProduct->need_layout?'+ макет':''}}</b>
                    <div class="small">
                        @foreach($orderProduct->parameters() as $parameter)
                            <div>
                                {{$parameter->name}}:
                                @include('components.parameter-values-representation',
                                    ['type'=>$parameter->type, 'values'=>$parameter->user_values])
                            </div>
                        @endforeach
                    </div>
                </td>
                <td>{{$orderProduct->count}} {{$orderProduct->product->count_type}}</td>
                <td align="right">{{number_format($orderProduct->total, 2,',',' ')}} ₽</td>
            </tr>
        @endforeach
        @if($order->delivery_coordinates)
            <tr>
                <td class="font-weight-normal">
                    {{$order->orderProducts->count()+1}}
                </td>
                <td>Доставка</td>
                <td>1 шт.</td>
                <td align="right">{{number_format($order->delivery_price, 2,',',' ')}} ₽</td>
            </tr>
        @endif
        </tbody>
    </table>
    <div class="mb-15px" align="right">
        <div class="mb-15px">
            @if($order->partner_sale > 0)
                <div class="">
                    <b>Корпоративная скидка:</b>
                    {{$order->partner_sale*100}}%
                </div>
            @endif
            @if($order->bonuses_spent > 0)
                <div class="">
                    <b>Скидка бонусными рублями:</b>
                    {{$order->bonuses_spent}} ₽
                </div>
            @endif
        </div>
        <div>
            <b>Всего к оплате:</b>
            {{number_format($order->total, 2,',',' ')}} ₽
        </div>
        <div>
            <div>
                НДС не облагается
            </div>
        </div>
    </div>
    <div>
        <table>
            <tr>
                <td>Генеральный директор</td>
                <td rowspan="2">
                    <img src="https://ra-bukva.com/images/stamp.png" style="height: 4cm;">
                </td>
                <td>/Чернов П.А./</td>
            </tr>
            <tr>
                <td>Главный бухгалтер</td>
                <td>/Чернов П.А./</td>
            </tr>
        </table>
    </div>
    <div>
        <div>
            <b>Обратите внимание.</b>
        </div>
        <div style="padding-left: 15px;">
            <div>
                1. При общей сумме заказа равной или свыше {{ $freeDeliveryFrom }} руб.
                доставка в пределах города Сургута – бесплатна.
            </div>
            <div>
                2. В стоимость включены все расходы.
            </div>
        </div>
    </div>

</div>
</body>
</html>
