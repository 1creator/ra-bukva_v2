<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="generator" content="PhpSpreadsheet, https://github.com/PHPOffice/PhpSpreadsheet">
    <title>УПД {{$contract['fullName']}}</title>
    <meta name="title" content="Untitled Spreadsheet"/>
    <meta name="company" content="Microsoft Corporation"/>
    <style type="text/css">
        @page {
            size: landscape;
        }

        html {
            font-size: 7pt;;
        }

        .img-wrap {
            position: relative;
        }

        .sign {
            height: 10mm;
            /*z-index: -1;*/
            margin-top: -10px;
            margin-bottom: -10px;
            position: relative;
        }

        .stamp {
            height: 40mm;
            /*z-index: -1;*/
            /*margin-left: 17mm;*/
            /*margin-right: -17mm;*/
            margin-top: -40mm;
            position: absolute;
            /*bottom: 0;*/
        }

        .pbb-avoid {
            page-break-inside: avoid;
        }

        a.comment-indicator:hover + div.comment {
            background: #ffd;
            position: absolute;
            display: block;
            border: 0.01em solid black;
            padding: 0.5em
        }

        a.comment-indicator {
            background: red;
            display: inline-block;
            border: 0.01em solid black;
            width: 0.5em;
            height: 0.5em
        }

        div.comment {
            display: none
        }

        table {
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }

        .gridlines td {
            border: 1px dotted black
        }

        .gridlines th {
            border: 1px dotted black
        }

        .b {
            text-align: center
        }

        .e {
            text-align: center
        }

        .f {
            text-align: right
        }

        .inlineStr {
            text-align: left
        }

        .n {
            text-align: right
        }

        .s {
            text-align: left
        }

        td.style0 {
            vertical-align: bottom;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        th.style0 {
            vertical-align: bottom;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        td.style1 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        th.style1 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        td.style2 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;
        }

        th.style2 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;
        }

        td.style3 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        th.style3 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        td.style4 {
            vertical-align: bottom;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;
        }

        th.style4 {
            vertical-align: bottom;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;
        }

        td.style5 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        th.style5 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        td.style6 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;
        }

        th.style6 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style7 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style7 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style8 {
            vertical-align: top;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style8 {
            vertical-align: top;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style9 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style9 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style10 {
            vertical-align: bottom;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style10 {
            vertical-align: bottom;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style11 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style11 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style12 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style12 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style13 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style13 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style14 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style14 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style15 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style15 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style16 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style16 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style17 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style17 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style18 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style18 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style19 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style19 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style20 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style20 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style21 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style21 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style22 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style22 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style23 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style23 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style24 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style24 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style25 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style25 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style26 {
            vertical-align: top;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style26 {
            vertical-align: top;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style27 {
            vertical-align: top;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style27 {
            vertical-align: top;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style28 {
            vertical-align: bottom;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style28 {
            vertical-align: bottom;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style29 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style29 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style30 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style30 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style31 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style31 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style32 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style32 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style33 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style33 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style34 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style34 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style35 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style35 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style36 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style36 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style37 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style37 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style38 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style38 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style39 {
            vertical-align: top;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style39 {
            vertical-align: top;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style40 {
            vertical-align: top;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style40 {
            vertical-align: top;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style41 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style41 {
            vertical-align: middle;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style42 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style42 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style43 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style43 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style44 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style44 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style45 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style45 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style46 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style46 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style47 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style47 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style48 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style48 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style49 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style49 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style50 {
            vertical-align: middle;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        th.style50 {
            vertical-align: middle;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        td.style51 {
            vertical-align: middle;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        th.style51 {
            vertical-align: middle;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        td.style52 {
            vertical-align: middle;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            font-weight: bold;
            color: #000000;

        }

        th.style52 {
            vertical-align: middle;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            font-weight: bold;
            color: #000000;

        }

        td.style53 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style53 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style54 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style54 {
            vertical-align: top;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style55 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style55 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style56 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style56 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style57 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style57 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style58 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style58 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style59 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style59 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style60 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        th.style60 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        td.style61 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        th.style61 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            font-weight: bold;
            color: #000000;

        }

        td.style62 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style62 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style63 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style63 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style64 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style64 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style65 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style65 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 0px;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style66 {
            vertical-align: top;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style66 {
            vertical-align: top;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style67 {
            vertical-align: top;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style67 {
            vertical-align: bottom;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style68 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style68 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style69 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style69 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style70 {
            vertical-align: top;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style70 {
            vertical-align: top;
            text-align: right;
            padding-right: 0px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style71 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style71 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style72 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style72 {
            vertical-align: bottom;
            text-align: left;
            padding-left: 9px;
            border-bottom: none #000000;
            border-top: none #000000;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style73 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style73 {
            vertical-align: bottom;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style74 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style74 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style75 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style75 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style76 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style76 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style77 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style77 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style78 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        th.style78 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: none #000000;
            color: #000000;

        }

        td.style79 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style79 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        td.style80 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        th.style80 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: 0.01em solid #000000 !important;
            border-right: none #000000;
            color: #000000;

        }

        td.style81 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        th.style81 {
            vertical-align: middle;
            text-align: center;
            border-bottom: 0.01em solid #000000 !important;
            border-top: 0.01em solid #000000 !important;
            border-left: none #000000;
            border-right: 0.01em solid #000000 !important;
            color: #000000;

        }

        table.sheet0 col.col0 {
            width: 7.45555547pt
        }

        table.sheet0 col.col1 {
            width: 7.45555547pt
        }

        table.sheet0 col.col2 {
            width: 7.45555547pt
        }

        table.sheet0 col.col3 {
            width: 7.45555547pt
        }

        table.sheet0 col.col4 {
            width: 7.45555547pt
        }

        table.sheet0 col.col5 {
            width: 7.45555547pt
        }

        table.sheet0 col.col6 {
            width: 4.06666662pt
        }

        table.sheet0 col.col7 {
            width: 4.74444439pt
        }

        table.sheet0 col.col8 {
            width: 42pt
        }

        table.sheet0 col.col9 {
            width: 42pt
        }

        table.sheet0 col.col10 {
            width: 42pt
        }

        table.sheet0 col.col11 {
            width: 42pt
        }

        table.sheet0 col.col12 {
            width: 42pt
        }

        table.sheet0 col.col13 {
            width: 42pt
        }

        table.sheet0 col.col14 {
            width: 42pt
        }

        table.sheet0 col.col15 {
            width: 42pt
        }

        table.sheet0 col.col16 {
            width: 42pt
        }

        table.sheet0 col.col17 {
            width: 42pt
        }

        table.sheet0 col.col18 {
            width: 42pt
        }

        table.sheet0 col.col19 {
            width: 42pt
        }

        table.sheet0 col.col20 {
            width: 42pt
        }

        table.sheet0 col.col21 {
            width: 42pt
        }

        table.sheet0 col.col22 {
            width: 42pt
        }

        table.sheet0 col.col23 {
            width: 42pt
        }

        table.sheet0 col.col24 {
            width: 42pt
        }

        table.sheet0 col.col25 {
            width: 42pt
        }

        table.sheet0 col.col26 {
            width: 42pt
        }

        table.sheet0 col.col27 {
            width: 42pt
        }

        table.sheet0 col.col28 {
            width: 42pt
        }

        table.sheet0 col.col29 {
            width: 42pt
        }

        table.sheet0 col.col30 {
            width: 42pt
        }

        table.sheet0 col.col31 {
            width: 42pt
        }

        table.sheet0 col.col32 {
            width: 42pt
        }

        table.sheet0 col.col33 {
            width: 42pt
        }

        table.sheet0 col.col34 {
            width: 42pt
        }

        table.sheet0 col.col35 {
            width: 42pt
        }

        table.sheet0 col.col36 {
            width: 42pt
        }

        table.sheet0 col.col37 {
            width: 42pt
        }

        table.sheet0 col.col38 {
            width: 42pt
        }

        table.sheet0 col.col39 {
            width: 42pt
        }

        table.sheet0 col.col40 {
            width: 42pt
        }

        table.sheet0 col.col41 {
            width: 42pt
        }

        table.sheet0 col.col42 {
            width: 42pt
        }

        table.sheet0 col.col43 {
            width: 42pt
        }

        table.sheet0 col.col44 {
            width: 42pt
        }

        table.sheet0 col.col45 {
            width: 42pt
        }

        table.sheet0 col.col46 {
            width: 42pt
        }

        table.sheet0 col.col47 {
            width: 42pt
        }

        table.sheet0 col.col48 {
            width: 42pt
        }

        table.sheet0 col.col49 {
            width: 42pt
        }

        table.sheet0 col.col50 {
            width: 42pt
        }

        table.sheet0 col.col51 {
            width: 42pt
        }

        table.sheet0 col.col52 {
            width: 42pt
        }

        table.sheet0 col.col53 {
            width: 42pt
        }

        table.sheet0 col.col54 {
            width: 42pt
        }

        table.sheet0 col.col55 {
            width: 42pt
        }

        table.sheet0 col.col56 {
            width: 42pt
        }

        table.sheet0 col.col57 {
            width: 42pt
        }

        table.sheet0 col.col58 {
            width: 42pt
        }

        table.sheet0 col.col59 {
            width: 42pt
        }

        table.sheet0 col.col60 {
            width: 42pt
        }

        table.sheet0 col.col61 {
            width: 42pt
        }

        table.sheet0 col.col62 {
            width: 42pt
        }

        table.sheet0 col.col63 {
            width: 42pt
        }

        table.sheet0 col.col64 {
            width: 42pt
        }

        table.sheet0 col.col65 {
            width: 42pt
        }

        table.sheet0 col.col66 {
            width: 42pt
        }

        table.sheet0 col.col67 {
            width: 42pt
        }

        table.sheet0 col.col68 {
            width: 42pt
        }

        table.sheet0 col.col69 {
            width: 42pt
        }

        table.sheet0 col.col70 {
            width: 42pt
        }

        table.sheet0 col.col71 {
            width: 42pt
        }

        table.sheet0 col.col72 {
            width: 42pt
        }

        table.sheet0 col.col73 {
            width: 42pt
        }

        table.sheet0 col.col74 {
            width: 42pt
        }

        table.sheet0 col.col75 {
            width: 42pt
        }

        table.sheet0 col.col76 {
            width: 42pt
        }

        table.sheet0 col.col77 {
            width: 42pt
        }

        table.sheet0 col.col78 {
            width: 42pt
        }

        table.sheet0 col.col79 {
            width: 42pt
        }

        table.sheet0 col.col80 {
            width: 42pt
        }

        table.sheet0 col.col81 {
            width: 42pt
        }

        table.sheet0 col.col82 {
            width: 42pt
        }

        table.sheet0 col.col83 {
            width: 42pt
        }

        table.sheet0 col.col84 {
            width: 42pt
        }

        table.sheet0 col.col85 {
            width: 42pt
        }

        table.sheet0 col.col86 {
            width: 42pt
        }

        table.sheet0 col.col87 {
            width: 42pt
        }

        table.sheet0 tr {
            height: 11.25pt
        }

        table.sheet0 tr.row0 {
            height: 11.25pt
        }

        table.sheet0 tr.row1 {
            height: 11.25pt
        }

        table.sheet0 tr.row2 {
            height: 11.25pt
        }

        table.sheet0 tr.row3 {
            height: 5.25pt
        }

        table.sheet0 tr.row4 {
            height: 11.25pt
        }

        table.sheet0 tr.row5 {
            height: 11.25pt
        }

        table.sheet0 tr.row6 {
            height: 11.25pt
        }

        table.sheet0 tr.row7 {
            height: 11.25pt
        }

        table.sheet0 tr.row8 {
            height: 11.25pt
        }

        table.sheet0 tr.row9 {
            height: 11.25pt
        }

        table.sheet0 tr.row10 {
            height: 11.25pt
        }

        table.sheet0 tr.row11 {
            height: 11.25pt
        }

        table.sheet0 tr.row12 {
            height: 11.25pt
        }

        table.sheet0 tr.row13 {
            height: 11.25pt
        }

        table.sheet0 tr.row14 {
            height: 11.25pt
        }

        table.sheet0 tr.row15 {
            height: 4.5pt
        }

        table.sheet0 tr.row16 {
            height: 21.75pt
        }

        table.sheet0 tr.row17 {
            height: 46.5pt
        }

        table.sheet0 tr.row18 {
            height: 11.25pt
        }

        table.sheet0 tr.row19 {
            height: 12pt
        }

        table.sheet0 tr.row20 {
            height: 12pt
        }

        table.sheet0 tr.row21 {
            height: 12pt
        }

        table.sheet0 tr.row22 {
            height: 12pt
        }

        table.sheet0 tr.row23 {
            height: 12pt
        }

        table.sheet0 tr.row24 {
            height: 4.5pt
        }

        table.sheet0 tr.row25 {
            height: 22.5pt
        }

        table.sheet0 tr.row26 {
            height: 11.25pt
        }

        table.sheet0 tr.row27 {
            height: 11.25pt
        }

        table.sheet0 tr.row28 {
            height: 11.25pt
        }

        table.sheet0 tr.row29 {
            height: 4.5pt
        }

        table.sheet0 tr.row30 {
            height: 11.25pt
        }

        table.sheet0 tr.row31 {
            height: 11.25pt
        }

        table.sheet0 tr.row32 {
            height: 11.25pt
        }

        table.sheet0 tr.row33 {
            height: 11.25pt
        }

        table.sheet0 tr.row34 {
            height: 11.25pt
        }

        table.sheet0 tr.row35 {
            height: 11.25pt
        }

        table.sheet0 tr.row36 {
            height: 11.25pt
        }

        table.sheet0 tr.row37 {
            height: 11.25pt
        }

        table.sheet0 tr.row38 {
            height: 11.25pt
        }

        table.sheet0 tr.row39 {
            height: 11.25pt
        }

        table.sheet0 tr.row40 {
            height: 11.25pt
        }

        table.sheet0 tr.row41 {
            height: 11.25pt
        }

        table.sheet0 tr.row42 {
            height: 11.25pt
        }

        table.sheet0 tr.row43 {
            height: 11.25pt
        }

        table.sheet0 tr.row44 {
            height: 11.25pt
        }

        table.sheet0 tr.row45 {
            height: 11.25pt
        }

        table.sheet0 tr.row46 {
            height: 11.25pt
        }

        table.sheet0 tr.row47 {
            height: 11.25pt
        }
    </style>
</head>
<body>
<style>
    @page {
        margin: 8pt;
    }
</style>
<table border="0" cellpadding="0" cellspacing="0" id="sheet0" class="sheet0">
    <colgroup>
        <col class="col0">
        <col class="col1">
        <col class="col2">
        <col class="col3">
        <col class="col4">
        <col class="col5">
        <col class="col6">
        <col class="col7">
        <col class="col8">
        <col class="col9">
        <col class="col10">
        <col class="col11">
        <col class="col12">
        <col class="col13">
        <col class="col14">
        <col class="col15">
        <col class="col16">
        <col class="col17">
        <col class="col18">
        <col class="col19">
        <col class="col20">
        <col class="col21">
        <col class="col22">
        <col class="col23">
        <col class="col24">
        <col class="col25">
        <col class="col26">
        <col class="col27">
        <col class="col28">
        <col class="col29">
        <col class="col30">
        <col class="col31">
        <col class="col32">
        <col class="col33">
        <col class="col34">
        <col class="col35">
        <col class="col36">
        <col class="col37">
        <col class="col38">
        <col class="col39">
        <col class="col40">
        <col class="col41">
        <col class="col42">
        <col class="col43">
        <col class="col44">
        <col class="col45">
        <col class="col46">
        <col class="col47">
        <col class="col48">
        <col class="col49">
        <col class="col50">
        <col class="col51">
        <col class="col52">
        <col class="col53">
        <col class="col54">
        <col class="col55">
        <col class="col56">
        <col class="col57">
        <col class="col58">
        <col class="col59">
        <col class="col60">
        <col class="col61">
        <col class="col62">
        <col class="col63">
        <col class="col64">
        <col class="col65">
        <col class="col66">
        <col class="col67">
        <col class="col68">
        <col class="col69">
        <col class="col70">
        <col class="col71">
        <col class="col72">
        <col class="col73">
        <col class="col74">
        <col class="col75">
        <col class="col76">
        <col class="col77">
        <col class="col78">
        <col class="col79">
        <col class="col80">
        <col class="col81">
        <col class="col82">
        <col class="col83">
        <col class="col84">
        <col class="col85">
        <col class="col86">
        <col class="col87">
    </colgroup>
    <tbody>
    <tr class="row0">
        <td class="column0 style66 s style66" colspan="88">Приложение N 1 к письму ФНС России от 21.10.2013 N
                                                           ММВ-20-3/96@
        </td>
    </tr>
    <tr class="row1">
        <td class="column0 style67 s style67" colspan="8" rowspan="4">Универсальный передаточный документ</td>
        <td class="column8 style71 s style72" colspan="9">Счет-фактура N</td>
        <td class="column17 style21 s style21" colspan="10">{{$order['id']}}</td>
        <td class="column27 style69 s style69" colspan="2">от</td>
        <td class="column29 style21 s style21" colspan="10">
            {{$shippingDate['day']}}.{{$shippingDate['month']}}.{{$shippingDate['year']}}
        </td>
        <td class="column39 style49 s style49" colspan="4">(1)</td>
        <td class="column43 style70 s style66" colspan="45" rowspan="3">Приложение N 1 к постановлению Правительства
                                                                        Российской Федерации от 26 декабря 2011 года №
                                                                        1137<br/>
                                                                        &nbsp;(в редакции постановления Правительства
                                                                        Российской Федерации от 19.08.2017 № 981)
        </td>
    </tr>
    <tr class="row2">
        <td class="column8 style71 s style72" colspan="9">Исправление N</td>
        <td class="column17 style68 null style68" colspan="10"></td>
        <td class="column27 style69 s style69" colspan="2">от</td>
        <td class="column29 style68 null style68" colspan="10"></td>
        <td class="column39 style49 s style49" colspan="4">(1а)</td>
    </tr>
    <tr class="row3">
        <td class="column8 style57 null style49" colspan="35"></td>
    </tr>
    <tr class="row4">
        <td class="column8 style60 s style61" colspan="19">Продавец</td>
        <td class="column27 style29 s style29" colspan="58">{{$seller['name']}}</td>
        <td class="column85 style32 s style32" colspan="3">(2)</td>
    </tr>
    <tr class="row5">
        <td class="column0 style24 s style24" colspan="4">Статус:</td>
        <td class="column4 style62 s style63" colspan="2">2</td>
        <td class="column6 style57 null style34" colspan="2"></td>
        <td class="column8 style58 s style59" colspan="19">Адрес</td>
        <td class="column27 style64 s style64" colspan="58">{{$seller['address']}}</td>
        <td class="column85 style32 s style32" colspan="3">(2а)</td>
    </tr>
    <tr class="row6">
        <td class="column0 style26 s style27" colspan="8" rowspan="9"><br/>
            1 - счет-фактура и передаточный документ (акт) <br/>
            2 - передаточный документ (акт)
        </td>
        <td class="column8 style58 s style59" colspan="19">ИНН/КПП продавца</td>
        <td class="column27 style64 s style64" colspan="58">{{$seller['inn']}} / {{$seller['kpp']}}</td>
        <td class="column85 style32 s style32" colspan="3">(2б)</td>
    </tr>
    <tr class="row7">
        <td class="column8 style58 s style59" colspan="19">Грузоотправитель и его адрес</td>
        <td class="column27 style64 null style64" colspan="58"></td>
        <td class="column85 style32 s style32" colspan="3">(3)</td>
    </tr>
    <tr class="row8">
        <td class="column8 style58 s style59" colspan="19">Грузополучатель и его адрес</td>
        <td class="column27 style64 null style64" colspan="58"></td>
        <td class="column85 style32 s style32" colspan="3">(4)</td>
    </tr>
    <tr class="row9">
        <td class="column8 style58 s style59" colspan="19">К платежно-расчетному документу</td>
        <td class="column27 style25 s style25" colspan="2">N</td>
        <td class="column29 style21 null style21" colspan="8"></td>
        <td class="column37 style22 s style22" colspan="2">от</td>
        <td class="column39 style64 null style64" colspan="46"></td>
        <td class="column85 style32 s style32" colspan="3">(5)</td>
    </tr>
    <tr class="row10">
        <td class="column8 style60 s style61" colspan="19">Покупатель</td>
        <td class="column27 style29 s style29" colspan="58">{{$client['name']}}</td>
        <td class="column85 style22 s style22" colspan="3">(6)</td>
    </tr>
    <tr class="row11">
        <td class="column8 style58 s style59" colspan="19">Адрес</td>
        <td class="column27 style65 s style64" colspan="58">{{$client['address']}}</td>
        <td class="column85 style22 s style22" colspan="3">(6а)</td>
    </tr>
    <tr class="row12">
        <td class="column8 style58 s style59" colspan="19">ИНН/КПП покупателя</td>
        <td class="column27 style64 s style64" colspan="58">{{$client['inn']}} / {{$client['kpp']}}</td>
        <td class="column85 style22 s style22" colspan="3">(6б)</td>
    </tr>
    <tr class="row13">
        <td class="column8 style58 s style59" colspan="19">Валюта: наименование, код</td>
        <td class="column27 style68 s style68" colspan="58">Российский рубль, 643</td>
        <td class="column85 style22 s style22" colspan="3">(7)</td>
    </tr>
    <tr class="row14">
        <td class="column9 style59 s" colspan="19">Идентификатор государственного контракта, договора
                                                   (соглашения)
        </td>
        <td class="column40 style73 null style73" colspan="58"></td>
        <td class="column85 style22 s style22" colspan="3">(8)</td>
    </tr>
    <tr class="row15">
        <td class="column0 style24 null style34" colspan="8"></td>
        <td class="column8 style57 null style49" colspan="80"></td>
    </tr>
    <tr class="row16">
        <td class="column0 style14 s style19" colspan="2" rowspan="2">N п/п</td>
        <td class="column2 style14 s style18" colspan="6" rowspan="2">Код товара/ работ, услуг</td>
        <td class="column8 style56 s style20" colspan="14" rowspan="2">Наименование товара (описание выполненных работ,
                                                                       оказанных услуг), имущественного права
        </td>
        <td class="column22 style14 s style19" colspan="3" rowspan="2">Код вида товара</td>
        <td class="column25 style20 s style20" colspan="9">Единица измерения</td>
        <td class="column34 style14 s style19" colspan="4" rowspan="2">Коли-<br/>
                                                                       чество (объем)
        </td>
        <td class="column38 style14 s style19" colspan="5" rowspan="2">Цена (тариф) за единицу измерения</td>
        <td class="column43 style14 s style19" colspan="7" rowspan="2">Стоимость товаров <br/>
                                                                       (работ, услуг), имущественных прав без налога -
                                                                       всего
        </td>
        <td class="column50 style14 s style19" colspan="4" rowspan="2">В том числе сумма акциза</td>
        <td class="column54 style14 s style19" colspan="4" rowspan="2">Нало-<br/>
                                                                       говая ставка
        </td>
        <td class="column58 style14 s style19" colspan="6" rowspan="2">Сумма налога, предъявляемая покупателю</td>
        <td class="column64 style14 s style19" colspan="7" rowspan="2">Стоимость товаров <br/>
                                                                       (работ, услуг), имущественных прав с налогом -
                                                                       всего
        </td>
        <td class="column71 style11 s style13" colspan="11">Страна происхождения товара</td>
        <td class="column82 style14 s style19" colspan="6" rowspan="2">Регистра-ционный номер таможенной декларации</td>
    </tr>
    <tr class="row17">
        <td class="column25 style20 s style20" colspan="2">код</td>
        <td class="column27 style20 s style20" colspan="7">условное обозначение (национальное)</td>
        <td class="column71 style20 s style20" colspan="4">Цифро-<br/>
                                                           вой код
        </td>
        <td class="column75 style20 s style20" colspan="7">Краткое наименование</td>
    </tr>
    <tr class="row18">
        <td class="column0 style11 s style13" colspan="2">А</td>
        <td class="column2 style11 s style12" colspan="6">Б</td>
        <td class="column8 style56 s style20" colspan="14">1</td>
        <td class="column22 style11 s style13" colspan="3">1а</td>
        <td class="column25 style20 s style20" colspan="2">2</td>
        <td class="column27 style20 s style20" colspan="7">2а</td>
        <td class="column34 style20 s style20" colspan="4">3</td>
        <td class="column38 style20 s style20" colspan="5">4</td>
        <td class="column43 style20 s style20" colspan="7">5</td>
        <td class="column50 style20 s style20" colspan="4">6</td>
        <td class="column54 style20 s style20" colspan="4">7</td>
        <td class="column58 style20 s style20" colspan="6">8</td>
        <td class="column64 style20 s style20" colspan="7">9</td>
        <td class="column71 style20 s style20" colspan="4">10</td>
        <td class="column75 style20 s style20" colspan="7">10а</td>
        <td class="column82 style20 s style20" colspan="6">11</td>
    </tr>
    @foreach($orderItems as $item)
        <tr class="row19">
            <td class="column0 style11 s style13" colspan="2">{{$loop->index+1}}</td>
            <td class="column2 style11 s style12" colspan="6">-</td>
            <td class="column8 style56 s style20" colspan="14">
                {{$item->product->name}} {{$item->need_layout?'+ макет':''}}
            </td>
            <td class="column22 style11 s style13" colspan="3">-</td>
            <td class="column25 style42 s style42" colspan="2">{{$item['product']['count_code']??'-'}}</td>
            <td class="column27 style42 s style42" colspan="7">{{$item['product']['count_symbol']??'-'}}</td>
            <td class="column34 style55 n style55" colspan="4">{{$item['count']}}</td>
            <td class="column38 style46 n style46" colspan="5">
                {{number_format($item['totalWithSale']/$item['count'], 2, '.', ' ')}}
            </td>
            <td class="column43 style46 f style46" colspan="7">
                {{number_format($item['totalWithSale'], 2, '.', ' ')}}
            </td>
            <td class="column50 style46 s style46" colspan="4">без акциза</td>
            <td class="column54 style46 s style46" colspan="4">Без НДС</td>
            <td class="column58 style46 s style46" colspan="6">Без НДС</td>
            <td class="column64 style46 f style46" colspan="7">
                {{number_format($item['totalWithSale'], 2, '.', ' ')}}
            </td>
            <td class="column71 style42 null style42" colspan="4"></td>
            <td class="column75 style42 null style42" colspan="7"></td>
            <td class="column82 style42 null style42" colspan="6"></td>
        </tr>
    @endforeach
    @if($order->delivery_coordinates)
        <tr class="row19">
            <td class="column0 style11 s style13" colspan="2">{{$order->orderProducts->count()+1}}</td>
            <td class="column2 style11 s style12" colspan="6">-</td>
            <td class="column8 style56 s style20" colspan="14">
                Доставка
            </td>
            <td class="column22 style11 s style13" colspan="3">-</td>
            <td class="column25 style42 s style42" colspan="2">-</td>
            <td class="column27 style42 s style42" colspan="7">Услуга</td>
            <td class="column34 style55 n style55" colspan="4">1</td>
            <td class="column38 style46 n style46" colspan="5">
                {{number_format($order->delivery_price, 2, '.', ' ')}}
            </td>
            <td class="column43 style46 f style46" colspan="7">
                {{number_format($order->delivery_price, 2,',',' ')}}
            </td>
            <td class="column50 style46 s style46" colspan="4">без акциза</td>
            <td class="column54 style46 s style46" colspan="4">Без НДС</td>
            <td class="column58 style46 s style46" colspan="6">Без НДС</td>
            <td class="column64 style46 f style46" colspan="7">
                {{number_format($order->delivery_price, 2,',',' ')}}
            </td>
            <td class="column71 style42 null style42" colspan="4"></td>
            <td class="column75 style42 null style42" colspan="7"></td>
            <td class="column82 style42 null style42" colspan="6"></td>
        </tr>
    @endif
    <tr class="row21">
        <td class="column0 style11 null style13" colspan="8"></td>
        <td class="column8 style2 null"></td>
        <td class="column8 s style52" colspan="34">Всего к оплате</td>
        <td class="column43 style46 s style46" colspan="7">{{number_format($order['total'], 2, '.', ' ')}}</td>
        <td class="column50 style43 s style45" colspan="8">Х</td>
        <td class="column58 style46 f style46" colspan="6">0.00</td>
        <td class="column64 style46 s style46" colspan="7">{{number_format($order['total'], 2, '.', ' ')}}</td>
        <td class="column71 style41 null style41" colspan="17"></td>
    </tr>
    <tr class="row22">
        <td class="column0 style47 null style47" colspan="8"></td>
        <td class="column8 style48 null style49" colspan="80"></td>
    </tr>
    <tr class="row23">
        <td class="column0 style53 s style53" colspan="8">
            Документ составлен на
        </td>
        <td class="column8 style2 null"></td>
        <td class="column9 style36 s style36" colspan="19">
            Руководитель организации <br/>
            или иное уполномоченное лицо
        </td>
        <td class="column28 style35 null style35" colspan="6">
            @if($client['isCompany'])
                <div class="img-wrap">
                    @if($signed)
                        <img class="sign" src="{{$seller['sign']}}">
                    @endif
                </div>
            @endif
        </td>
        <td class="column34 style5 null"></td>
        <td class="column35 style35 s style35" colspan="15">
            @if($client['isCompany'])
                {{$seller['director']}}
            @endif
        </td>
        <td class="column50 style36 s style36" colspan="16">
            Главный бухгалтер <br/>
            или иное уполномоченное лицо
        </td>
        <td class="column66 style35 null style35" colspan="6">
            @if($client['isCompany'])
                <div class="img-wrap">
                    @if($signed)
                        <img class="sign" src="{{$seller['sign']}}">
                    @endif
                </div>
            @endif
        </td>
        <td class="column72 style5 null"></td>
        <td class="column73 style35 s style35" colspan="15">
            @if($client['isCompany'])
                {{$seller['director']}}
            @endif
        </td>
    </tr>
    <tr class="row24">
        <td class="column0 style21 s style21" colspan="3"></td>
        <td class="column3 style24 s style24" colspan="5">&nbsp;листах</td>
        <td class="column8 style2 null"></td>
        <td class="column9 style36 null style36" colspan="19"></td>
        <td class="column28 style54 s style54" colspan="6">(подпись)</td>
        <td class="column34 style6 null"></td>
        <td class="column35 style54 s style54" colspan="15">(ф.и.о.)</td>
        <td class="column50 style36 null style36" colspan="16"></td>
        <td class="column66 style54 s style54" colspan="6">(подпись)</td>
        <td class="column72 style6 null"></td>
        <td class="column73 style54 s style54" colspan="15">(ф.и.о.)</td>
    </tr>
    <tr class="row25">
        <td class="column0 style24 null style34" colspan="8"></td>
        <td class="column8 style2 null"></td>
        <td class="column9 style36 s style36" colspan="19">Индивидуальный предприниматель</td>
        <td class="column28 style35 null style35" colspan="6">
            @if(!$client['isCompany'])
                <div class="img-wrap">
                    @if($signed)
                        <img class="sign" src="{{$seller['sign']}}">
                    @endif
                </div>
            @endif
        </td>
        <td class="column34 style5 null"></td>
        <td class="column35 style35 s style35" colspan="15">
            @if(!$client['isCompany'])
                {{$seller['director']}}
            @endif
        </td>
        <td class="column50 style36 null style36" colspan="3"></td>
        <td class="column53 style35 null style35" colspan="35">
            @if(!$client['isCompany'])
                318861700059622
            @endif
        </td>
    </tr>
    <tr class="row26">
        <td class="column0 style24 null style34" colspan="8"></td>
        <td class="column8 style7 null"></td>
        <td class="column9 style38 s style38" colspan="19">или иное уполномоченное лицо</td>
        <td class="column28 style40 s style40" colspan="6">(подпись)</td>
        <td class="column34 style8 null"></td>
        <td class="column35 style40 s style40" colspan="15">(ф.и.о.)</td>
        <td class="column50 style38 null style38" colspan="3"></td>
        <td class="column53 style39 s style39" colspan="35">(реквизиты свидетельства о государственной регистрации
                                                            индивидуального предпринимателя)
        </td>
    </tr>
    <tr class="row27">
        <td class="column0 style24 null style24" colspan="88"></td>
    </tr>
    <tr class="row28">
        <td class="column0 style24 s style24" colspan="25">Основание передачи (сдачи) / получения (приемки)</td>
        <td class="column25 style35 s style35" colspan="60">Договор {{$contract['fullName']}}</td>
        <td class="column85 style33 s style33" colspan="3">[8]</td>
    </tr>
    <tr class="row29">
        <td class="column0 style24 null style24" colspan="22"></td>
        <td class="column22">&nbsp;</td>
        <td class="column23">&nbsp;</td>
        <td class="column24">&nbsp;</td>
        <td class="column25 style37 s style37" colspan="60">(договор; доверенность и др.)</td>
        <td class="column85 style33 null style33" colspan="3"></td>
    </tr>
    <tr class="row30">
        <td class="column0 style24 s style24" colspan="16">Данные о транспортировке и грузе</td>
        <td class="column16 style35 null style35" colspan="69"></td>
        <td class="column85 style33 s style33" colspan="3">[9]</td>
    </tr>
    <tr class="row31">
        <td class="column0 style24 null style24" colspan="16"></td>
        <td class="column16 style37 s style37" colspan="69">(транспортная накладная, поручение экспедитору,
                                                            экспедиторская / складская расписка и др. / масса нетто/
                                                            брутто груза, если не приведены ссылки на
                                                            транспортные документы, содержащие эти сведения)
        </td>
        <td class="column85 style33 null style33" colspan="3"></td>
    </tr>
    <tr class="row32">
        <td class="column0 style24 s style34" colspan="45">Товар (груз) передал / услуги, результаты работ, права сдал
        </td>
        <td class="column45 style2 null"></td>
        <td class="column46 style36 s style36" colspan="42">Товар (груз) получил / услуги, результаты работ, права
                                                            принял
        </td>
    </tr>
    <tr class="row33">
        <td class="column0 style21 s style21" colspan="13">
            {{$seller['position']}}
        </td>
        <td class="column13 style3 null"></td>
        <td class="column14 style21 null style21" colspan="13">
            <div class="img-wrap">
                @if($signed)
                    <img class="sign" src="{{$seller['sign']}}">
                @endif
            </div>
        </td>
        <td class="column27 style3 null"></td>
        <td class="column28 style21 s style21" colspan="14">{{$seller['director']}}</td>
        <td class="column42 style22 s style23" colspan="3">[10]</td>
        <td class="column45 style2 null"></td>
        <td class="column46 style21 null style21" colspan="13"></td>
        <td class="column59 style3 null"></td>
        <td class="column60 style21 null style21" colspan="10"></td>
        <td class="column70 style3 null"></td>
        <td class="column71 style21 null style21" colspan="14"></td>
        <td class="column85 style33 s style33" colspan="3">[15]</td>
    </tr>
    <tr class="row34">
        <td class="column0 style31 s style31" colspan="13">(должность)</td>
        <td class="column13 style9 null"></td>
        <td class="column14 style31 s style31" colspan="13">(подпись)</td>
        <td class="column27 style9 null"></td>
        <td class="column28 style31 s style31" colspan="14">(ф.и.о.)</td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style31 s style31" colspan="13">(должность)</td>
        <td class="column59 style9 null"></td>
        <td class="column60 style31 s style31" colspan="10">(подпись)</td>
        <td class="column70 style9 null"></td>
        <td class="column71 style31 s style31" colspan="14">(ф.и.о.)</td>
        <td class="column85 style33 null style33" colspan="3"></td>
    </tr>
    <tr class="row35">
        <td class="column0 style24 s style24" colspan="15">Дата отгрузки, передачи (сдачи)</td>
        <td class="column15 style10 s">&quot;</td>
        <td class="column16 style21 s style21" colspan="2">{{$shippingDate['day']}}</td>
        <td class="column18 style1 s">&quot;</td>
        <td class="column19 style21 s style21" colspan="9">{{$shippingDate['month']}}</td>
        <td class="column18 style1 s">&quot;</td>
        <td class="column28 style22 s style21" colspan="3">{{$shippingDate['year']}}</td>
        <td class="column32 style24 s style24" colspan="10">г.</td>
        <td class="column42 style22 s style23" colspan="3">[11]</td>
        <td class="column45 style2 null"></td>
        <td class="column46 style24 s style24" colspan="13">Дата получения (приемки)</td>
        <td class="column59 style10 s">&quot;</td>
        <td class="column60 style21 null style21" colspan="2"></td>
        <td class="column62 style1 s">&quot;</td>
        <td class="column63 style21 null style21" colspan="6"></td>
        <td class="column69 style28 s style28" colspan="2">20</td>
        <td class="column71 style29 null style29" colspan="2"></td>
        <td class="column73 style24 s style24" colspan="12">г.</td>
        <td class="column85 style33 s style33" colspan="3">[16]</td>
    </tr>
    </tbody>
</table>
<table class="pbb-avoid sheet0">
    <colgroup>
        <col class="col0">
        <col class="col1">
        <col class="col2">
        <col class="col3">
        <col class="col4">
        <col class="col5">
        <col class="col6">
        <col class="col7">
        <col class="col8">
        <col class="col9">
        <col class="col10">
        <col class="col11">
        <col class="col12">
        <col class="col13">
        <col class="col14">
        <col class="col15">
        <col class="col16">
        <col class="col17">
        <col class="col18">
        <col class="col19">
        <col class="col20">
        <col class="col21">
        <col class="col22">
        <col class="col23">
        <col class="col24">
        <col class="col25">
        <col class="col26">
        <col class="col27">
        <col class="col28">
        <col class="col29">
        <col class="col30">
        <col class="col31">
        <col class="col32">
        <col class="col33">
        <col class="col34">
        <col class="col35">
        <col class="col36">
        <col class="col37">
        <col class="col38">
        <col class="col39">
        <col class="col40">
        <col class="col41">
        <col class="col42">
        <col class="col43">
        <col class="col44">
        <col class="col45">
        <col class="col46">
        <col class="col47">
        <col class="col48">
        <col class="col49">
        <col class="col50">
        <col class="col51">
        <col class="col52">
        <col class="col53">
        <col class="col54">
        <col class="col55">
        <col class="col56">
        <col class="col57">
        <col class="col58">
        <col class="col59">
        <col class="col60">
        <col class="col61">
        <col class="col62">
        <col class="col63">
        <col class="col64">
        <col class="col65">
        <col class="col66">
        <col class="col67">
        <col class="col68">
        <col class="col69">
        <col class="col70">
        <col class="col71">
        <col class="col72">
        <col class="col73">
        <col class="col74">
        <col class="col75">
        <col class="col76">
        <col class="col77">
        <col class="col78">
        <col class="col79">
        <col class="col80">
        <col class="col81">
        <col class="col82">
        <col class="col83">
        <col class="col84">
        <col class="col85">
        <col class="col86">
        <col class="col87">
    </colgroup>
    <tbody>
    <tr class="row36">
        <td class="column0 style24 s style24" colspan="42">Иные сведения об отгрузке, передаче</td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style24 s style24" colspan="39">Иные сведения о получении, приемке</td>
        <td class="column85 style33 null style33" colspan="3"></td>
    </tr>
    <tr class="row37">
        <td class="column0 style21 null style21" colspan="42"></td>
        <td class="column42 style22 s style23" colspan="3">[12]</td>
        <td class="column45 style2 null"></td>
        <td class="column46 style21 null style21" colspan="39"></td>
        <td class="column85 style33 s style33" colspan="3">[17]</td>
    </tr>
    <tr class="row38">
        <td class="column0 style30 s style30" colspan="42">(ссылки на неотъемлемые приложения, сопутствующие документы,
                                                           иные документы и т.п.)
        </td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style30 s style30" colspan="39">(информация о наличии/отсутствии претензии; ссылки на
                                                            неотъемлемые приложения, и другие документы и т.п.)
        </td>
        <td class="column85 style33 null style33" colspan="3"></td>
    </tr>
    <tr class="row39">
        <td class="column0 style24 s style24" colspan="42">Ответственный за правильность оформления факта хозяйственной
                                                           жизни
        </td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style24 s style24" colspan="39">Ответственный за правильность оформления факта хозяйственной
                                                            жизни
        </td>
        <td class="column85 style33 null style33" colspan="3"></td>
    </tr>
    <tr class="row40">
        <td class="column0 style21 s style21" colspan="13">
            {{$seller['position']}}
        </td>
        <td class="column13 style3 null"></td>
        <td class="column14 style21 null style21" colspan="13">
            <div class="img-wrap">
                @if($signed)
                    <img class="sign" src="{{$seller['sign']}}">
                @endif
            </div>
        </td>
        <td class="column27 style3 null"></td>
        <td class="column28 style21 s style21" colspan="14">{{$seller['director']}}</td>
        <td class="column42 style22 s style23" colspan="3">[13]</td>
        <td class="column45 style2 null"></td>
        <td class="column46 style21 null style21" colspan="13"></td>
        <td class="column59 style3 null"></td>
        <td class="column60 style21 null style21" colspan="10"></td>
        <td class="column70 style3 null"></td>
        <td class="column71 style21 null style21" colspan="14"></td>
        <td class="column85 style33 s style33" colspan="3">[18]</td>
    </tr>
    <tr class="row41 pbb-avoid">
        <td class="column0 style31 s style31" colspan="13">(должность)</td>
        <td class="column13 style9 null"></td>
        <td class="column14 style31 s style31" colspan="13">(подпись)</td>
        <td class="column27 style9 null"></td>
        <td class="column28 style31 s style31" colspan="14">(ф.и.о.)</td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style31 s style31" colspan="13">(должность)</td>
        <td class="column59 style9 null"></td>
        <td class="column60 style31 s style31" colspan="10">(подпись)</td>
        <td class="column70 style9 null"></td>
        <td class="column71 style31 s style31" colspan="14">(ф.и.о.)</td>
        <td class="column85 style32 null style32" colspan="3"></td>
    </tr>
    <tr class="row42 pbb-avoid">
        <td class="column0 style24 s style24" colspan="42">Наименование экономического субъекта – составителя документа
                                                           (в т.ч. комиссионера / агента)
        </td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style24 s style24" colspan="39">Наименование экономического субъекта - составителя
                                                            документа
        </td>
        <td class="column85 style32 null style32" colspan="3"></td>
    </tr>
    <tr class="row43 pbb-avoid">
        <td class="column0 style29 null style29" colspan="42"></td>
        <td class="column42 style22 s style23" colspan="3">[14]</td>
        <td class="column45 style2 null"></td>
        <td class="column46 style29 null style29" colspan="39"></td>
        <td class="column85 style32 s style32" colspan="3">[19]</td>
    </tr>
    <tr class="row44 pbb-avoid">
        <td class="column0 style30 s style30" colspan="42">(может не заполняться при проставлении печати в М.П., может
                                                           быть указан ИНН / КПП)
        </td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style30 s style30" colspan="39">(может не заполняться при проставлении печати в М.П., может
                                                            быть указан ИНН / КПП)
        </td>
        <td class="column85 style32 null style32" colspan="3"></td>
    </tr>
    <tr class="row45 pbb-avoid">
        <td class="column0 style22 s style22" colspan="13">
            <div class="img-wrap">
                @if($signed)
                    <img class="stamp" src="{{$seller['stamp']}}">
                @endif
            </div>
            М.П.
        </td>
        <td class="column13 style24 null style24" colspan="29"></td>
        <td class="column42 style22 null style23" colspan="3"></td>
        <td class="column45 style2 null"></td>
        <td class="column46 style22 s style22" colspan="13">М.П.</td>
        <td class="column59 style24 null style24" colspan="26"></td>
        <td class="column85 style32 null style32" colspan="3"></td>
    </tr>
    </tbody>
</table>
</body>
</html>
