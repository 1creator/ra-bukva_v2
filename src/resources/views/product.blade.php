@extends('layouts.app')
@push('head')
    @if($product->meta_title)
        <title>{{$product->meta_title}}</title>
    @else
        <title>{{$product->name}} - Заказать в Сургуте быстро, качественно, недорого</title>
    @endif
    <meta name="description" content="{{$product->meta_description}}">
    <link rel="canonical" href="{{route('products.show', ['product'=>$product->seo_name])}}"/>
@endpush
@section('content')
    <div class="p-product">
        <div class="container">
            @include('components.breadcrumbs',['items' => $product->breadCrumbs()])
            <div itemscope itemtype="http://schema.org/Product">
                <base-calculator class="mb-5"
                                 :product="{{ $calculator_data }}"
                                 :rating="{{ $average_rating }}"
                                 :reviews-count="{{ count($reviews) }}"
                >
                    <div itemprop="name" class="d-none">
                        {{$product->name}}
                    </div>
                    <h1 class="text-uppercase mb-4">{{$product->h1? $product->h1:$product->name}}</h1>
                    <div class="row mb-5 mt-0">
                        <div class="col-12 col-md-4 pr-5">
                            <div class="mb-3">
                                @if($product->image)
                                    <a class="chocolat-image"
                                       itemprop="image"
                                       title="{{ $product->name }}"
                                       href="{{ $product->image->url }}">
                                        <img class="product-image"
                                             title="{{ $product->name }}"
                                             alt="{{ $product->name }}"
                                             src="{{ $product->image->thumbnail }}">
                                    </a>
                                @else
                                    <div class="product-image">
                                        <i class="icofont-image"></i>
                                    </div>
                                @endif
                                @if($average_rating)
                                    <div itemprop="aggregateRating"
                                         itemscope
                                         itemtype="http://schema.org/AggregateRating">
                                        <div class="rating rating_inline">
                                            @for($i=0; $i<5; $i++)
                                                <i class="icofont-star {{ $i <= $average_rating ? 'active' : '' }}"></i>
                                            @endfor
                                            <div itemprop="ratingValue">{{ $average_rating }}</div>
                                            <meta itemprop="bestRating" content="5"/>
                                            <meta itemprop="worstRating" content="1"/>
                                        </div>
                                        <a class="small ml-3" href="#reviews">
                                            Читать отзывы
                                            (<span itemprop="reviewCount">{{ count($reviews) }}</span>)</a>
                                    </div>
                                @endif
                            </div>
                            <div class="position-relative" itemtype="http://schema.org/AggregateOffer" itemscope=""
                                 itemprop="offers">
                                <link itemprop="availability" href="http://schema.org/InStock"/>
                                <meta itemprop="priceCurrency" content="RUB"/>
                                <meta itemprop="lowPrice" content="{{$product->low_price}}"/>
                                <div>
                                    <span class="font-weight-bold">Время изготовления:</span>
                                    {{$product->production_time}}
                                </div>
                                <div class="mb-3">
                                    <span class="font-weight-bold">Цена:</span>
                                    от {{$product->base_price}} ₽ за {{$product->count_type}}
                                </div>
                                <div>
                                    <button class="btn btn-primary" disabled>
                                        <i class="icofont icofont-cart-alt"></i>
                                        Купить
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-8">
                            @foreach($product->parameters as $index=>$parameter)
                                <div class="mb-4">
                                    <div class="h5 mb-3">
                                        <span class="h1 mr-2">{{$index+1}}.</span>
                                        Выберите <span class="text-lowercase">{{$parameter->name}}</span>
                                    </div>
                                    <div class="btn-group btn-group_sm btn-group_light" role="group">
                                        @foreach($parameter->values as $value)
                                            <button type="button" class="btn">{{$value->name}}</button>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </base-calculator>
                @if($product->recommendations->count() > 0)
                    <div class="mb-5">
                        @include('components.slider-recommendations', ['products'=>$product->recommendations])
                    </div>
                @endif
                <div class="text-justify product-description mb-5" itemprop="description">
                    <div class="mb-5">{!!$product->description!!}</div>
                    <div class="d-flex justify-content-end">
                        <div class="mr-3">Поделись с друзьями:</div>
                        <div class="list-unstyled ya-share2"
                             data-description="{{$product->meta_description}}"
                             data-image="{{asset($product->image->thumbnail ?? null)}}"
                             data-services="vkontakte,facebook,odnoklassniki,twitter"></div>
                    </div>
                </div>
                @if(count($reviews))
                    <a id="reviews"></a>
                    @include('components.reviews-slider', ['class' => 'mb-5'])
                @endif
                @if($product->projects->count() > 0)
                    <div class="mb-5">
                        <div class="h3 text-uppercase mb-4">Выполненные проекты</div>
                        <div class="row">
                            @foreach($product->getProjectsSet() as $project)
                                @include('components.card-project', ['project'=>$project])
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @include('components.slider-articles', ['articles'=>$product->articles])
@endsection
