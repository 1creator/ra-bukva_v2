@if(\App\Models\Sale::active()->count() > 0)
    <div class="swiper-container">
        <div class="swiper-wrapper">
            @foreach(\App\Models\Sale::active()->get() as $sale)
                @if($sale->mediaFile->type == 'embed')
                    <div class="swiper-slide">
                        <div class="sale">
                            <div class="video-background">
                                <div class="video-foreground">
                                    {!! $sale->mediaFile->url !!}
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="h1">{{$sale->name}}</div>
                                        <div>{!!$sale->description!!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div data-background="{{$sale->mediaFile->url}}"
                         class="swiper-slide swiper-lazy">
                        <div class="sale">
                            <div class="swiper-lazy-preloader"></div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="h1">{{$sale->name}}</div>
                                        <div>{!!$sale->description!!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        {{--<div class="swiper-pagination"></div>--}}
        <div class="swiper-custom-arrow swiper-button-prev icofont-swoosh-left d-none d-md-block"></div>
        <div class="swiper-custom-arrow swiper-button-next icofont-swoosh-right d-none d-md-block"></div>
    </div>
@endif
