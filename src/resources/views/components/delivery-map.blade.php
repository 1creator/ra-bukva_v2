<div id="map" style="width: 100%;height: 400px"></div>
@push('scripts')
    <script>
        function initMap() {
            this.d_map = new ymaps.Map("map", {
                center: [61.264070, 73.393212],
                zoom: 12,
                controls: ['geolocationControl', 'searchControl']
            });
            var zones = ymaps.geoQuery({!! json_encode($deliveryZones) !!}).addToMap(this.d_map);

            zones.each(zone => {
                zone.options.set({
                    fillColor: zone.properties.get('fillColor') + '44',
                    strokeColor: '#00000022',
                    openBalloonOnClick: false,
                    interactivityModel: 'default#transparent',
                    hintContent: zone.properties.get('name') + ': ' + zone.properties.get('price') + 'Р'
                });

            });
        }

        ymaps.ready(this.initMap);
    </script>
@endpush