@if($project->mediaFile->type == 'embed')
    <div class="col-12 col-md-6 mb-4">
        <div class="project-preview">
            {!! $project->mediaFile->url !!}
            <div class="d-none">{{$project->description}}</div>
        </div>
    </div>
@else
    <div class="col-6 col-md-3 mb-4">
        <div class="card-project-wrapper">
            <div class="project-preview">
                <a class="chocolat-image"
                   href="{{ $project->mediaFile->url }}"
                   title="{{ $project->description }}">
                    <img class="project-preview__image"
                         alt="{{ $project->description }}"
                         src="{{ $project->mediaFile->thumbnail }}">
                </a>
            </div>
        </div>
    </div>
@endif
