<review-card :review="{{ json_encode($review) }}">
    <div class="review-card" itemprop="review" itemscope itemtype="http://schema.org/Review">
        <meta itemprop="datePublished" content="{{ $review['date'] }}">
        <div class="review-card__body">
            <div class="review-card__client-name"
                 itemprop="author"
                 itemscope
                 itemtype="https://schema.org/Person">
                <span itemprop="name">{{ $review['client_name'] }}</span>
                <span class="review-card__date">(Заказ от {{ $review['date'] }})</span>
            </div>
            <div itemprop="reviewBody">
                @if($review['body'])
                    <p>
                        {{ $review['body'] }}
                    </p>
                @endif
            </div>
            <div class="review-card__average"
                 itemprop="reviewRating"
                 itemscope
                 itemtype="http://schema.org/Rating">
                <meta itemprop="worstRating" content="1">
                <meta itemprop="bestRating" content="5">
                Заказ выполнен на
                <span itemprop="ratingValue">{{ $review['average_rating'] }} <i class="icofont-star"></i></span>
            </div>
        </div>
        <div>
            <div class="font-weight-bold mb-2">Состав заказа:</div>
            <div class="review-card__items">
                @foreach($review['items'] as $item)
                    <div class="review-card__item">
                        @if($item['product_image'])
                            <img class="review-card__item-img"
                                 src="{{ $item['product_image'] }}"
                                 alt="{{ $item['product_name'] }}">
                        @else
                            <i class="review-card__item-img icofont-image"></i>
                        @endif
                        <a class="review-card__item-name"
                           href="{{ $item['product_link'] }}"
                            {{ $loop->first ?  'itemprop="itemReviewed"'  : '' }}
                        >{{ $item['product_name'] }}</a>
                        <div class="review-card__rating rating">
                            @for($i=0; $i<5; $i++)
                                <i class="icofont-star {{ $item['rating'] > $i ? 'active' : '' }}"></i>
                            @endfor
                            <span>{{ $item['rating'] }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</review-card>
