<div class="{{ $class ?? '' }}">
    <h3 class="text-uppercase mb-4">Отзывы наших клиентов</h3>
    <div class="swiper-container" data-preset="reviews">
        <div class="swiper-wrapper">
            @foreach($reviews as $review)
                <div class="swiper-slide">
                    @include('components.review', ['review' => $review])
                </div>
            @endforeach
        </div>
    </div>
</div>
