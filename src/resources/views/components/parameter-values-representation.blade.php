@if(in_array($type, ['tabs', 'list']))
    @foreach($values as $value)
        {{$value['name']}},
    @endforeach
@elseif(in_array($type, ['number', 'text']))
    @foreach($values as $value)
        {{$value['data']['input']}},
    @endforeach
@endif
