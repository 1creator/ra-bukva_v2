<div class="category-card">
    <div class="category-card__header-wrapper">
        <a class="category-card__header"
           href="{{route('categories.show', ['category' => $category])}}">
            @if($category->image)
                <img class="category-card-header__image"
                     alt="{{ $category->name }}"
                     src="{{ $category->image->thumbnail }}">
            @else
                <div class="category-card-header__image">
                    <i class="icofont-image"></i>
                </div>
            @endif
            <div class="category-card-header__name">{{$category->name}}</div>
        </a>
    </div>
    <ul class="category-card__children list-unstyled">
        @foreach($category->cardListItems() as $item)
            <li><a href="{{$item['link']}}">{{$item['name']}}</a></li>
        @endforeach
    </ul>
</div>
