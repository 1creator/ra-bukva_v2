<div class="product-card-mini-wrapper">
    <a class="product-card-mini"
       href="{{route('products.show', ['product'=>$product->seo_name])}}">
        @if($product->image)
            <img class="product-card-mini__image"
                 alt="{{$product->name}}"
                 src="{{ $product->image->thumbnail }}">
        @else
            <div class="product-card-mini__image">
                <i class="icofont-image"></i>
            </div>
        @endif
        <div class="product-card-mini__name">{{$product->name}}</div>
    </a>
</div>
