@if(isset($articles) && count($articles)>0)
    <div class="overflow-hidden">
        <div class="container">
            <div class="">
                <div class="h3 text-uppercase mb-4">Полезные статьи</div>
                <div class="article-preview-line"></div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($articles as $article)
                            <div class="swiper-slide">
                                <div class="article-preview"
                                     style="background-image: url('{{$article->image->url}}')">
                                    <div class="article-preview__header">
                                        <div class="article-preview-header__date">
                                            <div class="h1">{{$article->created_at->day}}</div>
                                            <div class="text-nowrap">
                                                {{$article->created_at->locale('ru')->getTranslatedMonthName('Do MMMM')}}
                                                {{$article->created_at->year}}
                                            </div>
                                        </div>
                                        <div class="article-preview-header__title">
                                            <div class="h2">{{$article->name}}</div>
                                            <a href="{{route('articles.show',['article'=>$article->seo_name])}}"
                                               class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                                        </div>
                                        <div class="article-preview-header__stars ml-auto">
                                            <img src="/images/logo-mini.png">
                                        </div>
                                    </div>
                                    <div class="py-130 article-preview-body">
                                        <div class="article-preview-body__text">
                                            {!! Str::limit(strip_tags($article->text), 256) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-custom-arrow swiper-button-prev icofont-swoosh-left d-none d-md-block"></div>
                    <div class="swiper-custom-arrow swiper-button-next icofont-swoosh-right d-none d-md-block"></div>
                </div>
            </div>
        </div>
    </div>
@endif
