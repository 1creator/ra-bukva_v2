<h3 class="mb-3">Вам может быть интересно</h3>
<div class="swiper-container recommendations-slider" data-preset="recommendations">
    <div class="swiper-wrapper">
        @foreach($products as $recProduct)
            <div class="swiper-slide">
                @include('components.card-product', ['product'=>$recProduct])
            </div>
        @endforeach
    </div>
    <div class="swiper-pagination"></div>
</div>