@extends('layouts.app')
@push('head')
    <title>Нет доступа</title>
@endpush
@section('content')
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="p-error__code">403</h1>
                <h3>НЕТ ДОСТУПА</h3>
                <div>Кажется, у вас недостаточно прав на просмотр этой страницы.</div>
            </div>
            <div class="col-12 col-md-6">
                <img src="/images/errors/403.svg">
            </div>
        </div>
    </div>
@endsection
