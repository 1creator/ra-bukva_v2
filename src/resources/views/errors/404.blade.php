@extends('layouts.app')
@push('head')
    <title>Страница не найдена</title>
@endpush
@section('content')
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="p-error__code">404</h1>
                <h3>ЭТА СТРАНИЦА НЕ НАЙДЕНА</h3>
                <div>Но у нас есть много <a href="/">других страниц.</a></div>
            </div>
            <div class="col-12 col-md-6">
                <img src="/images/errors/404.gif">
            </div>
        </div>
    </div>
@endsection