<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'new' => 'Новый',
    'check' => 'На проверке',
    'payment' => 'Ждёт оплаты',
    'layout' => 'Разработка макета',
    'layout_agreement' => 'Согласование макета',
    'layout_rejected' => 'Макет отклонён',
    'layout_accepted' => 'Макет принят',
    'work' => 'В работе',
    'ready' => 'Готов',

];
