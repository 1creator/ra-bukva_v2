import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import cart from './modules/cart';
import messages from './modules/messages';

Vue.use(Vuex);


export default new Vuex.Store({
    modules: {
        auth, cart, messages
    },
})