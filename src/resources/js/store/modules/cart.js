function saveCartToLocalStorage() {
    localStorage.setItem('cart', JSON.stringify(state.items));
}

function getCartFromLocalStorage() {
    let value = localStorage.getItem('cart');
    try {
        return JSON.parse(value);
    } catch (e) {
        return [];
    }
}

// initial state
const state = {
    items: getCartFromLocalStorage() || []
};

// getters
const getters = {
    total() {
        return state.items.reduce((acc, item) => {
            return acc += item.total;
        }, 0);
    }
};

// actions
const actions = {
    // getAllProducts({commit}) {
    //     shop.getProducts(products => {
    //         commit('setProducts', products)
    //     })
    // }
};

// mutations
const mutations = {
    addItem(state, item) {
        state.items.push(item);
        saveCartToLocalStorage();
    },
    removeItem(state, item) {
        let index = state.items.indexOf(item);
        if (index >= 0) {
            state.items.splice(index, 1);
            saveCartToLocalStorage();
        }
    },
    clear(state) {
        state.items = [];
        saveCartToLocalStorage();
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};