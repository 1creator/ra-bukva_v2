import {safeToCamelCaseKeys} from "../../utils";
import UserAPI from "../../api/UserAPI";

let userToken = document.head.querySelector("meta[name=\"user\"]");
let user = null;
let mainCompany = null;

if (userToken) {
    user = safeToCamelCaseKeys(JSON.parse(userToken.content), {deep: true});
    if (user.companies && user.companies.length > 0) {
        mainCompany = user.companies[0];
    }
}

// initial state
const state = {
    user: user,
    company: mainCompany
};

// getters
const getters = {
    isAdmin: state => {
        return state.user && ("position" in state.user);
    },
    can: state => (ability, obj) => {
        return ability(obj);
    },
};

// actions
const actions = {
    login({commit}, {email, password}) {
        return new Promise((resolve, reject) => {
            UserAPI.auth.login({
                email: email,
                password: password,
            })
                .then(user => {
                    commit("setUser", user);
                    resolve(user);
                })
                .catch(e => {
                    reject(e);
                });
        });
    },
    register({commit}, payload) {
        return new Promise((resolve, reject) => {
            UserAPI.auth.register({
                first_name: payload.firstName,
                email: payload.email,
                password: payload.password,
                password_confirmation: payload.passwordConfirmation,
                terms_agreement: payload.termsAgreement,
            })
                .then(user => {
                    commit("setUser", user);
                    resolve(user);
                })
                .catch(e => {
                    reject(e);
                });
        });
    },
};

// mutations
const mutations = {
    setUser(state, user) {
        state.user = user
    }
    //
    // decrementProductInventory(state, {id}) {
    //     const product = state.all.find(product => product.id === id)
    //     product.inventory--
    // }
};

export const Abilities = {
    Articles: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "articles.update");
        },
    },
    Catalog: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "catalog.update");
        },
    },
    Companies: {
        View: function () {
            return state.user.permissions.some(item => item.id === "companies.view");
        },
        Update: function () {
            return state.user.permissions.some(item => item.id === "companies.update");
        },
    },
    Mailings: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "mailings.update");
        },
    },
    Orders: {
        View: function () {
            return state.user.permissions.some(item => item.id === "orders.view");
        },
        Update: function () {
            return state.user.permissions.some(item => item.id === "orders.update");
        },
        ViewForDesign: function () {
            return state.user.permissions.some(item => item.id === "orders.view_for_design");
        },
    },
    Projects: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "projects.update");
        },
    },
    Promocodes: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "promocodes.update");
        },
    },
    Sales: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "sales.update");
        },
    },
    Settings: {
        Update: function () {
            return state.user.permissions.some(item => item.id === "settings.update");
        },
    },
    Statistic: {
        View: function () {
            return state.user.permissions.some(item => item.id === "statistic.view");
        },
    },
    Users: {
        View: function () {
            return state.user.permissions.some(item => item.id === "users.view");
        },
        GrantStaffPermissions: function () {
            return state.user.permissions.some(item => item.id === "users.grant_staff_permissions");
        },
        Update: function () {
            return state.user.permissions.some(item => item.id === "users.update");
        },
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
