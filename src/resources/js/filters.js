import Vue from "vue";

import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru');

Vue.filter('f_percent', function (value) {
    if (!value) return 0;
    return (value * 100).toFixed(0);
});

Vue.filter('f_round', function (value) {
    if (!value) return 0;
    return value.toFixed(0);
});

Vue.filter('f_fixed', function (value) {
    return value.toFixed(2).replace(/[.,]00$/, "");
});

Vue.filter('f_orderProductStatus', function (value) {
    let statuses = {
        'new': 'Новый',
        'check': 'На проверке',
        'payment': 'Ждёт оплаты',
        'work': 'В работе',
        'ready': 'Готов',
        'layout': 'Разработка макета',
        'layout_agreement': 'Согласование макета',
        'layout_rejected': 'Макет отклонен',
        'offer': 'Коммерческое предложение',
    };
    if (!statuses[value]) return value;
    return statuses[value];
});

Vue.filter('f_partnerStatus', function (value) {
    let statuses = {
        'requested': 'Оформлена заявка',
        'rejected': 'Отклонено',
        'accepted': 'Принято',
    };
    if (!statuses[value]) return value;
    return statuses[value];
});

Vue.filter('f_user', function (user) {
    if (!user) return "Не указано";
    if (!user.firstName && !user.middleName && !user.secondName) {
        return 'Не заполнено';
    }
    return `${user.firstName || ''} ${user.middleName || ''} ${user.secondName || ''}`;
});

Vue.filter('f_datetime', function (datetime) {
    return moment.utc(datetime).local().locale('ru').format('LLL');
});

Vue.filter('f_date', function (datetime) {
    return moment.utc(datetime).local().locale('ru').format('LL');
});

Vue.filter('f_layoutStatus', function (value) {
    let statuses = {
        'new': 'Новый',
        'rejected': 'Отклонен',
        'accepted': 'Принят',
    };
    if (!statuses[value]) return value;
    return statuses[value];
});

Vue.filter('f_bonusChangeType', function (changeType) {
    if (changeType === 'sub') {
        return 'Использование';
    } else {
        return 'Начисление';
    }
});

Vue.filter('f_bonusChangeStatus', function (status) {
    return status ? 'Начислены' : 'Ожидают начисления';
});

/**
 * Выбирает склонение числа, соответствующее числу
 * пример использования f_choiceNominative(13, 'точка', 'точки', 'точек')
 * 1 21 точка
 * 2 3 4 22 23 24 точки
 * 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 точек
 * */
Vue.filter('f_choice', (value, a, b, c) => {
    if (value >= 20) value = value % 10;
    if (value === 1) return a;
    if (value !== 0 && value < 5) return b;
    return c;
});
