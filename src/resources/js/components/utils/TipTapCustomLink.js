import {Mark} from 'tiptap';
import {wrappingInputRule, setBlockType, toggleMark, updateMark, removeMark} from 'tiptap-commands';

export default class TipTapCustomLink extends Mark {

    // choose a unique name
    get name() {
        return 'customlink'
    }

    // the prosemirror schema object
    // take a look at https://prosemirror.net/docs/guide/#schema for a detailed explanation
    get schema() {
        return {
            attrs: {
                href: {
                    default: null
                }
            },
            inclusive: false,
            parseDOM: [{
                tag: 'a[href]',
                getAttrs: function getAttrs(dom) {
                    return {
                        href: dom.getAttribute('href')
                    };
                }
            }],
            toDOM: function toDOM(node) {
                return ['a', node.attrs, 0];
            }
        }
    }

    // this command will be called from menus to add a blockquote
    // `type` is the prosemirror schema object for this blockquote
    // `schema` is a collection of all registered nodes and marks
    commands({type, schema}) {
        return (attrs) => {
            if (attrs.href) {
                return updateMark(type, attrs);
            }

            return removeMark(type);
        }
    }
}