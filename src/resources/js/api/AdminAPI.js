import axios from "axios";
import {safeToCamelCaseKeys} from "../utils";

const http = axios.create({
    baseURL: "/crud/admin"
});

http.interceptors.response.use(value => {
    return safeToCamelCaseKeys(value, {deep: true});
}, error => {
    console.error(error);
    if (error.response.status === 403) {
        alert(error.response.data.message);
    }
    return Promise.reject(error);
});

export let categories = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`categories`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: categories.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`categories/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: categories.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`categories`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: categories.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`categories/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: categories.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`categories/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: categories.destroy");
                    reject(e.response)
                });
        });
    },
};

export let products = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`products`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`products/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`products`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`products/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`products/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.destroy");
                    reject(e.response)
                });
        });
    },
};

export let sales = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`sales`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: sales.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`sales/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: sales.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`sales`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: sales.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`sales/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: sales.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`sales/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: sales.destroy");
                    reject(e.response)
                });
        });
    },
};

export let articles = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`articles`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: articles.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`articles/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: articles.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`articles`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: articles.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`articles/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: articles.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`articles/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: articles.destroy");
                    reject(e.response)
                });
        });
    },
};

export let projects = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`projects`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: projects.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`projects/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: projects.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`projects`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: projects.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`projects/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: projects.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`projects/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: projects.destroy");
                    reject(e.response)
                });
        });
    },
};

export let users = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`users`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`users/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.getOne");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`users/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.update");
                    reject(e.response)
                });
        });
    },
    getSelf: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`users/self`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.getSelf");
                    reject(e.response)
                });
        });
    },
    updateSelf: function (params) {
        return new Promise((resolve, reject) => {
            http
                .put(`users/self`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.getSelf");
                    reject(e.response)
                });
        });
    },
};

export let settings = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`settings`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: settings.get");
                    reject(e.response)
                });
        });
    },
    update: function (params) {
        return new Promise((resolve, reject) => {
            http
                .put(`settings`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error(e);
                    console.error("Request failed: settings.update");
                    reject(e.response)
                });
        });
    },
};

export let orders = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`orders`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`orders/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`orders`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`orders/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`orders/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.destroy");
                    reject(e.response)
                });
        });
    },
    notify: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .post(`orders/${id}/notify`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.notify");
                    reject(e.response)
                });
        });
    },
};

export let orderProducts = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`order-products`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`order-products/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`order-products`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.store");
                    reject(e.response)
                });
        });
    },
    storeLayouts: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .post(`order-products/${id}/layouts`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.storeLayouts");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`order-products/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.update");
                    reject(e.response)
                });
        });
    },
    review: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`order-products/${id}/review`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.review");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`order-products/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.destroy");
                    reject(e.response)
                });
        });
    },
};

export let orderProductLayouts = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`order-product-layouts`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`order-product-layouts/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`order-product-layouts`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`order-product-layouts/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`order-product-layouts/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.destroy");
                    reject(e.response)
                });
        });
    },
};

export let promocodes = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`promocodes`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: promocodes.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`promocodes/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: promocodes.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`promocodes`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: promocodes.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`promocodes/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: promocodes.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`promocodes/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: promocodes.destroy");
                    reject(e.response)
                });
        });
    },
};

export let companies = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`companies`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`companies/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`companies`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`companies/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`companies/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.destroy");
                    reject(e.response)
                });
        });
    },
};

export let mailings = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`mailings`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: mailings.get");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`mailings`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: mailings.store");
                    reject(e.response)
                });
        });
    },
};

export let permissions = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`permissions`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: permissions.get");
                    reject(e.response)
                });
        });
    },
};

export default {
    categories,
    products,
    sales,
    articles,
    projects,
    users,
    settings,
    orders,
    orderProducts,
    orderProductLayouts,
    promocodes,
    mailings,
    companies,
    permissions,
}
