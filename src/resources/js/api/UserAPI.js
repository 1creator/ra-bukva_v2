import axios from "axios";
import {safeToCamelCaseKeys} from "../utils";

const http = axios.create({
    baseURL: "/crud"
});

http.interceptors.response.use(value => {
    return safeToCamelCaseKeys(value, {deep: true});
}, error => {
    console.error(error);
    if (error.response.status === 403) {
        alert(error.response.data.message);
    }
    return Promise.reject(error);
});

export let auth = {
    register: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`register`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: auth.register");
                    reject(e.response)
                });
        });
    },
    login: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`login`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: auth.login");
                    reject(e.response)
                });
        });
    },
    async sendResetPasswordCode(params) {
        return (await http.get(`reset-password`, {params})).data;
    },
    async resetPassword(params) {
        return (await http.post(`reset-password`, params)).data;
    },
};

export let users = {
    getSelf: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/users/self`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.getSelf");
                    reject(e.response)
                });
        });
    },
    getBonusChanges: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/users/self/bonus-changes`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.getBonusChanges");
                    reject(e.response)
                });
        });
    },
    updateSelf: function (params) {
        return new Promise((resolve, reject) => {
            http
                .put(`client/users/self`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: users.getSelf");
                    reject(e.response)
                });
        });
    },
};

export let products = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`products`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`products/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: products.getOne");
                    reject(e.response)
                });
        });
    },

};

export let companies = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/companies`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/companies/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/companies`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`client/companies/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: companies.update");
                    reject(e.response)
                });
        });
    },
};

export let orders = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/orders`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/orders/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/orders`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.store");
                    reject(e.response)
                });
        });
    },
    storeReview: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/orders/${id}/review`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.storeReview");
                    reject(e.response)
                });
        });
    },
    async uploadUpd(orderId, params) {
        return (await http.post(`client/orders/${orderId}/upload-upd`, params)).data.response;
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`client/orders/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`client/orders/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.destroy");
                    reject(e.response)
                });
        });
    },
    sendInvoiceToEmail: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/orders/${id}/invoice/send-to-email`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.sendInvoiceToEmail");
                    reject(e.response)
                });
        });
    },
    sendDocumentsToEmail: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/orders/${id}/documents/send-to-email`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orders.sendDocumentsToEmail");
                    reject(e.response)
                });
        });
    },
    async getPaymentForm(id) {
        return (await http.get(`client/orders/${id}/payment-form`)).data;
    },
};

export let orderProductLayouts = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/order-product-layouts`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/order-product-layouts/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/order-product-layouts`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.store");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`client/order-product-layouts/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.update");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`client/order-product-layouts/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProductLayouts.destroy");
                    reject(e.response)
                });
        });
    },
};

export let orderProducts = {
    get: function (params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/order-products`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.get");
                    reject(e.response)
                });
        });
    },
    getOne: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .get(`client/order-products/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.getOne");
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/order-products`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.store");
                    reject(e.response)
                });
        });
    },
    storeLayouts: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .post(`client/order-products/${id}/layouts`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.storeLayouts");
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`client/order-products/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.update");
                    reject(e.response)
                });
        });
    },
    review: function (id, params) {
        return new Promise((resolve, reject) => {
            http
                .put(`client/order-products/${id}/review`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.review");
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            http
                .delete(`client/order-products/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: orderProducts.destroy");
                    reject(e.response)
                });
        });
    },
};

export let callback = {
    request: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`callback`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: callback.request");
                    reject(e.response)
                });
        });
    },
};

export let delivery = {
    getInfo: function () {
        return new Promise((resolve, reject) => {
            http
                .get(`delivery`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    console.error("Request failed: delivery.getInfo");
                    reject(e.response)
                });
        });
    },
};

export let search = {
    search: function (params) {
        return new Promise((resolve, reject) => {
            http
                .post(`search`, params)
                .then(response => {
                    resolve(response.data);
                })
                .catch((e) => {
                    console.error("Request failed: search.search");
                    reject(e.response)
                });
        });
    },
};

export default {
    auth, orders, users, orderProductLayouts, orderProducts,
    companies, callback, products
};
