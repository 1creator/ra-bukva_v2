export default {
    Red: '#e31e24',
    Dark: '#212121',
    LightBlue: '#606aff',
    Blue: '#2b39ff',
    Pink: '#ff5a86',
};
