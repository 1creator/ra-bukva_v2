export default {
    Loading: 0,
    LoadingError: 1,
    LoadingSuccess: 2,
    Submitting: 3,
    SubmittingError: 4,
    SubmittingSuccess: 5,
    Initial: 999
}