import TheCatalog from "../components/Admin/TheCatalog";
import TheCategoryEditor from "../components/Admin/TheCategoryEditor";
import TheProductEditor from "../components/Admin/ProductEditor/TheProductEditor";
import TheSales from "../components/Admin/TheSales";
import TheSaleEditor from "../components/Admin/TheSaleEditor";
import TheArticles from "../components/Admin/TheArticles";
import TheArticleEditor from "../components/Admin/TheArticleEditor";
import TheProfile from "../components/Admin/TheProfile";
import TheSettings from "../components/Admin/Settings/TheSettings";
import TheProjects from "../components/Admin/TheProjects";
import TheProjectEditor from "../components/Admin/TheProjectEditor";
import TheOrders from "../components/Admin/TheOrders";
import TheOrderEditor from "../components/Admin/TheOrderEditor";
import TheOrder from "../components/Admin/TheOrder";
import ThePromocodes from "../components/Admin/ThePromocodes";
import ThePromocodeEditor from "../components/Admin/ThePromocodeEditor";
import TheStatistic from "../components/Admin/TheStatistic";
import TheMailings from "../components/Admin/TheMailings";
import TheMailingEditor from "../components/Admin/TheMailingEditor";
import TheCompanies from "../components/Admin/TheCompanies";
import TheCompanyEditor from "../components/Admin/TheCompanyEditor";
import TheUsers from "../components/Admin/TheUsers";
import TheUserEditor from "../components/Admin/TheUserEditor";

import TheClientOrder from "../components/User/TheOrder";
import VueRouter from "vue-router";
import Vue from "vue";

Vue.use(VueRouter);

let routes = [
    {
        component: TheCatalog,
        path: '/admin/catalog',
        name: 'catalog',
        meta: {title: 'Каталог товаров | РА Буква ПЛЮС'}
    },
    {
        component: TheCategoryEditor,
        path: '/admin/categories/create',
        name: 'categories.create',
        meta: {title: 'Создание категории | РА Буква ПЛЮС'}
    },
    {
        component: TheCategoryEditor,
        path: '/admin/categories/:seoName/edit',
        name: 'categories.edit',
        props: true,
        meta: {title: 'Редактировании категории | РА Буква ПЛЮС'}
    },
    {
        component: TheProductEditor,
        path: '/admin/products/create',
        name: 'products.create',
        meta: {title: 'Создание услуги | РА Буква ПЛЮС'}
    },
    {
        component: TheProductEditor,
        path: '/admin/products/:seoName/edit',
        name: 'products.edit',
        props: true,
        meta: {title: 'Редактирование услуги | РА Буква ПЛЮС'}
    },
    {
        component: TheSales,
        path: '/admin/sales',
        name: 'sales',
        meta: {title: 'Каталог акций | РА Буква ПЛЮС'}
    },
    {
        component: TheSaleEditor,
        path: '/admin/sales/create',
        name: 'sales.create',
        meta: {title: 'Создание акции | РА Буква ПЛЮС'}
    },
    {
        component: TheSaleEditor,
        path: '/admin/sales/:id/edit',
        name: 'sales.edit',
        props: true,
        meta: {title: 'Редактирование акции | РА Буква ПЛЮС'}
    },
    {
        component: TheArticles,
        path: '/admin/articles',
        name: 'articles',
        meta: {title: 'Статьи | РА Буква ПЛЮС'}
    },
    {
        component: TheArticleEditor,
        path: '/admin/articles/create',
        name: 'articles.create',
        meta: {title: 'Создание статьи | РА Буква ПЛЮС'}
    },
    {
        component: TheArticleEditor,
        path: '/admin/articles/:seoName/edit',
        name: 'articles.edit',
        props: true,
        meta: {title: 'Редактирование статьи | РА Буква ПЛЮС'}
    },
    {
        component: TheProfile,
        path: '/admin/profile',
        name: 'profile',
        meta: {title: 'Профиль | РА Буква ПЛЮС'}
    },
    {
        component: TheProjects,
        path: '/admin/projects',
        name: 'projects',
        meta: {title: 'Проекты | РА Буква ПЛЮС'}
    },
    {
        component: TheProjectEditor,
        path: '/admin/projects/create',
        name: 'projects.create',
        meta: {title: 'Создание проекта | РА Буква ПЛЮС'}
    },
    {
        component: TheProjectEditor,
        path: '/admin/projects/:id/edit',
        name: 'projects.edit',
        props: true,
        meta: {title: 'Редактирование проекта | РА Буква ПЛЮС'}
    },
    {
        component: TheSettings,
        path: '/admin/settings',
        name: 'settings',
        meta: {title: 'Настройки системы | РА Буква ПЛЮС'}
    },
    {
        component: TheOrders,
        path: '/admin/orders',
        name: 'orders',
        meta: {title: 'Заказы | РА Буква ПЛЮС'}
    },
    {
        component: TheOrderEditor,
        path: '/admin/orders/create',
        name: 'orders.create',
        props: true,
        meta: {title: 'Создание заказа | РА Буква ПЛЮС'}
    },
    {
        component: TheOrderEditor,
        path: '/admin/orders/:orderId/edit',
        name: 'orders.edit',
        props: true,
        meta: {title: 'Редактирование заказа | РА Буква ПЛЮС'}
    },
    {
        component: TheOrder,
        path: '/admin/orders/:orderId',
        name: 'orders.show',
        props: true,
        meta: {title: 'Просмотр заказа | РА Буква ПЛЮС'}
    },
    {
        component: ThePromocodes,
        path: '/admin/promocodes',
        name: 'promocodes',
        meta: {title: 'Промокоды | РА Буква ПЛЮС'}
    },
    {
        component: ThePromocodeEditor,
        path: '/admin/promocodes/create',
        name: 'promocodes.create',
        meta: {title: 'Создание промокода | РА Буква ПЛЮС'}
    },
    {
        component: ThePromocodeEditor,
        path: '/admin/promocodes/:promocodeId/edit',
        name: 'promocodes.edit',
        props: true,
        meta: {title: 'Редактирование промокода | РА Буква ПЛЮС'}
    },
    {
        component: TheClientOrder,
        path: '/orders/:orderId',
        name: 'orders.secretShow',
        props: true,
        meta: {title: 'Просмотр заказа | РА Буква ПЛЮС'}
    },
    {
        component: TheStatistic,
        path: '/admin/statistic',
        name: 'statistic',
        meta: {title: 'Статистика | РА Буква ПЛЮС'}
    },
    {
        component: TheMailings,
        path: '/admin/mailings',
        name: 'mailings',
        meta: {title: 'Рассылки | РА Буква ПЛЮС'}
    },
    {
        component: TheMailingEditor,
        path: '/admin/mailings/create',
        name: 'mailings.create',
        meta: {title: 'Создание рассылки| РА Буква ПЛЮС'}
    },
    {
        component: TheCompanies,
        path: '/admin/companies',
        name: 'companies',
        meta: {title: 'Организации | РА Буква ПЛЮС'}
    },
    {
        component: TheCompanyEditor,
        path: '/admin/companies/create',
        name: 'companies.create',
        meta: {title: 'Создание организации | РА Буква ПЛЮС'}
    },
    {
        component: TheCompanyEditor,
        path: '/admin/companies/:companyId/edit',
        name: 'companies.edit',
        props: true,
        meta: {title: 'Редактирование организации | РА Буква ПЛЮС'}
    },
    {
        component: TheUsers,
        path: '/admin/users',
        name: 'users',
        meta: {title: 'Пользователи | РА Буква ПЛЮС'}
    },
    {
        component: TheUserEditor,
        path: '/admin/users/:userId/edit',
        name: 'users.edit',
        props: true,
        meta: {title: 'Редактирование пользователя | РА Буква ПЛЮС'}
    },
];

export const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title || document.title;
    next();
});
