import TheOrders from "../components/User/TheOrders";
import TheProfile from "../components/User/TheProfile";
import TheOrder from "../components/User/TheOrder";
import OrderCheckout from "../components/User/Checkout/OrderCheckout";
import TheBonusDetails from "../components/User/TheBonusDetails";
import TheCompanyEditor from "../components/User/TheCompanyEditor";
import VueRouter from "vue-router";
import Vue from "vue";
import TheUpdUpload from "@/js/components/User/TheUpdUpload";

Vue.use(VueRouter);
let routes = [
    {
        component: TheOrders,
        path: '/cabinet/orders',
        name: 'orders',
        meta: {title: 'Мои заказы | РА Буква ПЛЮС'}
    },
    {
        component: TheProfile,
        path: '/cabinet/profile',
        name: 'profile',
        meta: {title: 'Профиль | РА Буква ПЛЮС'}
    },
    {
        component: TheProfile,
        path: '/cabinet/profile#companies',
        name: 'profile.companies',
        meta: {title: 'Связанные организации | РА Буква ПЛЮС'}
    },
    {
        component: TheOrder,
        path: '/cabinet/orders/:orderId',
        name: 'orders.show',
        props: true,
        meta: {title: 'Просмотр заказа | РА Буква ПЛЮС'}
    },
    {
        component: TheOrder,
        path: '/orders/:orderId',
        name: 'orders.secretShow',
        props: true,
        meta: {title: 'Просмотр заказа | РА Буква ПЛЮС'}
    },
    {
        component: TheUpdUpload,
        path: '/orders/:orderId/upload-upd',
        name: 'orders.uploadUpd',
        props: true,
        meta: {title: 'Загрузка УПД | РА Буква ПЛЮС'}
    },
    {
        component: OrderCheckout,
        path: '/checkout',
        name: 'checkout',
        meta: {title: 'Оформление заказа | РА Буква ПЛЮС'}
    },
    {
        component: TheBonusDetails,
        path: '/cabinet/bonus-details',
        name: 'bonuses',
        meta: {title: 'Бонусный баланс | РА Буква ПЛЮС'}
    },
    {
        component: TheCompanyEditor,
        path: '/cabinet/companies/create',
        name: 'companies.create',
        meta: {title: 'Добавление организации | РА Буква ПЛЮС'}
    },
    {
        component: TheCompanyEditor,
        path: '/cabinet/companies/:companyId/edit',
        name: 'companies.edit',
        props: true,
        meta: {title: 'Редактирование организации | РА Буква ПЛЮС'}
    },
];

export const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title || document.title;
    next();
});
