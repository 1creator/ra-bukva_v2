import Vue from "vue";
import {router} from "./routes/routes-client";

import BaseCalculator from "./components/User/Calculator/BaseCalculator";
import TopProfile from "./components/TopProfile";
import TopCart from "./components/User/TopCart";
import FeedbackModal from "./components/User/FeedbackModal";
import OrderCheckout from "./components/User/Checkout/OrderCheckout";
import TheOrder from "../js/components/User/TheOrder";
import "./filters";
import store from "./store/store-client";
import {initSwipers} from "./utils";
import MessagesComponent from "./components/utils/MessagesComponent";
import SearchComponent from "./components/User/Search/SearchComponent";
import {initLightBox} from "@/js/utils/initLightbox";

window["VueApp"] = new Vue({
    router: router,
    el: "#app",
    components: {
        MessagesComponent, BaseCalculator, TopProfile, TopCart, OrderCheckout, TheOrder, FeedbackModal,
        SearchComponent,
        ReviewCard: () => import("../js/components/User/Review/ReviewCard")
    },
    data() {
        return {
            d_showFeedbackModal: false
        }
    },
    store: store,
    mounted() {
        initSwipers();
        initLightBox();
        // setTimeout(function () {
        //     let jivosite = document.createElement('script');
        //     jivosite.setAttribute('src', '//code.jivosite.com/widget.js');
        //     jivosite.setAttribute('jv-id', '939xngDbqh');
        //     document.head.appendChild(jivosite);
        // }, 3000);
    }
});
