import Swiper from "swiper";

export const initSwipers = function () {
    let swipers = document.getElementsByClassName("swiper-container");

    for (let i = 0; i < swipers.length; i++) {
        let item = swipers[i];
        let preset = item.getAttribute("data-preset") || null;

        switch (preset) {
            case "recommendations":
                new Swiper(item, {
                    direction: "horizontal",
                    autoplay: true,
                    delay: 3000,
                    breakpoints: {
                        768: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        991: {
                            slidesPerView: 4,
                            spaceBetween: 30
                        }
                    },
                    slidesPerView: 6,
                    spaceBetween: 30,
                    lazy: {
                        loadPrevNext: true,
                    },
                    pagination: {
                        el: ".swiper-pagination",
                    },
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },
                    observer: true,
                });
                break;
            case "reviews":
                new Swiper(item, {
                    direction: "horizontal",
                    autoplay: true,
                    delay: 5000,
                    loop:false,
                    breakpoints: {
                        991: {
                            slidesPerView: 1,
                            spaceBetween: 30
                        }
                    },
                    slidesPerView: 2,
                    spaceBetween: 30,
                    // navigation: {
                    //     nextEl: ".swiper-button-next",
                    //     prevEl: ".swiper-button-prev",
                    // },
                    observer: true,
                });
                break;
            default:
                let spaceBetween = item.getAttribute("data-spaceBetween") || 0;
                let slidesPerView = item.getAttribute("data-slidesPerView") || "auto";
                new Swiper(item, {
                    direction: "horizontal",
                    autoplay: true,
                    delay: 10000,
                    spaceBetween: spaceBetween,
                    slidesPerView: slidesPerView,
                    loop: true,
                    lazy: {
                        loadPrevNext: true,
                    },
                    pagination: {
                        el: ".swiper-pagination",
                        dynamicBullets: true,
                    },
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },
                    observer: true,
                });
                break
        }
    }
};

export function loadMissedFields(fields, object, func, loadingObj) {

    let loadingFields = fields.filter(item => !object[item]);
    loadingFields.forEach(item => loadingObj[item] = true);

    func(object.id, {
        with: loadingFields
    }).then(response => {
        for (let field of loadingFields) {
            this.$set(object, field, response[field]);
            loadingObj[field] = false;
        }
    });
}


const toCamel = (s) => {
    return s.replace(/([-_][a-z])/ig, ($1) => {
        return $1.toUpperCase()
            .replace("-", "")
            .replace("_", "");
    });
};

const isObject = function (o) {
    return o === Object(o) && !isArray(o) && typeof o !== "function";
};

const isArray = function (o) {
    return Array.isArray(o);
};

export function safeToCamelCaseKeys(o, options) {
    if (isObject(o)) {
        const n = {};

        Object.keys(o)
            .forEach((k) => {
                n[toCamel(k)] = safeToCamelCaseKeys(o[k]);
            });

        return n;
    } else if (isArray(o)) {
        return o.map((i) => {
            return safeToCamelCaseKeys(i);
        });
    }

    return o;
}
