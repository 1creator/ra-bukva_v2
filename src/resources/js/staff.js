import Vue from "vue";
import {router} from "./routes/routes-admin";
import BaseCalculator from "./components/User/Calculator/BaseCalculator";
import TopProfile from "./components/TopProfile";
import TopCart from "./components/User/TopCart";
import FeedbackModal from "./components/User/FeedbackModal";
import store from "./store/store-admin";
import "./filters";
import {initSwipers} from "./utils";
import MessagesComponent from "./components/utils/MessagesComponent";
import SearchComponent from "@/js/components/User/Search/SearchComponent";
import {initLightBox} from "./utils/initLightbox";

window["VueApp"] = new Vue({
    router: router,
    el: "#app",
    components: {
        MessagesComponent, BaseCalculator, TopProfile, TopCart, FeedbackModal,
        SearchComponent,
        ReviewCard: () => import("../js/components/User/Review/ReviewCard")
    },
    data() {
        return {
            d_showFeedbackModal: false
        }
    },
    store: store,
    mounted() {
        initSwipers();
        initLightBox();
    }
});
